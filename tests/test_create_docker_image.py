"""Test post endpoint."""

import pytest
from starlette import status

from tests.database import execute_queries_from_file

ENDPOINT = '/docker-images/'


@pytest.mark.asyncio
async def test_create_image_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.post(
        ENDPOINT,
        json={
            'name': 'image_name',
            'user_id': 0,
            'url': 'image_url',
            'is_test': True
        }
    )
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.asyncio
async def test_create_image_name_already_registered(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.post(
        ENDPOINT,
        json={
            'name': 'my_docker_image1',
            'user_id': 0,
            'url': 'image_url',
            'is_test': True
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'name : my_docker_image1 already registered for object Docker image'}
