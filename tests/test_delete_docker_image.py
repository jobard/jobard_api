"""Test delete endpoint."""
import pytest
from starlette import status

from tests.database import execute_queries_from_file

DOCKER_IMAGES_ENDPOINT = '/docker-images/{0}'
DOCKER_MOUNT_POINTS_ENDPOINT = '/docker-mount-points/{0}'


@pytest.mark.asyncio
async def test_delete_image_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.delete(DOCKER_IMAGES_ENDPOINT.format(1))
    assert response.status_code == status.HTTP_204_NO_CONTENT


@pytest.mark.asyncio
async def test_delete_image_unknown_id(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.delete(DOCKER_IMAGES_ENDPOINT.format(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Docker image ID : 9999'}


@pytest.mark.asyncio
async def test_delete_image_with_mount_points_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.delete(DOCKER_IMAGES_ENDPOINT.format(2))
    assert response.status_code == status.HTTP_204_NO_CONTENT
    # Check mount points deletion
    response_check_mount_points = await async_app_client.get(DOCKER_MOUNT_POINTS_ENDPOINT.format(2))
    # check status and response
    assert response_check_mount_points.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.asyncio
async def test_delete_forbidden_image(postgresql_database, async_app_client_user1, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client_user1.delete(DOCKER_IMAGES_ENDPOINT.format(3))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert json_response == {'detail': 'Docker image ID : 3 does not belong to the authenticated user'}


@pytest.mark.asyncio
async def test_delete_disabled_image(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.delete(DOCKER_IMAGES_ENDPOINT.format(7))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Docker image ID : 7'}
