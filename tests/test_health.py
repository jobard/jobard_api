"""Test health endpoint."""
import pytest

from starlette import status

ENDPOINT = '/health'


@pytest.mark.asyncio
async def test_health_ok(postgresql_database, async_app_client):
    response = await async_app_client.get(ENDPOINT)
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    assert json_response['status'] == 'Healthy'
