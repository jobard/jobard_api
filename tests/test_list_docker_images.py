"""Test list endpoint."""
import pytest
from starlette import status

from jobard_api.docker.dto import DockerImageList
from tests.database import execute_queries_from_file

ENDPOINT = '/docker-images{0}'


@pytest.mark.asyncio
async def test_list_docker_images_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format('?offset=0&limit=2'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    docker_images_list = DockerImageList.parse_obj(json_response)
    assert docker_images_list.count == 3
    assert docker_images_list.offset == 0
    assert docker_images_list.limit == 2
    # Check sort order by image id
    for idx, image in enumerate(docker_images_list.records):
        assert image.image_id == idx + 1