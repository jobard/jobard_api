"""Test arguments endpoint."""

import pytest
from starlette import status

from jobard_api.jobs.dto import JobOrderList
from tests.database import execute_queries_from_file

ENDPOINT = '/job-orders{0}'

@pytest.mark.asyncio
async def test_job_orders_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format('?offset=0&limit=4'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    list_job_orders = JobOrderList.parse_obj(json_response)
    assert list_job_orders.count == 6
    assert len(list_job_orders.records) == list_job_orders.limit == 4
    assert list_job_orders.offset == 0
    # Check sort order by job order id
    expected_order = [1,2,3,5]
    for idx, jo in enumerate(list_job_orders.records):
        assert jo.id == expected_order[idx]

@pytest.mark.asyncio
async def test_job_orders_ok_with_state_filter(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format('?state=SUCCEEDED'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    list_job_orders = JobOrderList.parse_obj(json_response)
    assert list_job_orders.count == len(list_job_orders.records) == 3


@pytest.mark.asyncio
async def test_job_orders_with_good_name_filter(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)

    for search_name in ('my_job', 'my_', '_job'):
        # execute endpoint
        response = await async_app_client.get(ENDPOINT.format('?name={0}'.format(search_name)))
        # check status and response
        json_response = response.json()
        assert response.status_code == status.HTTP_200_OK, json_response
        # parse and check response
        list_job_orders = JobOrderList.parse_obj(json_response)
        assert list_job_orders.count == len(list_job_orders.records) == 1


@pytest.mark.asyncio
async def test_job_orders_with_bad_name_filter(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)

    for search_name in ('my%', '%job'):
        # execute endpoint
        response = await async_app_client.get(ENDPOINT.format('?name={0}'.format(search_name)))
        # check status and response
        json_response = response.json()
        assert response.status_code == status.HTTP_200_OK, json_response
        # parse and check response
        list_job_orders = JobOrderList.parse_obj(json_response)
        assert list_job_orders.count == 0


@pytest.mark.asyncio
async def test_arguments_unknown_state(async_app_client, postgresql_database):
    response = await async_app_client.get(ENDPOINT.format('?offset=0&limit=1000&state=EXOTIC'))
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
