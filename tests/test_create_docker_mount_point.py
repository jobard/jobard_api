"""Test post endpoint."""
import pytest
from starlette import status

from tests.database import execute_queries_from_file

ENDPOINT = '/docker-mount-points/'


@pytest.mark.asyncio
async def test_create_docker_mount_point_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.post(
        ENDPOINT,
        json={
            'image_id': 1,
            'name': 'mount_point_name',
            'source': 'source_name',
            'destination': 'destination_name'
        }
    )
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.asyncio
async def test_create_docker_mount_point_name_already_registered(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.post(
        ENDPOINT,
        json={
            'image_id': 1,
            'name': 'mount_point1',
            'source': 'source_name',
            'destination': 'destination_name'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'name : mount_point1 already registered for object Docker mount point'}


@pytest.mark.asyncio
async def test_create_docker_mount_point_forbidden_image(postgresql_database, async_app_client_user1, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client_user1.post(
        ENDPOINT,
        json={
            'image_id': 1,
            'name': 'mount_point_name',
            'source': 'source_name',
            'destination': 'destination_name'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert json_response == {'detail': 'Docker image ID : 1 does not belong to the authenticated user'}


@pytest.mark.asyncio
async def test_create_docker_mount_point_disabled_image(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.post(
        ENDPOINT,
        json={
            'image_id': 7,
            'name': 'mount_point_name',
            'source': 'source_name',
            'destination': 'destination_name'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Docker image ID : 7'}
