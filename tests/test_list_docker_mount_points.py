"""Test list endpoint."""
import pytest
from starlette import status

from jobard_api.docker.dto import DockerMountPointList
from tests.database import execute_queries_from_file

ENDPOINT = '/docker-mount-points/{0}'


@pytest.mark.asyncio
async def test_list_docker_mount_points_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format('1?offset=0&limit=2'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    docker_mount_points_list = DockerMountPointList.parse_obj(json_response)
    assert docker_mount_points_list.count == 3
    assert docker_mount_points_list.offset == 0
    assert docker_mount_points_list.limit == 2
    # Check sort order by mount point id
    for idx,mp in enumerate(docker_mount_points_list.records):
        assert mp.mount_point_id == idx + 1

@pytest.mark.asyncio
async def test_list_docker_mount_points_unknown_image_id(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Docker image ID : 9999'}


@pytest.mark.asyncio
async def test_list_docker_mount_points_unknown_mount_point_id(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format('9999/details'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Docker mount point ID : 9999'}


@pytest.mark.asyncio
async def test_list_docker_mount_points_forbidden_image(postgresql_database, async_app_client_user1, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client_user1.get(ENDPOINT.format('1/details'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN, json_response
    assert json_response == {'detail': 'Docker image ID : 1 does not belong to the authenticated user'}


@pytest.mark.asyncio
async def test_list_docker_mount_points_disabled_image(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format('7/details'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Docker image ID : 7'}
