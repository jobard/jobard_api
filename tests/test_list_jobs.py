"""Test list endpoint."""

import pytest
from starlette import status

from jobard_api.jobs.dto import JobList
from tests.database import execute_queries_from_file

ENDPOINT = '/job-orders/{0}/jobs/{1}'


@pytest.mark.asyncio
async def test_list_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(1, '?offset=0&limit=4'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    print(json_response)
    list_jobs = JobList.parse_obj(json_response)
    assert list_jobs.count == 13
    assert list_jobs.offset == 0
    assert list_jobs.limit == 4
    assert len(list_jobs.records) == 4
    # Check sort order by job id
    for idx, job in enumerate(list_jobs.records):
        assert job.id == idx + 1



@pytest.mark.asyncio
async def test_list_ok_with_state_filter(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(1, '?state=SUCCEEDED&state=FAILED'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    list_jobs = JobList.parse_obj(json_response)
    assert list_jobs.count == len(list_jobs.records) == 2
    assert list_jobs.offset == 0
    assert list_jobs.limit == 1000
    assert list_jobs.records[0].command_with_parameters == ['echo', '8']
    assert list_jobs.records[1].command_with_parameters == ['echo', '13']


@pytest.mark.asyncio
async def test_list_unknown_id(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(9999, ''))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Job order ID : 9999'}


@pytest.mark.asyncio
async def test_list_bad_id_type(async_app_client, postgresql_database):
    for job_id in ('ABC', None, 0.5):
        response = await async_app_client.get(ENDPOINT.format(job_id, '?offset=0&limit=1000'))
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.asyncio
async def test_list_unknown_state(async_app_client, postgresql_database):
    response = await async_app_client.get(ENDPOINT.format(1, '?offset=0&limit=1000&state=EXOTIC'))
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.asyncio
async def test_list_forbidden_job_order(postgresql_database, async_app_client_user1, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client_user1.get(ENDPOINT.format(2, ''))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN, json_response
    assert json_response == {'detail': 'Job order ID : 2 does not belong to the authenticated user'}
