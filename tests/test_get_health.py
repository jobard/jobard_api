"""Test get_health request."""

import pytest
from fastapi_healthcheck.enum import HealthCheckStatusEnum

from jobard_api.monitoring.dto import MonitoringResultDatabaseEntity, MonitoringResultDaemonEntity, \
    MonitoringResultConnectionEntity, MonitoringResult
from jobard_api.monitoring.service import get_monitoring
from tests.database import execute_queries_from_file


@pytest.mark.parametrize(
    'expected_res',
    [
        (
                MonitoringResult(status=HealthCheckStatusEnum.UNHEALTHY,
                                 database_entity=MonitoringResultDatabaseEntity(status=HealthCheckStatusEnum.HEALTHY),
                                 daemon_entities=[
                                     MonitoringResultDaemonEntity(daemon_id='0',
                                                                  host_connections=[
                                                                      MonitoringResultConnectionEntity(
                                                                          host_connection='local1',
                                                                          status=HealthCheckStatusEnum.UNHEALTHY
                                                                      ),
                                                                      MonitoringResultConnectionEntity(
                                                                          host_connection='local2',
                                                                          status=HealthCheckStatusEnum.HEALTHY
                                                                      ),
                                                                      MonitoringResultConnectionEntity(
                                                                          host_connection='local3',
                                                                          status=HealthCheckStatusEnum.UNHEALTHY
                                                                      ),
                                                                      MonitoringResultConnectionEntity(
                                                                          host_connection='local4',
                                                                          status=HealthCheckStatusEnum.UNHEALTHY
                                                                      ),
                                                                  ]
                                                                  )
                                 ]
                                 )
        )
    ]
)
@pytest.mark.asyncio
async def test_get_health(postgresql_database, healthcheck_sample, expected_res):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, healthcheck_sample)
    # retrieve in database monitoring results
    monitoring_list = await get_monitoring()

    assert monitoring_list is not None
    assert len(monitoring_list.daemon_entities) == len(expected_res.daemon_entities)
    assert monitoring_list.database_entity.status == expected_res.database_entity.status

    print('\nresult monitoring : {0}'.format(monitoring_list.json()))
    print('\nresult database : {0}'.format(monitoring_list.database_entity.json()))

    for daemon in monitoring_list.daemon_entities:
        print('\nresult daemon : {0}'.format(daemon.json()))
        daemon_res = next((x for x in expected_res.daemon_entities if x.daemon_id == daemon.daemon_id), None)
        assert daemon.daemon_id == daemon_res.daemon_id
        for connection in daemon.host_connections:
            connection_res = next(
                (x for x in daemon_res.host_connections if x.host_connection == connection.host_connection), None)
            assert connection_res is not None
            assert connection.status == connection_res.status
