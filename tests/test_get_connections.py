"""Test get_connections request"""

import pytest

from jobard_api.monitoring.dto import RemoteConnection, ConnectionList
from jobard_api.monitoring.service import get_connections
from tests.database import execute_queries_from_file


@pytest.mark.parametrize(
    'expected_res',
    [
        (
                ConnectionList(records=[
                    RemoteConnection(daemon_id='0',
                                     name='connection1',
                                     cluster_id=1,
                                     ),
                    RemoteConnection(daemon_id='0',
                                     name='connection2',
                                     cluster_id=2,
                                     ),
                    RemoteConnection(daemon_id='0',
                                     name='connection3',
                                     cluster_id=3,
                                     ),
                    RemoteConnection(daemon_id='1',
                                     name='connection1',
                                     cluster_id=1,
                                     ),
                    RemoteConnection(daemon_id='1',
                                     name='connection2',
                                     cluster_id=2,
                                     ),
                    RemoteConnection(daemon_id='2',
                                     name='connection3',
                                     cluster_id=3,
                                     ),
                    RemoteConnection(daemon_id='2',
                                     name='connection4',
                                     cluster_id=4,
                                     ),
                    RemoteConnection(daemon_id='2',
                                     name='connection5',
                                     cluster_id=5,
                                     ),
                ]
                )
        )
    ]
)
@pytest.mark.asyncio
async def test_get_connections(postgresql_database, connections_sample, expected_res):

    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)

    # retrieve in database connections list
    connections_list = await get_connections(offset=0, limit=1000)
    assert connections_list is not None

    # compare results with expected
    assert len(connections_list.records) == len(expected_res.records)

    for idx, conn in enumerate(connections_list.records):
        print('\nconnection : {0}'.format(conn.json()))
        assert conn == expected_res.records[idx]

