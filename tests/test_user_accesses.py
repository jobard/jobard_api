"""Test post endpoint."""

import pytest
from starlette import status

from tests.database import execute_queries_from_file

ENDPOINT = '/user-accesses/'

@pytest.mark.asyncio
async def test_create_user_accesses_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'user': {
                'username': 'username',
                'email': 'email',
                'remote_username': 'remote_username',
                'password': 'password',
                'client_key': 'client_key',
                'unix_uid': 1234
            },
            'access_token': 'access_token',
            'cluster_accesses': [
                {
                    'cluster_access_name': 'cluster_access_name',
                    'cluster_id': 1,
                    'log_root': 'log_root',
                    'remote_app_path': 'remote_app_path',
                    'entry_point_wrapper': 'entry_point_wrapper',
                    'entry_point': 'entry_point'
                },
                {
                    'cluster_access_name': 'cluster_access_name2',
                    'cluster_id': 2,
                    'log_root': 'log_root2',
                    'remote_app_path': 'remote_app_path2',
                    'entry_point_wrapper': 'entry_point_wrapper2',
                    'entry_point': 'entry_point2'
                }
            ]
        }
    )

    assert response.status_code == status.HTTP_201_CREATED

@pytest.mark.asyncio
async def test_create_user_accesses_user_already_registered(postgresql_database, connections_sample, async_app_client_admin):
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'user': {
                'username': 'user_test1',
                'email': 'email',
                'remote_username': 'remote_username',
                'password': 'password',
                'client_key': 'client_key',
                'unix_uid': 1234
            },
            'access_token': 'access_token',
            'cluster_accesses': [
                {
                    'cluster_access_name': 'cluster_access_name',
                    'cluster_id': 1,
                    'log_root': 'log_root',
                    'remote_app_path': 'remote_app_path',
                    'entry_point_wrapper': 'entry_point_wrapper',
                    'entry_point': 'entry_point'
                },
                {
                    'cluster_access_name': 'cluster_access_name2',
                    'cluster_id': 2,
                    'log_root': 'log_root2',
                    'remote_app_path': 'remote_app_path2',
                    'entry_point_wrapper': 'entry_point_wrapper2',
                    'entry_point': 'entry_point2'
                }
            ]
        }
    )

    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'username : user_test1 already registered for object Jobard user'}

@pytest.mark.asyncio
async def test_create_user_accesses_token_already_registered(postgresql_database, connections_sample, async_app_client_admin):
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'user': {
                'username': 'username',
                'email': 'email',
                'remote_username': 'remote_username',
                'password': 'password',
                'client_key': 'client_key',
                'unix_uid': 1234
            },
            'access_token': 'jobard_access_token2',
            'cluster_accesses': [
                {
                    'cluster_access_name': 'cluster_access_name',
                    'cluster_id': 1,
                    'log_root': 'log_root',
                    'remote_app_path': 'remote_app_path',
                    'entry_point_wrapper': 'entry_point_wrapper',
                    'entry_point': 'entry_point'
                },
                {
                    'cluster_access_name': 'cluster_access_name2',
                    'cluster_id': 2,
                    'log_root': 'log_root2',
                    'remote_app_path': 'remote_app_path2',
                    'entry_point_wrapper': 'entry_point_wrapper2',
                    'entry_point': 'entry_point2'
                }
            ]
        }
    )

    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'access_token : jobard_access_token2 already registered for object Access token'}

@pytest.mark.asyncio
async def test_create_user_accesses_cluster_access_already_registered(postgresql_database, connections_sample, async_app_client_admin):
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'user': {
                'username': 'username',
                'email': 'email',
                'remote_username': 'remote_username',
                'password': 'password',
                'client_key': 'client_key',
                'unix_uid': 1234
            },
            'access_token': 'acces_token',
            'cluster_accesses': [
                {
                    'cluster_access_name': 'access_user1_cluster_local',
                    'cluster_id': 1,
                    'log_root': 'log_root',
                    'remote_app_path': 'remote_app_path',
                    'entry_point_wrapper': 'entry_point_wrapper',
                    'entry_point': 'entry_point'
                },
                {
                    'cluster_access_name': 'cluster_access_name2',
                    'cluster_id': 2,
                    'log_root': 'log_root2',
                    'remote_app_path': 'remote_app_path2',
                    'entry_point_wrapper': 'entry_point_wrapper2',
                    'entry_point': 'entry_point2'
                }
            ]
        }
    )

    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'cluster_access_name : access_user1_cluster_local already registered for object Cluster access'}

@pytest.mark.asyncio
async def test_create_user_accesses_cluster_unknown(postgresql_database, connections_sample, async_app_client_admin):
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'user': {
                'username': 'username',
                'email': 'email',
                'remote_username': 'remote_username',
                'password': 'password',
                'client_key': 'client_key',
                'unix_uid': 1234
            },
            'access_token': 'acces_token',
            'cluster_accesses': [
                {
                    'cluster_access_name': 'cluster_access_name',
                    'cluster_id': 10,
                    'log_root': 'log_root',
                    'remote_app_path': 'remote_app_path',
                    'entry_point_wrapper': 'entry_point_wrapper',
                    'entry_point': 'entry_point'
                },
                {
                    'cluster_access_name': 'cluster_access_name2',
                    'cluster_id': 2,
                    'log_root': 'log_root2',
                    'remote_app_path': 'remote_app_path2',
                    'entry_point_wrapper': 'entry_point_wrapper2',
                    'entry_point': 'entry_point2'
                }
            ]
        }
    )

    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'Unknown Cluster ID : 10'}
