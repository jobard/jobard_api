"""Test submit endpoint."""
import json

import pytest
from starlette import status

from jobard_api.docker.dto import DockerImageInput
from jobard_api.docker.service import create_docker_image
from jobard_api.monitoring.dto import RemoteConnectionInput
from jobard_api.monitoring.service import create_connection
from tests.database import retrieve_query_result

JOB_ORDERS_ENDPOINT = '/job-orders/'
DOCKER_IMAGES_ENDPOINT = '/docker-images/'
DELETE_DOCKER_IMAGES_ENDPOINT = '/docker-images/{0}'


@pytest.mark.asyncio
async def test_submit_job_order_ok(postgresql_database, async_app_client):

    # Create remote test connection
    await create_connection(connection_input=RemoteConnectionInput(name='localhost', cluster_id=1))

    # Create the Job order
    response = await async_app_client.post(JOB_ORDERS_ENDPOINT, json={
        'name': 'test',
        'command': ['my_command', 'subcommand', '-v'],
        'arguments': [[1], [2], ['3']],
        'connection': 'localhost',
    })
    json_response = response.json()
    query_result = await retrieve_query_result(
        postgresql_database,
        'SELECT * FROM JOB_ORDER',
    )
    assert response.status_code == status.HTTP_201_CREATED, json_response
    assert json_response == query_result[0]['id']
    assert query_result[0]['command'] == ['my_command', 'subcommand', '-v']
    assert query_result[0]['arguments'] == [['1'], ['2'], ['3']]
    assert query_result[0]['name'] == 'test'


@pytest.mark.asyncio
async def test_submit_job_order_full(postgresql_database, async_app_client):

    # Create remote test connection
    await create_connection(connection_input=RemoteConnectionInput(name='localhost', cluster_id=1))

    # Create the Job order
    response = await async_app_client.post(JOB_ORDERS_ENDPOINT, json={
        'name': 'my_job',
        'command': ['my_command'],
        'arguments': [
            ['value1'],
            ['value2'],
            ['value100'],
        ],
        'cores': 2,
        'memory': '2G',
        'wallime': '01:30:00',
        'connection': 'localhost',
        'split': '10',
        'priority': 10,
        'job_extra': [
            'extra1',
            'extra2',
        ],
        'env_extra': {
            'additionalProp1': 'env1',
            'additionalProp2': 'env2',
            'additionalProp3': 'env3',
        },
    })
    json_response = response.json()
    query_result = await retrieve_query_result(postgresql_database, 'SELECT MAX(id) from JOB_ORDER')
    last_created_id = query_result[0]['max']
    assert response.status_code == status.HTTP_201_CREATED, json_response
    assert json_response == last_created_id


@pytest.mark.asyncio
async def test_submit_invalid_memory(async_app_client, postgresql_database):
    response = await async_app_client.post(JOB_ORDERS_ENDPOINT, json={'command': ['fake'], 'memory': '512'})
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.asyncio
async def test_submit_invalid_walltime(async_app_client, postgresql_database):
    response = await async_app_client.post(JOB_ORDERS_ENDPOINT, json={'command': ['fake'], 'walltime': '00:05'})
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.asyncio
async def test_submit_invalid_split(async_app_client, postgresql_database):
    response = await async_app_client.post(JOB_ORDERS_ENDPOINT, json={'command': ['fake'], 'split': 'wrong_value'})
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.asyncio
async def test_submit_job_order_wrong_cluster(postgresql_database, async_app_client):

    # Create remote test connection
    await create_connection(connection_input=RemoteConnectionInput(name='swarm', cluster_id=2))

    # Create the Job order
    response = await async_app_client.post(JOB_ORDERS_ENDPOINT, json={
        'name': 'test',
        'command': ['my_command', 'subcommand', '-v'],
        'arguments': [[1], [2], ['3']],
        'connection': 'swarm',
    })
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.asyncio
async def test_submit_job_order_wrong_image(postgresql_database, async_app_client):

    # Create remote test connection
    await create_connection(connection_input=RemoteConnectionInput(name='swarm', cluster_id=2))

    # Create the Job order
    response = await async_app_client.post(JOB_ORDERS_ENDPOINT, json={
        'name': 'test',
        'command': ['my_command', 'subcommand', '-v'],
        'arguments': [[1], [2], ['3']],
        'connection': 'swarm',
        'image_id': 9999,
    })
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.asyncio
async def test_submit_job_order_forbidden_image(postgresql_database, async_app_client, async_app_client_user1):

    # Create remote test connection
    await create_connection(connection_input=RemoteConnectionInput(name='swarm', cluster_id=2))

    # Create docker image for another user
    docker_image_response = await async_app_client_user1.post(
        DOCKER_IMAGES_ENDPOINT,
        json={
            'name': 'image_name',
            'url': 'image_url',
            'is_test': True
        }
    )
    image_id = json.loads(docker_image_response.content)['image_id']

    # Create the Job order
    response = await async_app_client.post(JOB_ORDERS_ENDPOINT, json={
        'name': 'test',
        'command': ['my_command', 'subcommand', '-v'],
        'arguments': [[1], [2], ['3']],
        'connection': 'swarm',
        'image_id': image_id,
    })
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert json_response == {'detail': 'Docker image ID : 1 does not belong to the authenticated user'}


@pytest.mark.asyncio
async def test_submit_job_order_image_ok(postgresql_database, async_app_client):
    # Create remote test connection
    await create_connection(connection_input=RemoteConnectionInput(name='swarm', cluster_id=2))

    # Create docker image for the submitting user.
    await create_docker_image(image_input=DockerImageInput(name='image_user2', url='url'), user_id=2)

    # Create the Job order
    response = await async_app_client.post(JOB_ORDERS_ENDPOINT, json={
        'name': 'test',
        'command': ['my_command', 'subcommand', '-v'],
        'arguments': [[1], [2], ['3']],
        'connection': 'swarm',
        'image_id': 1,
    })
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.asyncio
async def test_submit_job_order_disabled_image(postgresql_database, async_app_client):

    # Create remote test connection
    await create_connection(connection_input=RemoteConnectionInput(name='swarm', cluster_id=2))

    # Create docker image for the user
    docker_image_response = await async_app_client.post(
        DOCKER_IMAGES_ENDPOINT,
        json={
            'name': 'image_name',
            'url': 'image_url',
            'is_test': True
        }
    )
    image_id = json.loads(docker_image_response.content)

    # Disable docker image
    await async_app_client.delete(DELETE_DOCKER_IMAGES_ENDPOINT.format(image_id))

    # Create the Job order
    response = await async_app_client.post(JOB_ORDERS_ENDPOINT, json={
        'name': 'test',
        'command': ['my_command', 'subcommand', '-v'],
        'arguments': [[1], [2], ['3']],
        'connection': 'swarm',
        'image_id': image_id,
    })
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Docker image ID : {0}'. format(image_id)}

@pytest.mark.asyncio
async def test_submit_invalid_command_alias(async_app_client, postgresql_database):
    response = await async_app_client.post(JOB_ORDERS_ENDPOINT, json={'command': ['fake'], 'command_alias': 'command%'})
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

@pytest.mark.asyncio
async def test_submit_invalid_job_log_prefix_arg_idx(async_app_client, postgresql_database):
    response = await async_app_client.post(JOB_ORDERS_ENDPOINT, json={
        'command': ['fake'],
        'arguments': [[1], [2], ['3']],
        'job_log_prefix_arg_idx': 3
    })
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

@pytest.mark.asyncio
async def test_command_alias_and_arg_idx_ok(postgresql_database, async_app_client):

    # Create remote test connection
    await create_connection(connection_input=RemoteConnectionInput(name='localhost', cluster_id=1))

    # Create the Job order
    response = await async_app_client.post(JOB_ORDERS_ENDPOINT, json={
        'name': 'test',
        'command': ['my_command', 'subcommand', '-v'],
        'arguments': [[1], [2], ['3']],
        'connection': 'localhost',
        'command_alias': 'command',
        'job_log_prefix_arg_idx': 0
    })
    json_response = response.json()
    query_result = await retrieve_query_result(
        postgresql_database,
        'SELECT * FROM JOB_ORDER',
    )
    assert response.status_code == status.HTTP_201_CREATED, json_response
    assert json_response == query_result[0]['id']
    assert query_result[0]['command'] == ['my_command', 'subcommand', '-v']
    assert query_result[0]['arguments'] == [['1'], ['2'], ['3']]
    assert query_result[0]['name'] == 'test'
    assert query_result[0]['command_alias'] == 'command'
    assert query_result[0]['job_log_prefix_arg_idx'] == 0
