"""Test get_clusters request"""
from datetime import timedelta

import pytest

from jobard_api.core.types import ClusterType
from jobard_api.monitoring.dto import ClusterList, Cluster
from jobard_api.monitoring.service import get_clusters


@pytest.mark.parametrize(
    'expected_res',
    [
        (
                ClusterList(records=[
                    Cluster(cluster_id='1',
                            cluster_name='local_cluster',
                            type=ClusterType.LOCAL,
                            healthcheck_interval=timedelta(hours=12),
                            ),
                    Cluster(cluster_id='2',
                            cluster_name='swarm_cluster',
                            type=ClusterType.SWARM,
                            healthcheck_interval=timedelta(hours=12),
                            remote_username='user_swarm',
                            client_key='key_swarm',
                            password='password_swarm',
                            ),
                    Cluster(cluster_id='3',
                            cluster_name='pbs_cluster',
                            type=ClusterType.PBS,
                            healthcheck_interval=timedelta(days=1),
                            ),
                    Cluster(cluster_id='4',
                            cluster_name='htcondor_cluster',
                            type=ClusterType.HTCONDOR,
                            healthcheck_interval=timedelta(hours=12),
                            ),
                ]
                )
        )
    ]
)
@pytest.mark.asyncio
async def test_get_clusters(postgresql_database, expected_res):

    # retrieve in database cluster list
    clusters_list = await get_clusters(offset=0, limit=1000)
    assert clusters_list is not None

    # compare results with expected
    assert len(clusters_list.records) == len(expected_res.records)

    for idx, cluster in enumerate(clusters_list.records):
        print('\ncluster : {0}'.format(cluster.json()))
        assert cluster == expected_res.records[idx]

