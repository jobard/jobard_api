"""Test get endpoint."""
import pytest
from starlette import status

from jobard_api.docker.dto import DockerMountPoint
from tests.database import execute_queries_from_file

ENDPOINT = '/docker-mount-points/{0}/details'


@pytest.mark.asyncio
async def test_get_docker_mount_point_details_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(1))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    docker_mount_point = DockerMountPoint.parse_obj(json_response)
    assert docker_mount_point.mount_point_id == 1
    assert docker_mount_point.image_id == 1
    assert docker_mount_point.name == 'mount_point1'
    assert docker_mount_point.source == 'source1'
    assert docker_mount_point.destination == 'destination1'


@pytest.mark.asyncio
async def test_get_docker_mount_point_unknown_mount_point_id(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Docker mount point ID : 9999'}


@pytest.mark.asyncio
async def test_get_docker_mount_point_bad_id_type(postgresql_database, async_app_client, common_sample):
    for mount_point_id in ('ABC', None, 0.5):
        response = await async_app_client.get(ENDPOINT.format(mount_point_id))
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.asyncio
async def test_get_docker_mount_point_forbidden_image(postgresql_database, async_app_client_user1, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client_user1.get(ENDPOINT.format(1))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN, json_response
    assert json_response == {'detail': 'Docker image ID : 1 does not belong to the authenticated user'}


@pytest.mark.asyncio
async def test_get_docker_mount_point_disabled_image(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(7))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Docker image ID : 7'}
