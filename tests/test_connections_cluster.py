"""Test post endpoint."""

import pytest
from starlette import status

from tests.database import execute_queries_from_file

ENDPOINT = '/connections-cluster/'

@pytest.mark.asyncio
async def test_create_connection_cluster_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'name': 'connection_local',
            'daemon_id' : '1',
            'cluster': {
                'cluster_name': 'local',
                'type': 'LOCAL',
                'healthcheck_interval': 3600,
                'remote_username': 'remote_user',
                'password': 'pwd1',
                'client_key': 'key1'
            }
        }
    )
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.asyncio
async def test_create_connection_cluster_connection_already_registered(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'name': 'connection1',
            'daemon_id': '1',
            'cluster': {
                'cluster_name': 'local',
                'type': 'LOCAL',
                'healthcheck_interval': 3600,
                'remote_username': 'remote_user',
                'password': 'pwd1',
                'client_key': 'key1'
            }
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'name,daemon_id : connection1,1 already registered for object Remote connection'}

@pytest.mark.asyncio
async def test_create_connection_cluster_cluster_already_registered(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'name': 'connection_local',
            'daemon_id': '1',
            'cluster': {
                'cluster_name': 'local_cluster',
                'type': 'LOCAL',
                'healthcheck_interval': 3600,
                'remote_username': 'remote_user',
                'password': 'password1',
                'client_key': 'key1',
            }
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'name : local_cluster already registered for object Cluster'}

