"""Common utilities for testing."""
import os
from pathlib import Path

from httpx import AsyncClient, Auth
from pytest import fixture

CONFIG_DIR = Path(__file__).parent.resolve()
os.environ['jobard_api_secrets_dir'] = str(CONFIG_DIR / 'resources' / 'config')

BASE_URL = 'http://localhost:8000'

from jobard_api.conf.database import database
from jobard_api.conf.settings import settings
from jobard_api.main import app
from tests.database import (
    DockerDatabase,
    clean_schema,
    run_migrations,
    init_users,
    JOBARD_TEST_TOKEN_1,
    JOBARD_TEST_TOKEN_2,
)


class JobardAuth(Auth):
    def __init__(self, token):
        self.token = token

    def auth_flow(self, request):
        request.headers['Authorization'] = self.token
        yield request


@fixture()
async def async_app_client():
    """Jobard API test client fixture.

    Yields:
        AsyncClient instance
    """
    async with AsyncClient(
        app=app,
        base_url=BASE_URL,
        auth=JobardAuth(token=f'Bearer {JOBARD_TEST_TOKEN_1}'),
    ) as client:
        yield client


@fixture()
async def async_app_client_user1():
    """Jobard API test client fixture.

    Yields:
        AsyncClient instance
    """
    async with AsyncClient(
        app=app,
        base_url=BASE_URL,
        auth=JobardAuth(token=f'Bearer {JOBARD_TEST_TOKEN_2}'),
    ) as client:
        yield client


@fixture()
async def async_app_client_admin():
    """Jobard API test client fixture with admin user.

    Yields:
        AsyncClient instance
    """
    async with AsyncClient(
        app=app,
        base_url=BASE_URL,
        auth=JobardAuth(token=f'Bearer {settings.get_admin_token()}'),
    ) as client:
        yield client

async def postgresql_docker_database():
    """Manage postgresql docker database.

    Yields:
        databases.Database instance
    """
    dockerdb = DockerDatabase(port=settings.database_dsn.port)
    try:  # noqa: WPS501
        dockerdb.start(initialize_schema=True)
        await database.connect()
        await init_users()
        yield database
        await database.disconnect()
    finally:
        dockerdb.stop()


async def postgresql_existing_database():
    """Manage existing postgresql database.

    Yields:
        databases.Database instance
    """
    await database.connect()
    await clean_schema(database, settings.database_dsn.user)
    run_migrations()
    await init_users()
    yield database
    await clean_schema(database, settings.database_dsn.user)
    await database.disconnect()


@fixture()
async def postgresql_database():
    """Postgresql database fixture.

    Yields:
        databases.Database instance
    """
    manage_docker = os.getenv('MANAGE_POSTGRES_DOCKER', default='True').lower() == 'true'
    postgres_mngt_fct = postgresql_docker_database if manage_docker else postgresql_existing_database
    async for yield_database in postgres_mngt_fct():
        yield yield_database


@fixture()
def resources_dir():
    """Path to resource dir fixture.

    Returns:
        Path to resource dir
    """
    return Path(__file__).parent / 'resources'


@fixture()
def db_resources_dir(resources_dir):
    """Path to database resource dir fixture.

    Args:
        resources_dir: resource dir fixture

    Returns:
        Path to database resource dir
    """
    return resources_dir / 'db'


@fixture()
def common_sample(db_resources_dir):
    """Path to common sample sql file fixture.

    Args:
        db_resources_dir: database resource dir fixture

    Returns:
        Path to common sample sql file
    """
    return db_resources_dir / 'common_sample.sql'


@fixture()
def healthcheck_sample(db_resources_dir):
    """Path to healthcheck sample sql file fixture.

    Args:
        db_resources_dir: database resource dir fixture

    Returns:
        Path to healthcheck sample sql file
    """
    return db_resources_dir / 'healthcheck_sample.sql'


@fixture()
def connections_sample(db_resources_dir):
    """Path to connections sample sql file fixture.

    Args:
        db_resources_dir: database resource dir fixture

    Returns:
        Path to connections sample sql file
    """
    return db_resources_dir / 'connections_sample.sql'
