"""Test post endpoint."""

import pytest
from starlette import status

from jobard_api.users.dto import ClusterAccess, ClusterAccessList, AccessToken, AccessTokenList

ENDPOINT = '/access-tokens/'

@pytest.mark.asyncio
async def test_create_access_token_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'user_id': 2,
            'access_token': 'token2',
        }
    )

    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.asyncio
async def test_create_access_token_already_registered(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'user_id': 2,
            'access_token': 'jobard_access_token2',
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'access_token : jobard_access_token2 already registered for object Access token'}

@pytest.mark.asyncio
async def test_create_access_token_unknown_user_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'user_id': 10,
            'access_token': 'token10',
        }
    )

    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'Unknown User ID : 10'}


@pytest.mark.asyncio
async def test_update_access_token_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'token_id': 1,
            'user_id': 3,
            'access_token': 'token_update',
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK
    # parse and check response
    access_token = AccessToken.parse_obj(json_response)
    assert access_token.token_id == 1
    assert access_token.user_id == 3
    assert access_token.access_token == 'token_update'


@pytest.mark.asyncio
async def test_update_access_token_unknown_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'token_id': 9999,
            'user_id': 3,
            'access_token': 'token_update',
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Access token ID : 9999'}

@pytest.mark.asyncio
async def test_update_access_token_unknown_user(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'token_id': 1,
            'user_id': 10,
            'access_token': 'token_update',
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'Unknown User ID : 10'}

@pytest.mark.asyncio
async def test_update_access_token_already_registered(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'token_id': 1,
            'user_id': 2,
            'access_token': 'jobard_access_token2',
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'access_token : jobard_access_token2 already registered for object Access token'}

@pytest.mark.asyncio
async def test_delete_access_token_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+str(1))
    assert response.status_code == status.HTTP_204_NO_CONTENT


@pytest.mark.asyncio
async def test_delete_access_token_unknown_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+str(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Access token ID : 9999'}

@pytest.mark.asyncio
async def test_get_access_token_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+str(1))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    access_token = AccessToken.parse_obj(json_response)
    assert access_token.token_id == 1
    assert access_token.access_token == 'jobard_access_token1'
    assert access_token.user_id == 2

@pytest.mark.asyncio
async def test_get_access_token_unknown_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+str(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Access token ID : 9999'}

@pytest.mark.asyncio
async def test_get_access_token_bad_id_type(async_app_client_admin, postgresql_database):
    for access_token_id in ('ABC', None, 0.5):
        response = await async_app_client_admin.get(ENDPOINT+str(access_token_id))
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

@pytest.mark.asyncio
async def test_list_access_token_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get('/access-tokens'+str('?offset=0&limit=2'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    access_token_list = AccessTokenList.parse_obj(json_response)
    assert access_token_list.count == 2
    assert access_token_list.offset == 0
    assert access_token_list.limit == 2
    # Check access tokens sort order
    for idx, token in enumerate(access_token_list.records):
        assert token.token_id == idx+1
