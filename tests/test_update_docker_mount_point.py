"""Test put endpoint."""
import pytest
from starlette import status

from jobard_api.docker.dto import DockerMountPoint
from tests.database import execute_queries_from_file

ENDPOINT = '/docker-mount-points/'


@pytest.mark.asyncio
async def test_update_mount_point_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.put(
        ENDPOINT,
        json={
            'mount_point_id': 1,
            'image_id': 1,
            'name': 'my_new_mount_point_name'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK
    # parse and check response
    docker_mount_point = DockerMountPoint.parse_obj(json_response)
    assert docker_mount_point.mount_point_id == 1
    assert docker_mount_point.image_id == 1
    assert docker_mount_point.name == 'my_new_mount_point_name'
    assert docker_mount_point.source == 'source1'
    assert docker_mount_point.destination == 'destination1'


@pytest.mark.asyncio
async def test_update_mount_point_unknown_id(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.put(
        ENDPOINT,
        json={
            'mount_point_id': 9999,
            'image_id': 1,
            'name': 'my_new_mount_point_name'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Docker mount point ID : 9999'}


@pytest.mark.asyncio
async def test_update_mount_point_name_already_registered(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.put(
        ENDPOINT,
        json={
            'mount_point_id': 1,
            'name': 'mount_point2'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'name : mount_point2 already registered for object Docker mount point'}


@pytest.mark.asyncio
async def test_update_mount_point_forbidden(postgresql_database, async_app_client_user1, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client_user1.put(
        ENDPOINT,
        json={
            'mount_point_id': 1,
            'image_id': 1,
            'name': 'my_new_mount_point_name'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN, json_response
    assert json_response == {'detail': 'Docker image ID : 1 does not belong to the authenticated user'}


@pytest.mark.asyncio
async def test_update_mount_point_disabled_image(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.put(
        ENDPOINT,
        json={
            'mount_point_id': 7,
            'image_id': 7,
            'name': 'my_new_mount_point_name'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Docker image ID : 7'}
