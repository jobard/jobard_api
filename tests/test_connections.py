"""Test post endpoint."""

import pytest
from starlette import status

from jobard_api.monitoring.dto import RemoteConnection, ConnectionList
from tests.database import execute_queries_from_file

ENDPOINT = '/connections/'

@pytest.mark.asyncio
async def test_create_connection_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'name': 'connection_local',
            'cluster_id': 1,
            'daemon_id' : '0'
        }
    )

    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.asyncio
async def test_create_connection_already_registered(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'name': 'connection1',
            'cluster_id': 1,
            'daemon_id': '1'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'name,daemon_id : connection1,1 already registered for object Remote connection'}

@pytest.mark.asyncio
async def test_create_connection_unknown_cluster(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'name': 'new_connection',
            'cluster_id': 10,
            'daemon_id': '0'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_424_FAILED_DEPENDENCY
    assert json_response == {'detail': 'Unknown Cluster ID : 10'}

@pytest.mark.asyncio
async def test_update_connection_ok(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'name': 'connection1',
            'cluster_id': 4,
            'daemon_id': '1'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK
    # parse and check response
    connection = RemoteConnection.parse_obj(json_response)
    assert connection.name == 'connection1'
    assert connection.cluster_id == 4
    assert connection.daemon_id == '1'


@pytest.mark.asyncio
async def test_update_connection_unknown_name(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'name': 'noname',
            'cluster_id': 3,
            'daemon_id': '0'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Remote connection Name : name : noname daemon id : 0'}

@pytest.mark.asyncio
async def test_update_connection_unknown_daemon_id(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'name': 'connection_local',
            'cluster_id': 3,
            'daemon_id': '999'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Remote connection Name : name : connection_local daemon id : 999'}

@pytest.mark.asyncio
async def test_update_connection_unknown_cluster(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'name': 'connection2',
            'cluster_id': 10,
            'daemon_id': '0'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_424_FAILED_DEPENDENCY
    assert json_response == {'detail': 'Unknown Cluster ID : 10'}

@pytest.mark.asyncio
async def test_update_disabled_connection(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'name': 'connection5',
            'cluster_id': 1,
            'daemon_id': '3'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Remote connection Name : name : connection5 daemon id : 3'}

@pytest.mark.asyncio
async def test_delete_connection_ok(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+'connection2/0')
    assert response.status_code == status.HTTP_204_NO_CONTENT

@pytest.mark.asyncio
async def test_delete_connection_unknown_name(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+'noname/0')
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Remote connection Name : name : noname daemon id : 0'}

@pytest.mark.asyncio
async def test_delete_connection_unknown_daemon_id(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+'connection1/10')
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Remote connection Name : name : connection1 daemon id : 10'}

@pytest.mark.asyncio
async def test_delete_disabled_connection(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+'connection5/3')
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Remote connection Name : name : connection5 daemon id : 3'}

@pytest.mark.asyncio
async def test_get_connection_ok(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+'connection3/2')
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    connection = RemoteConnection.parse_obj(json_response)
    assert connection.name == 'connection3'
    assert connection.cluster_id == 3
    assert connection.daemon_id == '2'

@pytest.mark.asyncio
async def test_get_connection_unknown_name(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+'noname/0')
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Remote connection Name : name : noname daemon id : 0'}

@pytest.mark.asyncio
async def test_get_connection_unknown_daemon_id(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+'connection3/10')
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Remote connection Name : name : connection3 daemon id : 10'}

@pytest.mark.asyncio
async def test_get_connection_disabled(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+'connection5/3')
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Remote connection Name : name : connection5 daemon id : 3'}

@pytest.mark.asyncio
async def test_list_connection_ok(postgresql_database, connections_sample, async_app_client_admin):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, connections_sample)
    # execute endpoint
    response = await async_app_client_admin.get('/connections'+str('?offset=0&limit=2'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    connection_list = ConnectionList.parse_obj(json_response)
    assert connection_list.count == 8
    assert connection_list.offset == 0
    assert connection_list.limit == 2
