"""Test post endpoint."""

import pytest
from starlette import status

from jobard_api.users import service as users_service
from jobard_api.users.dto import JobardUser, JobardUserList

ENDPOINT = '/users/'

@pytest.mark.asyncio
async def test_create_user_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'username': 'user2',
            'email': 'mail2',
            'remote_username': 'remote2',
            'password': 'password2',
            'client_key': 'key2',
            'unix_uid': 2
        }
    )
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.asyncio
async def test_create_user_already_registered(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'username': 'user_test2',
            'email': 'mail2',
            'remote_username': 'remote2',
            'password': 'password2',
            'client_key': 'key2',
            'unix_uid': 2
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'username : user_test2 already registered for object Jobard user'}

@pytest.mark.asyncio
async def test_update_user_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'user_id': 2,
            'username': 'name_update1',
            'email': 'email_update1',
            'remote_username': 'remote_update1',
            'password': 'password_update1',
            'client_key': 'key_update1',
            'unix_uid': 3
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK
    # parse and check response
    user = JobardUser.parse_obj(json_response)
    assert user.user_id == 2
    assert user.username == 'name_update1'
    assert user.email == 'email_update1'
    assert user.remote_username == 'remote_update1'
    assert user.unix_uid == 3


@pytest.mark.asyncio
async def test_update_user_unknown_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'user_id': 9999,
            'username': 'name_update2',
            'email': 'email_update2',
            'remote_username': 'remote_update2',
            'password': 'password_update2',
            'client_key': 'key_update2',
            'unix_uid': 3
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown User ID : 9999'}


@pytest.mark.asyncio
async def test_update_user_name_already_registered(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'user_id': 2,
            'username': 'user_test2',
            'email': 'email_update2',
            'remote_username': 'remote_update2',
            'password': 'password_update2',
            'client_key': 'key_update2',
            'unix_uid': 3
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'username : user_test2 already registered for object Jobard user'}

@pytest.mark.asyncio
async def test_update_disabled_user(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'user_id': 4,
            'username': 'user_update3',
            'email': 'email_update3',
            'remote_username': 'remote_update3',
            'password': 'password_update3',
            'client_key': 'key_update3',
            'unix_uid': 3
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown User ID : 4'}

@pytest.mark.asyncio
async def test_delete_usre_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+str(3))
    assert response.status_code == status.HTTP_204_NO_CONTENT


@pytest.mark.asyncio
async def test_delete_user_unknown_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+str(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown User ID : 9999'}

@pytest.mark.asyncio
async def test_delete_admin_user_forbidden(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+str(1))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert json_response == {'detail': 'The admin user cannot be deleted'}


@pytest.mark.asyncio
async def test_delete_disabled_user(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+str(4))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown User ID : 4'}


@pytest.mark.asyncio
async def test_get_user_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+str(3))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    user = JobardUser.parse_obj(json_response)
    assert user.user_id == 3
    assert user.username == 'user_test2'
    assert user.email == 'user_test2@user_test.xyz'
    assert user.remote_username == 'remote_user2'
    assert user.unix_uid == 2

@pytest.mark.asyncio
async def test_get_user_pwd_key_ok(postgresql_database, async_app_client_admin):
    # execute service
    user = await users_service.get_user(user_id=3)
    assert user.user_id == 3
    assert user.username == 'user_test2'
    assert user.email == 'user_test2@user_test.xyz'
    assert user.password.get_secret_value() == 'password2'
    assert user.client_key.get_secret_value() == 'key2'
    assert user.remote_username == 'remote_user2'
    assert user.unix_uid == 2

@pytest.mark.asyncio
async def test_get_user_unknown_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+str(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown User ID : 9999'}

@pytest.mark.asyncio
async def test_get_user_bad_id_type(async_app_client_admin, postgresql_database):
    for user_id in ('ABC', None, 0.5):
        response = await async_app_client_admin.get(ENDPOINT+str(user_id))
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

@pytest.mark.asyncio
async def test_get_user_disabled(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+str(4))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown User ID : 4'}

@pytest.mark.asyncio
async def test_list_user_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get('/users'+str('?offset=0&limit=2'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    users_list = JobardUserList.parse_obj(json_response)
    assert users_list.count == 3
    assert users_list.offset == 0
    assert users_list.limit == 2
    # Check users sort order
    for idx, user in enumerate(users_list.records):
        assert user.user_id == idx + 1
