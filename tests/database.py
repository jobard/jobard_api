"""Database utilities for testing."""
import time
from datetime import timedelta
from pathlib import Path
from typing import Iterable, List, Mapping, Optional

import docker
from alembic import command
from alembic.config import Config
from databases import Database
from docker.models.containers import Container

from jobard_api.core.exceptions import JobardError
from jobard_api.core.types import ClusterType
from jobard_api.monitoring.dto import ClusterInput
from jobard_api.monitoring.service import create_cluster
from jobard_api.users.service import create_cluster_access, delete_cluster_access
from jobard_api.users.dto import ClusterAccessInput
from jobard_api.routers.clusters import delete_cluster
from jobard_api.users.dto import JobardUserInput, AccessTokenInput
from jobard_api.users.service import create_user, create_access_token, delete_user

JOBARD_TEST_TOKEN_1 = 'jobard_access_token1'
JOBARD_TEST_TOKEN_2 = 'jobard_access_token2'


async def init_users():
    """Initialize the database with users"""

    # Create clusters
    await create_cluster(
        cluster_input=ClusterInput(cluster_name='local_cluster', type=ClusterType.LOCAL, healthcheck_interval=timedelta(hours=12)))
    await create_cluster(
        cluster_input=ClusterInput(
            cluster_name='swarm_cluster',
            type=ClusterType.SWARM,
            healthcheck_interval=timedelta(hours=12),
            remote_username='user_swarm',
            client_key='key_swarm',
            password='password_swarm',
        )
    )
    await create_cluster(
        cluster_input=ClusterInput(cluster_name='pbs_cluster', type=ClusterType.PBS, healthcheck_interval=timedelta(days=1)))
    await create_cluster(
        cluster_input=ClusterInput(cluster_name='htcondor_cluster', type=ClusterType.HTCONDOR, healthcheck_interval=timedelta(seconds=43200)))
    await create_cluster(
        cluster_input=ClusterInput(cluster_name='kube_cluster', type=ClusterType.KUBE, healthcheck_interval=timedelta(days=1),
                              remote_username='user_kube', client_key='key_kube', password='password_swarm'))
    await delete_cluster(cluster_id=5)

    # Create users
    await create_user(
        user_input=JobardUserInput(username='admin', email='email_admin', remote_username='remote_admin')
    )

    await create_user(
        user_input=JobardUserInput(
            username='user_test1', email='user_test1@user_test.xyz',
            remote_username='remote_user1', password='password1', client_key='key1')
    )

    await create_user(
        user_input=JobardUserInput(
            username='user_test2', email='user_test2@user_test.xyz',
            remote_username='remote_user2', password='password2', client_key='key2',
            unix_uid = 2
        ))

    await create_user(
        user_input=JobardUserInput(
            username='user_test3', email='user_test3@user_test.xyz',
            remote_username='remote_user3', password='password3', client_key='key3',
            unix_uid = 3
        ))
    await delete_user(user_id=4)

    # Create cluster access
    await create_cluster_access(cluster_access_input=ClusterAccessInput(
            cluster_access_name='access_user1_cluster_local',
            user_id=2, cluster_id=1,
            log_root=None, entry_point='jobard-remote-dask'))

    await create_cluster_access(cluster_access_input=ClusterAccessInput(
            cluster_access_name='access_user1_cluster_swarm',
            user_id=2, cluster_id=2,
            log_root=None, entry_point='jobard-remote-dask'))

    await create_cluster_access(cluster_access_input=ClusterAccessInput(
            cluster_access_name='access_user2_cluster_local',
            user_id=1, cluster_id=1,
            log_root=None, entry_point='jobard-remote-dask'))

    await create_cluster_access(cluster_access_input=ClusterAccessInput(
        cluster_access_name='access_disabled',
        user_id=1, cluster_id=1,
        log_root=None, entry_point='jobard-remote-dask'))

    await delete_cluster_access(cluster_access_id=4)

    # Create user access tokens
    await create_access_token(
        access_token_input=AccessTokenInput(user_id=2, access_token=JOBARD_TEST_TOKEN_1))
    await create_access_token(
        access_token_input=AccessTokenInput(user_id=3, access_token=JOBARD_TEST_TOKEN_2))


async def clean_schema(database: Database, db_user: str = 'jobard') -> None:
    """Recreate an empty schema.

    Args:
        database: database connection
        db_user: database user
    """
    queries = (
        'DROP SCHEMA public CASCADE',
        'CREATE SCHEMA public',
        'GRANT ALL ON SCHEMA public TO {0}'.format(db_user),
        'GRANT ALL ON SCHEMA public TO public',
    )
    await execute_queries(database, queries)


def run_migrations(revision: str = 'head', downgrade: bool = False) -> None:
    """Run alembic migration.

    Args:
        revision: revision index. Default = 'head'
        downgrade: downgrade to the revision. Default = False
    """
    root_directory = Path(__file__).parent.parent
    alembic_cfg = Config(str(root_directory / 'alembic.ini'))
    alembic_cfg.set_main_option('script_location', str(root_directory / 'jobard_api' / 'core' / 'alembic'))
    if downgrade is False:
        command.upgrade(alembic_cfg, revision)
    else:
        command.downgrade(alembic_cfg, revision)


async def retrieve_query_result(database: Database, query: str) -> List[Mapping]:
    """Retrieve a query results.

    Args:
        database: databases.Database instance
        query: query to execute

    Returns:
        List of fields
    """
    rows = []
    for row in await database.fetch_all(query):
        rows.append({field_key: field_value for field_key, field_value in row.items()})
    return rows


async def execute_queries(database: Database, queries: Iterable[str]) -> None:
    """Execute queries on jobard database. For example, to load sample data.

    Args:
        database: databases.Database instance
        queries: list of queries to execute
    """
    for query in queries:
        await database.execute(query)


async def execute_queries_from_file(database: Database, sql_file_path: Path) -> None:
    """Execute queries on jobard database. For example, to load sample data.

    Args:
        database: databases.Database instance
        sql_file_path: file containing sql instructions
    """
    with open(sql_file_path) as fd:
        await execute_queries(database, fd.readlines())


class DockerDatabase(object):
    """Docker postgresql database wrapper."""

    def __init__(
        self,
        postgres_version: str = '13',
        port: int = 5432,
        db_schema_user: str = 'jobard',
        container_name: str = 'jobard_postgres',
    ):
        """Initialize test database.

        Args:
            postgres_version: version of postgresql server
            port: port to start postgresql server
            db_schema_user: user, password and schema name
            container_name: name of the container
        """
        self.postgres_version = postgres_version
        self.port = port
        self.db_schema_user = db_schema_user
        self.container_name = container_name

        self.docker_client = docker.from_env()
        self.container: Optional[Container] = None

        self.stop_existing_containers()

    def stop_existing_containers(self):
        """Remove existing jobard_posgres containers."""
        for container in self.docker_client.containers.list(filters={'name': self.container_name}):
            container.remove(force=True)

    def start(self, initialize_schema: bool = True):
        """Start a postgresql server in a docker.

        Args:
            initialize_schema: if true, initialize database schema

        Raises:
            JobardError: if the database failed to be started
        """
        self.container = self.docker_client.containers.run(
            'postgres:{0}'.format(self.postgres_version),
            ports={'5432/tcp': '{0}/tcp'.format(self.port)},
            environment={
                'POSTGRES_USER': self.db_schema_user,
                'POSTGRES_PASSWORD': self.db_schema_user,
                'POSTGRES_DB': self.db_schema_user,
            },
            detach=True,
            name=self.container_name,
        )

        if not self.wait_running():
            raise JobardError('The database failed to be started')

        if initialize_schema:
            run_migrations()


    def wait_running(self, timeout: int = 120, delay: float = 1) -> bool:
        """Wait until postgresql database runs.

        Args:
            delay: delay between two checks
            timeout: time until check stop

        Returns:
            bool: True if database is running, otherwise False
        """
        elapsed_time = 0
        while elapsed_time < timeout:
            if self.container.status == 'running':
                postgresql_service_status, output = self.container.exec_run('pg_isready -U postgres')
                if postgresql_service_status == 0:
                    time.sleep(delay)
                    return True
            else:
                self.container.reload()

            time.sleep(delay)
            elapsed_time += delay

        return False

    def stop(self):
        """Stop a postgresql container if exists."""
        if self.container:
            # Add v option to suppress volume associated to the container
            self.container.remove(force=True, v=True)
            self.container = None
