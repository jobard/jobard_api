"""Test cancel endpoint."""

import pytest
from starlette import status

from tests.database import execute_queries_from_file


@pytest.mark.asyncio
async def test_cancel_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.delete('/job-orders/3')
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    assert json_response == {'detail': 'Cancellation for JobOrder 3 successfully registered'}


@pytest.mark.asyncio
async def test_cancel_unknown_id(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.delete('/job-orders/9999')
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Job order ID : 9999'}


@pytest.mark.asyncio
async def test_cancel_bad_id_type(postgresql_database, async_app_client):
    for job_id in ('ABC', None, 0.5):
        response = await async_app_client.delete('/job-orders/{0}'.format(job_id))
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.asyncio
async def test_cancel_non_cancellable_state(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.delete('/job-orders/2')
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST, json_response
    assert json_response == {'detail': 'Job order state SUCCEEDED does not allow cancellation'}


@pytest.mark.asyncio
async def test_cancel_wrong_user(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.delete('/job-orders/4')
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN, json_response
    assert json_response == {'detail': 'Job order ID : 4 does not belong to the authenticated user'}
