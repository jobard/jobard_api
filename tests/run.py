"""Run Jobard API for manual tests."""
import argparse
import logging

import uvicorn

from jobard_api.conf.settings import settings
from tests.database import DockerDatabase


def run(
    start_database: bool = True,
    database_port: int = 5432,
):

    logger = logging.getLogger()
    database: DockerDatabase = DockerDatabase(port=database_port)

    try:
        if start_database:
            logger.info('Start jobard database container')
            database.start(initialize_schema=True)
            logger.info('Container : {0}'.format(database.container))
        # launch webapp
        uvicorn.run('jobard_api.main:app', host='127.0.0.1', port=8000, reload=True)
    except Exception as error:
        logger.error('An error occurred in jobard api application : {0}'.format(error))
    finally:
        if start_database:
            logger.info('Stop container')
            database.stop()


def main():
    logging.basicConfig()

    parser = argparse.ArgumentParser()
    parser.add_argument('--start-database', dest='start_database', action='store_true')
    parser.add_argument('--no-start-database', dest='start_database', action='store_false')
    parser.set_defaults(start_database=True)

    parser.add_argument('--database-port', type=int, default=settings.database_dsn.port)

    options = parser.parse_args()

    run(**vars(options))  # noqa: WPS421


if __name__ == '__main__':
    main()
