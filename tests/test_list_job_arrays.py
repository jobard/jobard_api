"""Test get_job endpoint."""

import pytest
from starlette import status

from jobard_api.jobs.dto import JobArrayList
from tests.database import execute_queries_from_file

ENDPOINT = '/job-orders/{0}/arrays'


@pytest.mark.asyncio
async def test_get_job_arrays_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(1, '?offset=0&limit=4'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    list_job_arrays = JobArrayList.parse_obj(json_response)
    assert list_job_arrays.count == len(list_job_arrays.records) == 3
    assert list_job_arrays.offset == 0
    assert list_job_arrays.limit == 1000
    # Check sort order by job array id
    for idx, ja in enumerate(list_job_arrays.records):
        assert ja.id == idx+1

@pytest.mark.asyncio
async def test_arguments_ok_with_state_filter(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(1, '?state=SUCCEEDED&state=FAILED'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    list_job_arrays = JobArrayList.parse_obj(json_response)
    assert list_job_arrays.count == len(list_job_arrays.records) == 3
    assert list_job_arrays.offset == 0
    assert list_job_arrays.limit == 1000


@pytest.mark.asyncio
async def test_get_job_array_unknown_id(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Job order ID : 9999'}


@pytest.mark.asyncio
async def test_get_job_array_bad_id_type(async_app_client, postgresql_database):
    for job_id in ('ABC', None, 0.5):
        response = await async_app_client.get(ENDPOINT.format(job_id))
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.asyncio
async def test_get_job_array_forbidden_job_order(postgresql_database, async_app_client_user1, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client_user1.get(ENDPOINT.format(2))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN, json_response
    assert json_response == {'detail': 'Job order ID : 2 does not belong to the authenticated user'}
