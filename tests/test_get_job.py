"""Test get_job endpoint."""

import pytest
from starlette import status

from jobard_api.core.types import State
from jobard_api.jobs.dto import Job
from tests.database import execute_queries_from_file

ENDPOINT = '/jobs/{0}'


@pytest.mark.asyncio
async def test_get_job_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(1))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    job = Job.parse_obj(json_response)
    assert job.id == 1
    assert job.command_with_parameters == ['echo', '1']
    assert job.state == State.NEW
    assert job.created is not None
    assert job.updated is not None


@pytest.mark.asyncio
async def test_get_job_unknown_id(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Job ID : 9999'}


@pytest.mark.asyncio
async def test_get_job_bad_id_type(async_app_client, postgresql_database):
    for job_id in ('ABC', None, 0.5):
        response = await async_app_client.get(ENDPOINT.format(job_id))
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.asyncio
async def test_get_job_forbidden_job_order(postgresql_database, async_app_client_user1, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client_user1.get(ENDPOINT.format(2))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN, json_response
    assert json_response == {'detail': 'Job order ID : 1 does not belong to the authenticated user'}
