"""Test post endpoint."""

import pytest
from starlette import status

from jobard_api.users.dto import ClusterAccess, ClusterAccessList

ENDPOINT = '/cluster-accesses/'

@pytest.mark.asyncio
async def test_create_cluster_access_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'cluster_access_name': 'cluster_access_local',
            'user_id': 2,
            'cluster_id' : 1,
            'log_root': 'log_root1',
            'remote_app_path': 'remote_app_path1',
            'entry_point_wrapper': 'entry_point_wrapper1',
            'entry_point': 'entry_point1'
        }
    )
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.asyncio
async def test_create_cluster_access_already_registered(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'cluster_access_name': 'access_user1_cluster_local',
            'user_id': 2,
            'cluster_id': 1,
            'log_root': 'log_root1',
            'remote_app_path': 'remote_app_path1',
            'entry_point_wrapper': 'entry_point_wrapper1',
            'entry_point': 'entry_point1'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'cluster_access_name : access_user1_cluster_local already registered for object Cluster access'}

@pytest.mark.asyncio
async def test_create_cluster_access_unknown_cluster_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'cluster_access_name': 'access_user1_cluster_local',
            'user_id': 2,
            'cluster_id': 10,
            'log_root': 'log_root1',
            'remote_app_path': 'remote_app_path1',
            'entry_point_wrapper': 'entry_point_wrapper1',
            'entry_point': 'entry_point1'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'Unknown Cluster ID : 10'}

@pytest.mark.asyncio
async def test_create_cluster_access_unknown_user_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'cluster_access_name': 'access_user1_cluster_local',
            'user_id': 10,
            'cluster_id': 1,
            'log_root': 'log_root1',
            'remote_app_path': 'remote_app_path1',
            'entry_point_wrapper': 'entry_point_wrapper1',
            'entry_point': 'entry_point1'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'Unknown User ID : 10'}

@pytest.mark.asyncio
async def test_update_cluster_access_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'cluster_access_id' : 1,
            'cluster_access_name': 'access_user1_cluster_local_update',
            'user_id': 3,
            'cluster_id': 2,
            'log_root': 'log_root_update',
            'remote_app_path': 'remote_app_path_update',
            'entry_point_wrapper': 'entry_point_wrapper_update',
            'entry_point': 'entry_point_update'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK
    # parse and check response
    cluster_access = ClusterAccess.parse_obj(json_response)
    assert cluster_access.cluster_access_id == 1
    assert cluster_access.cluster_access_name == 'access_user1_cluster_local_update'
    assert cluster_access.user_id == 3
    assert cluster_access.cluster_id == 2
    assert cluster_access.log_root == 'log_root_update'
    assert cluster_access.remote_app_path == 'remote_app_path_update'
    assert cluster_access.entry_point_wrapper == 'entry_point_wrapper_update'
    assert cluster_access.entry_point == 'entry_point_update'


@pytest.mark.asyncio
async def test_update_cluster_access_unknown_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'cluster_access_id': 9999,
            'cluster_access_name': 'access_user1_cluster_local_update',
            'user_id': 3,
            'cluster_id': 2,
            'log_root': 'log_root_update',
            'remote_app_path': 'remote_app_path_update',
            'entry_point_wrapper': 'entry_point_wrapper_update',
            'entry_point': 'entry_point_update'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Cluster Access ID : 9999'}

@pytest.mark.asyncio
async def test_update_cluster_access_unknown_user(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'cluster_access_id': 1,
            'cluster_access_name': 'access_user1_cluster_local_update',
            'user_id': 10,
            'cluster_id': 2,
            'log_root': 'log_root_update',
            'remote_app_path': 'remote_app_path_update',
            'entry_point_wrapper': 'entry_point_wrapper_update',
            'entry_point': 'entry_point_update'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'Unknown User ID : 10'}

@pytest.mark.asyncio
async def test_update_cluster_access_unknown_cluster(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'cluster_access_id': 1,
            'cluster_access_name': 'access_user1_cluster_local_update',
            'user_id': 3,
            'cluster_id': 10,
            'log_root': 'log_root_update',
            'remote_app_path': 'remote_app_path_update',
            'entry_point_wrapper': 'entry_point_wrapper_update',
            'entry_point': 'entry_point_update'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'Unknown Cluster ID : 10'}

@pytest.mark.asyncio
async def test_update_cluster_access_name_already_registered(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'cluster_access_id': 1,
            'cluster_access_name': 'access_user1_cluster_swarm',
            'user_id': 3,
            'cluster_id': 2,
            'log_root': 'log_root_update',
            'remote_app_path': 'remote_app_path_update',
            'entry_point_wrapper': 'entry_point_wrapper_update',
            'entry_point': 'entry_point_update'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'cluster_access_name : access_user1_cluster_swarm already registered for object Cluster access'}

@pytest.mark.asyncio
async def test_update_disabled_cluster_access(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'cluster_access_id': 4,
            'cluster_access_name': 'cluster_access_swarm_update',
            'user_id': 3,
            'cluster_id': 2,
            'log_root': 'log_root_update',
            'remote_app_path': 'remote_app_path_update',
            'entry_point_wrapper': 'entry_point_wrapper_update',
            'entry_point': 'entry_point_update'
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Cluster Access ID : 4'}

@pytest.mark.asyncio
async def test_delete_cluster_access_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+str(1))
    assert response.status_code == status.HTTP_204_NO_CONTENT


@pytest.mark.asyncio
async def test_delete_cluster_access_unknown_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+str(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Cluster Access ID : 9999'}


@pytest.mark.asyncio
async def test_delete_disabled_cluster_access(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+str(4))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Cluster Access ID : 4'}

@pytest.mark.asyncio
async def test_get_cluster_access_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+str(2))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    cluster_access = ClusterAccess.parse_obj(json_response)
    assert cluster_access.cluster_access_id == 2
    assert cluster_access.cluster_access_name == 'access_user1_cluster_swarm'
    assert cluster_access.user_id == 2
    assert cluster_access.cluster_id == 2
    assert not cluster_access.log_root
    assert not cluster_access.remote_app_path
    assert not cluster_access.entry_point_wrapper
    assert cluster_access.entry_point == 'jobard-remote-dask'

@pytest.mark.asyncio
async def test_get_cluster_access_unknown_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+str(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Cluster Access ID : 9999'}

@pytest.mark.asyncio
async def test_get_cluster_access_bad_id_type(async_app_client_admin, postgresql_database):
    for cluster_access_id in ('ABC', None, 0.5):
        response = await async_app_client_admin.get(ENDPOINT+str(cluster_access_id))
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

@pytest.mark.asyncio
async def test_get_cluster_access_disabled(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+str(4))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Cluster Access ID : 4'}

@pytest.mark.asyncio
async def test_list_cluster_access_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get('/cluster-accesses'+str('?offset=0&limit=2'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    cluster_access_list = ClusterAccessList.parse_obj(json_response)
    assert cluster_access_list.count == 3
    assert cluster_access_list.offset == 0
    assert cluster_access_list.limit == 2
    # Check cluster accesses sort order
    for idx, ca in enumerate(cluster_access_list.records):
        assert ca.cluster_access_id == idx + 1