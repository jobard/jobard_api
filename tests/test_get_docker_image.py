"""Test get endpoint."""
from tests.database import execute_queries_from_file
import pytest
from starlette import status
from jobard_api.docker.dto import DockerImage

ENDPOINT = '/docker-images/{0}'


@pytest.mark.asyncio
async def test_get_docker_image_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(1))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    docker_image = DockerImage.parse_obj(json_response)
    assert docker_image.image_id == 1
    assert docker_image.name == 'my_docker_image1'
    assert docker_image.user_id == 2
    assert docker_image.url == 'my_url1'
    assert docker_image.is_test is False
    assert docker_image.enabled is True


@pytest.mark.asyncio
async def test_get_docker_image_unknown_id(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Docker image ID : 9999'}


@pytest.mark.asyncio
async def test_get_docker_image_bad_id_type(async_app_client, postgresql_database):
    for image_id in ('ABC', None, 0.5):
        response = await async_app_client.get(ENDPOINT.format(image_id))
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.asyncio
async def test_get_docker_image_forbidden(postgresql_database, async_app_client_user1, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client_user1.get(ENDPOINT.format(1))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN, json_response
    assert json_response == {'detail': 'Docker image ID : 1 does not belong to the authenticated user'}


@pytest.mark.asyncio
async def test_get_docker_image_disabled(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(7))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Docker image ID : 7'}
