"""Test put endpoint."""
import pytest
from starlette import status

from jobard_api.docker.dto import DockerImage
from tests.database import execute_queries_from_file

ENDPOINT = '/docker-images/'


@pytest.mark.asyncio
async def test_update_image_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.put(
        ENDPOINT,
        json={
            'image_id': 1,
            'name': 'my_new_docker_image1',
            'user_id': 2,
            'url': 'image_url',
            'is_test': True
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK
    # parse and check response
    docker_image = DockerImage.parse_obj(json_response)
    assert docker_image.image_id == 1
    assert docker_image.name == 'my_new_docker_image1'
    assert docker_image.user_id == 2
    assert docker_image.url == 'image_url'
    assert docker_image.is_test is True
    assert docker_image.enabled is True


@pytest.mark.asyncio
async def test_update_image_unknown_id(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.put(
        ENDPOINT,
        json={
            'image_id': 9999,
            'name': 'my_docker_image2',
            'is_test': False
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Docker image ID : 9999'}


@pytest.mark.asyncio
async def test_update_image_name_already_registered(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.put(
        ENDPOINT,
        json={
            'image_id': 1,
            'name': 'my_docker_image2',
            'is_test': False
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'name : my_docker_image2 already registered for object Docker image'}


@pytest.mark.asyncio
async def test_update_image_forbidden(postgresql_database, async_app_client_user1, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client_user1.put(
        ENDPOINT,
        json={
            'image_id': 3,
            'name': 'my_docker_image2',
            'is_test': False
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN, json_response
    assert json_response == {'detail': 'Docker image ID : 3 does not belong to the authenticated user'}


@pytest.mark.asyncio
async def test_update_disabled_image(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.put(
        ENDPOINT,
        json={
            'image_id': 7,
            'name': 'my_docker_image2',
            'is_test': False
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Docker image ID : 7'}
