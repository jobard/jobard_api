"""Test stat endpoint."""

import pytest
from starlette import status

from jobard_api.core.types import State
from jobard_api.jobs.dto import JobOrderStat
from tests.database import execute_queries_from_file

ENDPOINT = '/job-orders/{0}/stat'


@pytest.mark.asyncio
async def test_stat_ok(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(1))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    job_stat = JobOrderStat.parse_obj(json_response)
    assert job_stat.id == 1
    assert job_stat.state == State.INIT_ASKED
    assert job_stat.progress.percentage == 0
    assert job_stat.progress.job_states == {
        'INIT_ASKED': 4,
        'INIT_RUNNING': 1,
        'NEW': 2,
        'ENQUEUED': 1,
        'RUNNING': 2,
        'SUCCEEDED': 1,
        'FAILED': 1,
        'CANCELLED': 1,
    }


@pytest.mark.asyncio
async def test_stat_unknown_id(postgresql_database, async_app_client, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client.get(ENDPOINT.format(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Job order ID : 9999'}


@pytest.mark.asyncio
async def test_stat_bad_id_type(postgresql_database, async_app_client):
    for job_id in ('ABC', None, 0.5):
        response = await async_app_client.get(ENDPOINT.format(job_id))
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.asyncio
async def test_stat_forbidden_job_order(postgresql_database, async_app_client_user1, common_sample):
    # initialize sample data
    await execute_queries_from_file(postgresql_database, common_sample)
    # execute endpoint
    response = await async_app_client_user1.get(ENDPOINT.format(2))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_403_FORBIDDEN, json_response
    assert json_response == {'detail': 'Job order ID : 2 does not belong to the authenticated user'}
