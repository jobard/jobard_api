"""Test post endpoint."""
from datetime import timedelta

import pytest
from starlette import status

from jobard_api.monitoring import service as clusters_service
from jobard_api.monitoring.dto import Cluster, ClusterList

ENDPOINT = '/clusters/'

@pytest.mark.asyncio
async def test_create_cluster_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'cluster_name': 'cluster_local',
            'type': 'LOCAL',
            'healthcheck_interval' : 3600,
            'remote_username': 'remote_user',
            'password': 'password1',
            'client_key': 'key1'
        }
    )
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.asyncio
async def test_create_cluster_already_registered(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.post(
        ENDPOINT,
        json={
            'cluster_name': 'local_cluster',
            'type': 'LOCAL',
            'healthcheck_interval': 3600,
            'remote_username': 'remote_user',
            'password': 'password1',
            'client_key': 'key1',
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'name : local_cluster already registered for object Cluster'}

@pytest.mark.asyncio
async def test_update_cluster_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'cluster_id': 4,
            'type': 'HTCONDOR',
            'cluster_name': 'htcondor_cluster',
            'healthcheck_interval': 3600,
            'remote_username': 'remote_user4_update',
            'password': 'password4_update',
            'client_key': 'key4_update',
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK
    # parse and check response
    cluster = Cluster.parse_obj(json_response)
    assert cluster.cluster_id == 4
    assert cluster.cluster_name == 'htcondor_cluster'
    assert cluster.healthcheck_interval == timedelta(seconds=3600)
    assert cluster.type == 'HTCONDOR'
    assert cluster.remote_username == 'remote_user4_update'


@pytest.mark.asyncio
async def test_update_cluster_unknown_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'cluster_id': 9999,
            'cluster_name': 'update_cluster_local',
            'healthcheck_interval': 3600,
            'remote_username': 'remote_user_update',
            'password': 'password1_update',
            'client_key': 'key1_update',
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Cluster ID : 9999'}


@pytest.mark.asyncio
async def test_update_cluster_name_already_registered(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'cluster_id': 4,
            'cluster_name': 'pbs_cluster',
            'healthcheck_interval': 3600,
            'remote_username': 'remote_user4_update',
            'password': 'password4_update',
            'client_key': 'key4_update',
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json_response == {'detail': 'name : pbs_cluster already registered for object Cluster'}

@pytest.mark.asyncio
async def test_update_disabled_cluster(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.put(
        ENDPOINT,
        json={
            'cluster_id': 5,
            'cluster_name': 'update_cluster_local',
            'healthcheck_interval': 3600,
            'remote_username': 'remote_user_update',
            'password': 'password1_update',
            'client_key': 'key1_update',
        }
    )
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Cluster ID : 5'}

@pytest.mark.asyncio
async def test_delete_cluster_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+str(3))
    assert response.status_code == status.HTTP_204_NO_CONTENT

@pytest.mark.asyncio
async def test_delete_cluster_used(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+str(1))
    assert response.status_code == status.HTTP_400_BAD_REQUEST

@pytest.mark.asyncio
async def test_delete_cluster_unknown_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+str(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Cluster ID : 9999'}


@pytest.mark.asyncio
async def test_delete_disabled_cluster(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.delete(ENDPOINT+str(5))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Cluster ID : 5'}

@pytest.mark.asyncio
async def test_get_cluster_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+str(2))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    cluster = Cluster.parse_obj(json_response)
    assert cluster.cluster_id == 2
    assert cluster.cluster_name == 'swarm_cluster'
    assert cluster.type == 'SWARM'
    assert cluster.healthcheck_interval == timedelta(seconds=12*3600)
    assert cluster.remote_username == 'user_swarm'

@pytest.mark.asyncio
async def test_get_cluster_pwd_key_ok(postgresql_database, async_app_client_admin):
    # execute service
    cluster = await clusters_service.get_cluster(cluster_id=2)
    assert cluster.cluster_id == 2
    assert cluster.cluster_name == 'swarm_cluster'
    assert cluster.type == 'SWARM'
    assert cluster.healthcheck_interval == timedelta(seconds=12 * 3600)
    assert cluster.password.get_secret_value() == 'password_swarm'
    assert cluster.client_key.get_secret_value() == 'key_swarm'
    assert cluster.remote_username == 'user_swarm'


@pytest.mark.asyncio
async def test_get_cluster_unknown_id(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+str(9999))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND, json_response
    assert json_response == {'detail': 'Unknown Cluster ID : 9999'}

@pytest.mark.asyncio
async def test_get_cluster_bad_id_type(async_app_client_admin, postgresql_database):
    for cluster_id in ('ABC', None, 0.5):
        response = await async_app_client_admin.get(ENDPOINT+str(cluster_id))
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

@pytest.mark.asyncio
async def test_get_cluster_disabled(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get(ENDPOINT+str(5))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert json_response == {'detail': 'Unknown Cluster ID : 5'}

@pytest.mark.asyncio
async def test_list_cluster_ok(postgresql_database, async_app_client_admin):
    # execute endpoint
    response = await async_app_client_admin.get('/clusters'+str('?offset=0&limit=2'))
    # check status and response
    json_response = response.json()
    assert response.status_code == status.HTTP_200_OK, json_response
    # parse and check response
    cluster_list = ClusterList.parse_obj(json_response)
    assert cluster_list.count == 4
    assert cluster_list.offset == 0
    assert cluster_list.limit == 2
