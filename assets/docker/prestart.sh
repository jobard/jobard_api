#! /usr/bin/env sh
set -e

echo "Upgrade database schema"
alembic upgrade head
