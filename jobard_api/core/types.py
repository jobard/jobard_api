"""Jobard core types."""

from enum import Enum
from typing import List

from pydantic import constr

Memory = constr(regex=r'\d+[GMK]')
Walltime = constr(regex=r'^\d{2}:\d{2}:\d{2}$')
Split = constr(regex=r'\/?[1-9]\d*')
Arguments = List[str]
CommandAlias = constr(regex=r'^[A-Za-z0-9_-]*$')
JobOrderName = constr(regex=r'^[A-Za-z0-9_-]*$')

class ClusterType(str, Enum):
    """List of clusters."""

    LOCAL = 'LOCAL'
    SWARM = 'SWARM'
    PBS = 'PBS'
    HTCONDOR = 'HTCONDOR'
    KUBE = 'KUBE'


class State(str, Enum):
    """List of states."""

    INIT_ASKED = 'INIT_ASKED'
    INIT_RUNNING = 'INIT_RUNNING'
    NEW = 'NEW'
    ENQUEUED = 'ENQUEUED'
    SUBMITTED = 'SUBMITTED'
    RUNNING = 'RUNNING'
    SUCCEEDED = 'SUCCEEDED'
    FAILED = 'FAILED'
    CANCELLATION_ASKED = 'CANCELLATION_ASKED'
    CANCELLED = 'CANCELLED'


class MonitoringStatus(str, Enum):
    """List of monitoring status."""

    DOWN = 'DOWN'
    UP = 'UP'
