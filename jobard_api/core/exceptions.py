"""Generic exceptions."""
from typing import Any


class JobardError(Exception):
    """Jobard generic error."""


class JobardApiException(JobardError):
    """Jobard generic exception class."""


class AccessForbiddenException(JobardApiException):
    """Access forbidden exception"""

    def __init__(self, item_type: str, item_id: Any):
        """Initialize exception.

        Args:
            item_type: type of item
            item_id: identifier of item
        """
        super().__init__('{0} ID : {1} does not belong to the authenticated user'.format(item_type, item_id))


class UnknownIdException(JobardApiException):
    """Unknown ID exception class."""

    def __init__(self, item_type: str, item_id: Any):
        """Initialize exception.

        Args:
            item_type: type of item
            item_id: identifier of item
        """
        super().__init__('Unknown {0} ID : {1}'.format(item_type, item_id))


class AlreadyRegisteredException(JobardApiException):
    """Already registered exception"""

    def __init__(self, item_type: str, item_property: str, item_value: Any):
        """Initialize exception.

        Args:
            item_type: type of the item
            item_property: property of the item
            item_value: value of the item
        """
        super().__init__(
            '{0} : {1} already registered for object {2}'.format(item_property, item_value, item_type)
        )


class UnknownNameException(JobardApiException):
    """Unknown name exception class."""

    def __init__(self, item_type: str, item_name: Any):
        """Initialize exception.

        Args:
            item_type: type of item
            item_name: identifier of item
        """
        super().__init__('Unknown {0} Name : {1}'.format(item_type, item_name))
