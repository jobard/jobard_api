from __future__ import annotations

from datetime import datetime, timedelta
from typing import TypeVar
from pydantic import BaseModel


class JobardModel(BaseModel):
    """Base Jobard model."""

    class Config:   # noqa: WPS431, WPS431, WPS202, WPS306
        """Pydantic configuration."""

        validate_all = True
        validate_assignment = True
        json_encoders = {
            datetime: lambda date: date.isoformat(),
            timedelta: lambda time: str(time)
        }


jobard_model_type = TypeVar('jobard_model_type', bound=JobardModel)


class Message(JobardModel):
    """API Message model."""

    detail: str
