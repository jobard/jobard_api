"""initial jobard schema.

Revision ID: e04a95ad47d0
Revises:
Create Date: 2021-12-06 17:52:46.270266

"""
import sqlalchemy as sa
from alembic import op

from jobard_api.core.types import ClusterType, State

# revision identifiers, used by Alembic.
revision = 'e04a95ad47d0'
down_revision = None
branch_labels = None
depends_on = None

DEFAULT_VALUE_ZERO = '0'
DEFAULT_VALUE_ONE = '1'
DEFAULT_VALUE_NOW_UTC = sa.text("(NOW() AT TIME ZONE 'UTC')")

drop_state_type = 'DROP TYPE IF EXISTS state;'
drop_cluster_type = 'DROP TYPE IF EXISTS cluster_type;'
drop_job_order_update_fct = 'DROP FUNCTION IF EXISTS update_job_order;'
drop_job_order_update_trigger = 'DROP TRIGGER IF EXISTS job_order_bu on job_order;'
drop_job_array_fct = 'DROP FUNCTION IF EXISTS update_job_array;'
drop_job_array_trigger = 'DROP TRIGGER IF EXISTS job_array_bu on job_array;'
drop_job_fct = 'DROP FUNCTION IF EXISTS update_job;'
drop_job_trigger = 'DROP TRIGGER IF EXISTS job_bu on job;'

create_pgcrypto_extension = 'CREATE EXTENSION IF NOT EXISTS pgcrypto;'

create_state_type = """
    CREATE TYPE state as ENUM(
    'INIT_ASKED',
    'INIT_RUNNING',
    'NEW',
    'ENQUEUED',
    'SUBMITTED',
    'RUNNING',
    'SUCCEEDED',
    'FAILED',
    'CANCELLATION_ASKED',
    'CANCELLED'
    );
"""

create_cluster_type = """
    CREATE TYPE cluster_type as ENUM('LOCAL', 'SWARM', 'PBS', 'HTCONDOR', 'KUBE');
"""

job_order_update_fct = """
    CREATE OR REPLACE FUNCTION update_job_order()
    RETURNS TRIGGER AS $$
    BEGIN
        NEW.updated := NOW() AT TIME ZONE 'UTC';
        RETURN NEW;
    END; $$ LANGUAGE PLPGSQL
"""

job_order_update_trigger = """
    DROP TRIGGER IF EXISTS job_order_bu on job_order;
    CREATE TRIGGER job_order_bu
    BEFORE UPDATE ON job_order
    FOR EACH ROW
    EXECUTE PROCEDURE update_job_order();
"""

job_array_fct = """
    CREATE OR REPLACE FUNCTION update_job_array()
    RETURNS TRIGGER AS $$
    BEGIN
        NEW.updated := NOW() AT TIME ZONE 'UTC';
        RETURN NEW;
    END; $$ LANGUAGE PLPGSQL
"""

job_array_trigger = """
    DROP TRIGGER IF EXISTS job_array_bu on job_array;
    CREATE TRIGGER job_array_bu
    BEFORE UPDATE ON job_array
    FOR EACH ROW
    EXECUTE PROCEDURE update_job_array();
"""

job_fct = """
    CREATE OR REPLACE FUNCTION update_job()
    RETURNS TRIGGER AS $$
    BEGIN
        NEW.updated := NOW() AT TIME ZONE 'UTC';
        update job_array a
        SET updated = NOW() AT TIME ZONE 'UTC'
        WHERE a.id = NEW.job_array;
        RETURN NEW;
    END; $$ LANGUAGE PLPGSQL
"""
job_trigger = """
    DROP TRIGGER IF EXISTS job_bu on job;
    CREATE TRIGGER job_bu
    BEFORE UPDATE ON job
    FOR EACH ROW
    EXECUTE PROCEDURE update_job();
"""


def upgrade():
    """Upgrade database schema."""

    op.execute(create_pgcrypto_extension)

    op.create_table(
        'jobard_user',
        sa.Column('user_id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('username', sa.String, unique=True),
        sa.Column('email', sa.String, unique=True),
        sa.Column('remote_username', sa.String),
        sa.Column('password', sa.LargeBinary, nullable=True),
        sa.Column('client_key', sa.LargeBinary, nullable=True),
        sa.Column('unix_uid', sa.Integer, nullable=True),
        sa.Column('enabled', sa.Boolean, default=True),
    )

    op.create_table(
        'docker_image',
        sa.Column('image_id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('name', sa.String),
        sa.Column('user_id', sa.Integer, sa.ForeignKey('jobard_user.user_id')),
        sa.Column('url', sa.String),
        sa.Column('is_test', sa.Boolean),
        sa.Column('enabled', sa.Boolean, default=True),
    )

    op.create_table(
        'docker_mount_point',
        sa.Column('mount_point_id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('image_id', sa.Integer, sa.ForeignKey('docker_image.image_id')),
        sa.Column('name', sa.String),
        sa.Column('source', sa.String),
        sa.Column('destination', sa.String),
    )

    op.create_table(
        'job_order',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('command', sa.ARRAY(sa.String)),
        sa.Column('arguments', sa.ARRAY(sa.String), nullable=True),
        sa.Column('interpolate_command', sa.Boolean, server_default='f'),
        sa.Column('split', sa.String, nullable=True),
        sa.Column('priority', sa.Integer, server_default=DEFAULT_VALUE_ONE),
        sa.Column('cores', sa.Integer, server_default=DEFAULT_VALUE_ONE),
        sa.Column('memory', sa.String, server_default='512M'),
        sa.Column('walltime', sa.String, server_default='00:05:00'),
        sa.Column('concurrency', sa.Integer, server_default=DEFAULT_VALUE_ZERO),
        sa.Column('max_concurrency', sa.Integer, server_default=DEFAULT_VALUE_ONE),
        sa.Column('job_extra', sa.ARRAY(sa.String), nullable=True),
        sa.Column('env_extra', sa.JSON, nullable=True),
        sa.Column('state', sa.Enum(State), server_default=State.INIT_ASKED),
        sa.Column('progress', sa.Float, server_default=DEFAULT_VALUE_ZERO),
        sa.Column('job_count', sa.Integer, server_default=DEFAULT_VALUE_ZERO),
        sa.Column('created', sa.TIMESTAMP, server_default=DEFAULT_VALUE_NOW_UTC),
        sa.Column('updated', sa.TIMESTAMP, server_default=DEFAULT_VALUE_NOW_UTC),
        sa.Column('submitted', sa.TIMESTAMP, nullable=True),
        sa.Column('finished', sa.TIMESTAMP, nullable=True),
        sa.Column('root_log_path', sa.String, nullable=True),
        sa.Column('name', sa.String, nullable=True),
        sa.Column('connection', sa.String),
        sa.Column('user_id', sa.Integer, sa.ForeignKey('jobard_user.user_id')),
        sa.Column('image_id', sa.Integer, sa.ForeignKey('docker_image.image_id'), nullable=True),
        sa.Column('error_message', sa.String, nullable=True),
        sa.Column('is_healthcheck', sa.Boolean),
    )
    op.execute(job_order_update_fct)
    op.execute(job_order_update_trigger)

    op.create_table(
        'job_array',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True, nullable=True),
        sa.Column('job_order', sa.Integer, sa.ForeignKey('job_order.id', ondelete='CASCADE')),
        sa.Column('job_offset', sa.Integer, server_default=DEFAULT_VALUE_ZERO),
        sa.Column('job_count', sa.Integer, server_default=DEFAULT_VALUE_ZERO),
        sa.Column('scheduled_id', sa.String, nullable=True),
        sa.Column('state', sa.Enum(State), server_default=State.NEW),
        sa.Column('progress', sa.Float, server_default=DEFAULT_VALUE_ZERO),
        sa.Column('created', sa.TIMESTAMP, server_default=DEFAULT_VALUE_NOW_UTC),
        sa.Column('updated', sa.TIMESTAMP, server_default=DEFAULT_VALUE_NOW_UTC),
        sa.Column('submitted', sa.TIMESTAMP, nullable=True),
        sa.Column('finished', sa.TIMESTAMP, nullable=True),
        sa.Column('daemon_id', sa.String, nullable=True),
        sa.Column('executor_id', sa.String, nullable=True),
        sa.Column('executor_log_path', sa.String, nullable=True),
        sa.Column('error_message', sa.String, nullable=True),
        sa.Column('try_nb', sa.Integer, server_default=DEFAULT_VALUE_ONE),
    )
    op.execute(job_array_fct)
    op.execute(job_array_trigger)

    op.create_table(
        'job',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True, nullable=True),
        sa.Column('job_array', sa.Integer, sa.ForeignKey('job_array.id', ondelete='CASCADE')),
        sa.Column('job_array_index', sa.Integer, server_default=DEFAULT_VALUE_ONE),
        sa.Column('command_with_parameters', sa.ARRAY(sa.String)),
        sa.Column('arguments', sa.ARRAY(sa.String)),
        sa.Column('scheduled_id', sa.String, nullable=True),
        sa.Column('state', sa.Enum(State), server_default=State.NEW),
        sa.Column('progress', sa.Float, server_default=DEFAULT_VALUE_ZERO),
        sa.Column('created', sa.TIMESTAMP, server_default=DEFAULT_VALUE_NOW_UTC),
        sa.Column(
            'updated',
            sa.TIMESTAMP,
            server_default=DEFAULT_VALUE_NOW_UTC,
            server_onupdate=DEFAULT_VALUE_NOW_UTC,  # noqa: WPS221
        ),
        sa.Column('submitted', sa.TIMESTAMP, nullable=True),
        sa.Column('finished', sa.TIMESTAMP, nullable=True),
        sa.Column('error_message', sa.String, nullable=True),
        sa.Column('log_path_stdout', sa.String, nullable=True),
        sa.Column('log_path_stderr', sa.String, nullable=True),
    )
    op.execute(job_fct)
    op.execute(job_trigger)

    op.create_table(
        'cluster',
        sa.Column('cluster_id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('cluster_name', sa.String, unique=True, nullable=False),
        sa.Column('type', sa.Enum(ClusterType), server_default=ClusterType.LOCAL, nullable=False),
        sa.Column('healthcheck_interval', sa.Interval, nullable=False),
        sa.Column('remote_username', sa.String, nullable=True),
        sa.Column('password', sa.LargeBinary, nullable=True),
        sa.Column('client_key', sa.LargeBinary, nullable=True),
        sa.Column('enabled', sa.Boolean, default=True, nullable=False),
    )

    op.create_table(
            'remote_connection',
            sa.Column('daemon_id', sa.String, primary_key=True),
            sa.Column('name', sa.String, primary_key=True),
            sa.Column('cluster_id', sa.Integer, sa.ForeignKey('cluster.cluster_id')),
            sa.Column('enabled', sa.Boolean, default=True, nullable=False),
    )

    op.create_table(
        'access_token',
        sa.Column('token_id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('user_id', sa.Integer, sa.ForeignKey('jobard_user.user_id')),
        sa.Column('access_token', sa.String, primary_key=True),
        sa.Column('expiration_date', sa.TIMESTAMP, nullable=True),
    )

    op.create_table(
        'cluster_access',
        sa.Column('cluster_access_id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('user_id', sa.Integer, sa.ForeignKey('jobard_user.user_id')),
        sa.Column('cluster_id', sa.Integer, sa.ForeignKey('cluster.cluster_id')),
        sa.Column('cluster_access_name', sa.String, unique=True, nullable=False),
        sa.Column('log_root', sa.String, nullable=True),
        sa.Column('remote_app_path', sa.String, nullable=True),
        sa.Column('entry_point_wrapper', sa.String, nullable=True),
        sa.Column('entry_point', sa.String, nullable=False),
        sa.Column('enabled', sa.Boolean, default=True, nullable=False),
    )


def downgrade():
    """Downgrade database schema."""
    op.execute(drop_job_order_update_trigger)
    op.execute(drop_job_order_update_fct)
    op.execute(drop_job_array_trigger)
    op.execute(drop_job_array_fct)
    op.execute(drop_job_trigger)
    op.execute(drop_job_fct)
    op.drop_table('job')
    op.drop_table('job_array')
    op.drop_table('job_order')
    op.drop_table('remote_connection')
    op.drop_table('access_token')
    op.drop_table('cluster_access')
    op.drop_table('cluster')
    op.drop_table('docker_mount_point')
    op.drop_table('docker_image')
    op.drop_table('jobard_user')
    op.execute(drop_state_type)
    op.execute(drop_cluster_type)
