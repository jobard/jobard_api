"""jobs_logs

Revision ID: 72960604252a
Revises: e04a95ad47d0
Create Date: 2025-02-13 16:14:15.365708

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '72960604252a'
down_revision = 'e04a95ad47d0'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        table_name='job_order',
        column=sa.Column('command_alias', sa.String, nullable=True),
    )
    op.add_column(
        table_name='job_order',
        column=sa.Column('job_log_prefix_arg_idx', sa.Integer, nullable=True),
    )


def downgrade():
    op.drop_column(
        table_name='job_order',
        column_name='command_alias',
    )
    op.drop_column(
        table_name='job_order',
        column_name='job_log_prefix_arg_idx',
    )
