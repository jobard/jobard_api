"""Alchemy custom types """
from sqlalchemy import TypeDecorator, type_coerce, String, func
from sqlalchemy.dialects.postgresql import BYTEA


class PGPString(TypeDecorator):
    """Custom type for implementing encryption/decryption strings with AES key into Postgres database."""

    impl = BYTEA

    cache_ok = True

    def __init__(self, passphrase):
        """Initialization."""
        super(PGPString, self).__init__() # noqa: WPS608
        self.passphrase = passphrase

    def bind_expression(self, bindvalue, sa=None):
        """Convert the bind's type from PGPString to String, so that it's passed to psycopg2 as is without a
        dbapi.Binary wrapper."""
        bindvalue = type_coerce(bindvalue, String)
        return func.pgp_sym_encrypt(bindvalue, self.passphrase)

    def column_expression(self, col):
        """Decrypt the database value."""
        return func.pgp_sym_decrypt(col, self.passphrase)
