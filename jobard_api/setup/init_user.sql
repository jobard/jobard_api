-- Create authorized clusters
INSERT INTO CLUSTER(type, cluster_name, healthcheck_interval, enabled)
VALUES('LOCAL', 'local_cluster', INTERVAL '12 hours', TRUE);
INSERT INTO CLUSTER(type, cluster_name, healthcheck_interval, remote_username, password, enabled)
VALUES('SWARM', 'swarm_cluster', INTERVAL '12 hours', 'user_swarm', PGP_SYM_ENCRYPT('password_swarm', 'AES_KEY'), TRUE);
INSERT INTO CLUSTER(type, cluster_name, healthcheck_interval, enabled)
VALUES('PBS', 'pbs_cluster', INTERVAL '1 day', TRUE);
INSERT INTO CLUSTER(type, cluster_name, healthcheck_interval, enabled)
VALUES('HTCONDOR', 'htcondor_cluster', INTERVAL '1 day', TRUE);
INSERT INTO CLUSTER(type, cluster_name, healthcheck_interval, remote_username, client_key, enabled)
VALUES('KUBE', 'kube_cluster', INTERVAL '1 day', 'user_kube', PGP_SYM_ENCRYPT('key_kube', 'AES_KEY'), TRUE);

-- Create users (User/RemoteUserAccess)
INSERT INTO JOBARD_USER(username, email, remote_username, password, client_key, enabled)
VALUES('test_user', 'test_user@company.xyz', 'test_remote_user', PGP_SYM_ENCRYPT('password', 'AES_KEY'), NULL, TRUE);

-- Create ClusterAccess
INSERT INTO CLUSTER_ACCESS(user_id, cluster_id, cluster_access_name, log_root, remote_app_path, entry_point_wrapper, entry_point, enabled)
VALUES(2, 1, 'access_test_user_local', '/tmp/logs', '/path/to/remote/dask', NULL, 'jobard-remote-dask', TRUE);
-- Create user access token
INSERT INTO ACCESS_TOKEN(user_id, access_token, expiration_date)
VALUES(2, 'access_token', NULL);

-- Create remote connection
INSERT INTO REMOTE_CONNECTION(daemon_id, name, cluster_id, enabled) VALUES('0', 'localhost', 1, TRUE);
