"""Jobard API application."""

from fastapi import FastAPI, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware

from jobard_api.conf.database import database
from jobard_api.conf.settings import settings
from jobard_api.monitoring import dto as monitoring_dto
from jobard_api.monitoring import service as monitoring_service
from jobard_api.routers import (
    access_tokens,
    cluster_access,
    clusters,
    connections,
    connections_cluster,
    docker_images,
    docker_mount_points,
    job_orders,
    jobs,
    user_accesses,
    users,
)
from jobard_api.users import dto as users_dto
from jobard_api.users import service as users_service
from jobard_api.users.exceptions import UnknownUserIdException

# create application

app = FastAPI(
    root_path=settings.context_path,
    title='Jobard API',
    description='Submit and follow the execution of jobs.',
    version=settings.version,
)

# configure middlewares
app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.allowed_hosts,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


@app.get(
    '/health',
    response_model=monitoring_dto.MonitoringResult,
    status_code=status.HTTP_200_OK,
    description='Retrieve health check results'
)
async def healthcheck():
    """Get Jobard health information.
      Args:
          token: The token.
      Returns:
          The health information.
    """
    try:
        return await monitoring_service.get_monitoring()
    except Exception as error:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(error))


app.include_router(jobs.router)
app.include_router(job_orders.router)
app.include_router(docker_images.router)
app.include_router(docker_mount_points.router)
app.include_router(clusters.router)
app.include_router(connections.router)
app.include_router(users.router)
app.include_router(cluster_access.router)
app.include_router(access_tokens.router)
app.include_router(connections_cluster.router)
app.include_router(user_accesses.router)

@app.on_event('startup')
async def startup():
    """Initialize database connection."""
    # create connection pool
    if not database.is_connected:
        await database.connect()

    # Create admin user if it does not exist
    try:
        await users_service.get_user_by_id(user_id=1)
    except UnknownUserIdException:
        user_input = users_dto.JobardUserInput(username='admin', email='email_admin', remote_username='remote_admin')
        await users_service.create_user(user_input=user_input)

@app.on_event('shutdown')
async def shutdown():
    """Close database connection."""
    if database.is_connected:
        await database.disconnect()


if __name__ == '__main__':
    import uvicorn  # noqa: WPS433
    uvicorn.run('jobard_api.main:app', host='127.0.0.1', port=8000, reload=True)  # noqa: WPS432
