"""Jobard API Remote connection cluster router."""

from fastapi import APIRouter, Depends, HTTPException
from fastapi import status

from jobard_api.dependencies.auth import bearer_or_basic_auth_admin
from jobard_api.core.exceptions import AlreadyRegisteredException
from jobard_api.monitoring.exceptions import UnknownClusterIdException
from jobard_api.users import dto as users_dto
from jobard_api.users import service as users_service
from jobard_api.users.dto import AccessToken

router = APIRouter(
    prefix='/{0}'.format('user-accesses'),
    tags=['{0}'.format('user-accesses')]
)

@router.post(
    '/',
    status_code=status.HTTP_201_CREATED,
    description='Create a Jobard user with his token and its cluster accesses',
)
async def create_user_accesses(
    user_access_input: users_dto.UserAccessesInput,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        return await users_service.create_user_accesses(user_access_input=user_access_input)
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except UnknownClusterIdException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))


