"""Jobard API job orders router."""
from datetime import datetime, timezone
from typing import List, Optional

from fastapi import APIRouter, Depends, HTTPException
from fastapi import Query, status

import jobard_api.core.dto
from jobard_api.core.types import State, ClusterType
from jobard_api.dependencies.auth import bearer_or_basic_auth
from jobard_api.docker import service as docker_service
from jobard_api.docker.exceptions import (
    UnknownImageIdException,
    ForbiddenDockerImageException,
)
from jobard_api.jobs import dto as job_dto, service as job_service
from jobard_api.jobs.exceptions import NotCancellableException, UnknownJobOrderIdException, ForbiddenJobOrderException
from jobard_api.users import service as users_service
from jobard_api.users.dto import AccessToken
from jobard_api.users.exceptions import DeniedClusterAccessException, MandatoryPasswordOrKeyException, \
    MandatoryRemoteUsernameException

router = APIRouter(
    prefix='/{0}'.format('job-orders'),
    tags=['{0}'.format('job-orders')]
)


@router.post(
    '/',
    status_code=status.HTTP_201_CREATED,
    description='Submit a job order.',
)
async def submit_job_order(job_order_input: job_dto.JobOrderSubmit, token: AccessToken = Depends(bearer_or_basic_auth)):
    """Submit a job order.

    Args:
        job_order_input: Job order to submit
        token: The token.

    Returns:
        JobOrder ID

    Raises:
        HTTPException: In case of failure
    """
    # Get user_id from the token
    job_order_input.user_id = token.user_id
    # First, check if the user has the rights on the cluster
    try:
        cluster_type = await users_service.validate_cluster_access(
            remote_connection_name=job_order_input.connection, user_id=token.user_id)
    except DeniedClusterAccessException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))
    except MandatoryPasswordOrKeyException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except MandatoryRemoteUsernameException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))

    # For K8S and Swarm clusters, check there is a docker image set , and check that image belongs to the user
    if cluster_type in (ClusterType.SWARM, ClusterType.KUBE):
        if job_order_input.image_id is None:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Image is mandatory for Swarm and Kubernetes clusters',
            )
        try:
            await docker_service.get_docker_image(image_id=job_order_input.image_id, user_id=token.user_id)
        except UnknownImageIdException as error:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
        except ForbiddenDockerImageException as error:
            raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))

    # For others, check there is no docker image set
    if cluster_type not in (ClusterType.SWARM, ClusterType.KUBE) and job_order_input.image_id is not None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail='Image must only be set for Swarm and Kubernetes clusters')

    # If not set, set a default job order name
    if job_order_input.name is None:
        # Retrieve the username of the jobard user
        jobard_user = await users_service.get_user_by_id(user_id=token.user_id)
        job_order_input.name = datetime.now(timezone.utc).strftime('{0}_%Y%m%dT%H%M%S'.format(jobard_user.username))

    return await job_service.create_job_order(job_order_input=job_order_input)


@router.delete(
    '/{job_order_id}',
    status_code=status.HTTP_200_OK,
    description='Cancel a job order (set status to CANCELLATION_ASKED)',
)
async def cancel_job_order(job_order_id: int, token: AccessToken = Depends(bearer_or_basic_auth)):
    """Ask for cancellation of a job order.

    Args:
        job_order_id: JobOrder ID
        token: The token

    Returns:
        Cancellation message

    Raises:
        HTTPException: if the job order is unknown, can't be cancelled or belongs to another user.
    """
    try:
        await job_service.cancel_job_order(job_order_id=job_order_id, user_id=token.user_id)
        return jobard_api.core.dto.Message(
            detail='Cancellation for JobOrder {0} successfully registered'.format(job_order_id))
    except UnknownJobOrderIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except NotCancellableException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except ForbiddenJobOrderException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))


@router.get(
    '/{job_order_id}/stat',
    response_model=job_dto.JobOrderStat,
    status_code=status.HTTP_200_OK,
    description='Retrieve statistics of a job order.',
)
async def get_job_order_stat(job_order_id: int, token: AccessToken = Depends(bearer_or_basic_auth)):
    """Retrieve statistics of a job order.

    Args:
        job_order_id: JobOrder ID
        token: The token.

    Returns:
        Statistics of the job order

    Raises:
        HTTPException: If the job order id is unknown or belongs to another user
    """
    try:
        return await job_service.get_job_order_stat(job_order_id=job_order_id, user_id=token.user_id)
    except UnknownJobOrderIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except ForbiddenJobOrderException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))


@router.get(
    '/{job_order_id}/jobs/',
    response_model=job_dto.JobList,
    status_code=status.HTTP_200_OK,
    description='Retrieve a paginated list of job ID of a job order.',
)
async def get_jobs(
    job_order_id: int,
    state: Optional[List[State]] = Query(None),  # noqa: B008, WPS404
    offset: Optional[int] = 0,
    limit: Optional[int] = 1000,
    token: AccessToken = Depends(bearer_or_basic_auth),
):
    """List jobs of a job order.

    Args:
        job_order_id: JobOrder ID
        state: states to filter
        offset: first job to select
        limit: maximum number of jobs returned
        token: the token

    Returns:
        List of Job ID

    Raises:
        HTTPException: If the job order id is unknown or belongs to another user
    """
    try:
        return await job_service.get_jobs_list(
            job_order_id=job_order_id,
            user_id=token.user_id,
            offset=offset,
            limit=limit,
            states=state,
        )
    except UnknownJobOrderIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except ForbiddenJobOrderException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))


@router.get(
    '/{job_order_id}/arguments/',
    response_model=job_dto.JobArgList,
    status_code=status.HTTP_200_OK,
    description='Retrieve a paginated list of job ID of a job order.',
)
async def get_arguments(
    job_order_id: int,
    state: Optional[List[State]] = Query(None),  # noqa: B008, WPS404
    offset: Optional[int] = 0,
    limit: Optional[int] = 1000,
    token: AccessToken = Depends(bearer_or_basic_auth),
):
    """List jobs args of a job order.

    Args:
        job_order_id: JobOrder ID
        state: states to filter
        offset: first job to select
        limit: maximum number of jobs returned
        token: the token

    Returns:
        List of job arguments

    Raises:
        HTTPException: If the job order id is unknown or belongs to another user
    """
    try:
        return await job_service.get_jobs_arguments(
            job_order_id=job_order_id,
            user_id=token.user_id,
            offset=offset,
            limit=limit,
            states=state,
        )
    except UnknownJobOrderIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except ForbiddenJobOrderException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))


@router.get(
    '/{job_order_id}',
    response_model=job_dto.JobOrder,
    status_code=status.HTTP_200_OK,
    description='Retrieve a job order details.',
)
async def get_job_order(job_order_id: int, token: AccessToken = Depends(bearer_or_basic_auth)):
    """Retrieve a job order.

    Args:
        job_order_id: Job order ID
        token: the token
    Returns:
        The corresponding Job order

    Raises:
        HTTPException: If the job order id is unknown or belongs to another user
    """
    try:
        return await job_service.get_job_order(job_order_id=job_order_id, user_id=token.user_id)
    except UnknownJobOrderIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except ForbiddenJobOrderException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))


@router.get(
    '/{job_order_id}/arrays',
    response_model=job_dto.JobArrayList,
    status_code=status.HTTP_200_OK,
    description='Retrieve a job array details.',
)
async def get_job_arrays(
    job_order_id: int,
    state: Optional[List[State]] = Query(None),  # noqa: B008, WPS404
    offset: Optional[int] = 0,
    limit: Optional[int] = 1000,
    token: AccessToken = Depends(bearer_or_basic_auth),
):
    """Retrieve a job array.

    Args:
        job_order_id: Job order ID
        state: state to filter
        offset: first job to select
        limit: maximum number of jobs returned
        token: the token
    Returns:
        The corresponding Job

    Raises:
        HTTPException: If the job order id is unknown or belongs to another user.
    """
    try:
        return await job_service.get_job_arrays(
            job_order_id=job_order_id,
            user_id=token.user_id,
            offset=offset,
            limit=limit,
            states=state,
        )
    except UnknownJobOrderIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except ForbiddenJobOrderException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))


@router.get(
    '',
    response_model=job_dto.JobOrderList,
    status_code=status.HTTP_200_OK,
    description='Retrieve job orders.',
)
async def get_job_orders(
    state: Optional[List[State]] = Query(None),  # noqa: B008, WPS404
    offset: Optional[int] = 0,
    limit: Optional[int] = 1000,
    name: Optional[str] = None,
    token: AccessToken = Depends(bearer_or_basic_auth),
):
    """Retrieve authenticated user's job orders.

    Args:
        state: state to filter
        offset: first job to select
        limit: maximum number of jobs returned
        name: job order name
        token: the token
    Returns:
        The job orders' list
    """
    return await job_service.get_job_orders(
            offset=offset,
            limit=limit,
            states=state,
            name=name,
            user_id=token.user_id,
    )
