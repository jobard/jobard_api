"""Jobard API Remote connection router."""
from typing import Optional

from fastapi import APIRouter, Depends, HTTPException
from fastapi import status

from jobard_api.dependencies.auth import bearer_or_basic_auth_admin
from jobard_api.docker.exceptions import (
    AlreadyRegisteredException,
)
from jobard_api.monitoring import dto as monitoring_dto
from jobard_api.monitoring import service as monitoring_service
from jobard_api.monitoring.exceptions import UnknownRemoteConnectionNameException, NoConnectionException, \
    UnknownClusterIdException
from jobard_api.users.dto import AccessToken

router = APIRouter(
    prefix='/{0}'.format('connections'),
    tags=['{0}'.format('connections')]
)


@router.post(
    '/',
    status_code=status.HTTP_201_CREATED,
    description='Create a Host connection',
)
async def create_connection(
    connection_input: monitoring_dto.RemoteConnectionInput,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        # Check cluster_id
        await monitoring_service.get_cluster(connection_input.cluster_id)
        return await monitoring_service.create_connection(connection_input=connection_input)
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except UnknownClusterIdException as error:
        raise HTTPException(status_code=status.HTTP_424_FAILED_DEPENDENCY, detail=str(error))


@router.get(
    '',
    response_model=monitoring_dto.ConnectionList,
    status_code=status.HTTP_200_OK,
    description='Retrieve Host connections',
)
async def get_connections(
    offset: Optional[int] = 0,
    limit: Optional[int] = 1000,
):
    try:
        return await monitoring_service.get_connections(
            offset=offset,
            limit=limit
        )
    except NoConnectionException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))


@router.get(
    '/{connection_name}/{daemon_id}',
    response_model=monitoring_dto.RemoteConnection,
    status_code=status.HTTP_200_OK,
    description='Retrieve Host connection properties',
)
async def get_connection(
    connection_name: str,
    daemon_id: str,
    token: AccessToken = Depends(bearer_or_basic_auth_admin),
):
    try:
        return await monitoring_service.get_connection(name=connection_name, daemon_id=daemon_id)
    except UnknownRemoteConnectionNameException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))


@router.delete(
    '/{connection_name}/{daemon_id}',
    status_code=status.HTTP_204_NO_CONTENT,
    description='Delete a Host connection',
)
async def delete_connection(
    connection_name: str,
    daemon_id: str,
    token: AccessToken = Depends(bearer_or_basic_auth_admin),
):
    try:
        return await monitoring_service.delete_connection(name=connection_name, daemon_id=daemon_id)
    except UnknownRemoteConnectionNameException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))

@router.put(
    '/',
    response_model=monitoring_dto.RemoteConnection,
    description='Update a Host connection properties',
)
async def update_connection(
    connection: monitoring_dto.RemoteConnectionUpdate,
    token: AccessToken = Depends(bearer_or_basic_auth_admin),
):
    try:
        # Check cluster_id
        await monitoring_service.get_cluster(connection.cluster_id)

        return await monitoring_service.update_connection(connection_update=connection)
    except UnknownRemoteConnectionNameException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except UnknownClusterIdException as error:
        raise HTTPException(status_code=status.HTTP_424_FAILED_DEPENDENCY, detail=str(error))
