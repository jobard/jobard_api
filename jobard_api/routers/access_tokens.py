"""Jobard API access tokens router."""
from typing import Optional

from fastapi import APIRouter, HTTPException, Depends
from fastapi import status

from jobard_api.dependencies.auth import bearer_or_basic_auth_admin
from jobard_api.docker.exceptions import (
    AlreadyRegisteredException,
)
from jobard_api.users import dto as access_token_dto
from jobard_api.users import service as access_token_service
from jobard_api.users.dto import AccessToken
from jobard_api.users.exceptions import UnknownUserIdException, \
    UnknownAccessTokenIdException, NoAccessTokenException, AdminAccessTokenCreationForbidden, \
    AdminAccessTokenUpdateForbidden
from jobard_api.users.service import get_user

router = APIRouter(
    prefix='/{0}'.format('access-tokens'),
    tags=['{0}'.format('access-tokens')]
)


@router.post(
    '/',
    status_code=status.HTTP_201_CREATED,
    description='Create an access token',
)
async def create_access_token(
    access_token_input: access_token_dto.AccessTokenInput,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        # Check user validity
        await get_user(user_id=access_token_input.user_id)

        return await access_token_service.create_access_token(access_token_input=access_token_input)
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except UnknownUserIdException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except AdminAccessTokenCreationForbidden as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))


@router.get(
    '/{access_token_id}',
    response_model=access_token_dto.AccessToken,
    status_code=status.HTTP_200_OK,
    description='Retrieve access token properties',
)
async def get_access_token(
    access_token_id: int,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        access_token = await access_token_service.get_access_token(access_token_id=access_token_id)
        return access_token
    except UnknownAccessTokenIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))

@router.put(
    '/',
    response_model=access_token_dto.AccessToken,
    description='Update an access token properties',
)
async def update_access_token(
    access_token: access_token_dto.AccessTokenUpdate,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        # Check user validity
        await get_user(user_id=access_token.user_id)

        return await access_token_service.update_access_token(access_token_update=access_token)
    except UnknownAccessTokenIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except UnknownUserIdException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except AdminAccessTokenUpdateForbidden as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))


@router.delete(
    '/{access_token_id}',
    status_code=status.HTTP_204_NO_CONTENT,
    description='Delete an access token',
)
async def delete_access_token(
    access_token_id: int,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        return await access_token_service.delete_access_token(access_token_id=access_token_id)
    except UnknownAccessTokenIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))

@router.get(
    '',
    response_model=access_token_dto.AccessTokenList,
    status_code=status.HTTP_200_OK,
    description='Retrieve access tokens list',
)
async def get_access_tokens(
    offset: Optional[int] = 0,
    limit: Optional[int] = 1000,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        return await access_token_service.get_access_tokens(
            offset=offset,
            limit=limit,
        )
    except NoAccessTokenException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))


