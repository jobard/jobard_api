"""Jobard API Remote connection cluster router."""

from fastapi import APIRouter, Depends, HTTPException
from fastapi import status

from jobard_api.dependencies.auth import bearer_or_basic_auth_admin
from jobard_api.core.exceptions import AlreadyRegisteredException
from jobard_api.monitoring import dto as monitoring_dto
from jobard_api.monitoring import service as monitoring_service
from jobard_api.users.dto import AccessToken

router = APIRouter(
    prefix='/{0}'.format('connections-cluster'),
    tags=['{0}'.format('connections-cluster')]
)

@router.post(
    '/',
    status_code=status.HTTP_201_CREATED,
    description='Create a Host connection with a cluster',
)
async def create_connection_cluster(
    connection_cluster_input: monitoring_dto.RemoteConnectionClusterInput,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        return await monitoring_service.create_connection_cluster(connection_cluster_input=connection_cluster_input)
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))


