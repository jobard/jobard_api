"""Jobard API jobs router."""

from fastapi import APIRouter, Depends, HTTPException
from fastapi import status

from jobard_api.dependencies.auth import bearer_or_basic_auth
from jobard_api.jobs import dto as job_dto, service as job_service
from jobard_api.jobs.exceptions import UnknownJobIdException, ForbiddenJobOrderException
from jobard_api.users.dto import AccessToken

router = APIRouter(
    prefix='/{0}'.format('jobs'),
    tags=['{0}'.format('jobs')]
)


@router.get(
    '/{job_id}',
    response_model=job_dto.Job,
    status_code=status.HTTP_200_OK,
    description='Retrieve a job details.',
)
async def get_job(job_id: int, token: AccessToken = Depends(bearer_or_basic_auth)):
    """Retrieve a job.

    Args:
        job_id: Job ID
        token: The token

    Returns:
        The corresponding Job

    Raises:
        HTTPException: if the job is unknown or the job order belongs to another user
    """
    try:
        return await job_service.get_job(job_id=job_id, user_id=token.user_id)
    except UnknownJobIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except ForbiddenJobOrderException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))
