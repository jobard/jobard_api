"""Jobard API Docker mount points router."""
from typing import Optional

from fastapi import APIRouter, Depends, HTTPException
from fastapi import status

from jobard_api.dependencies.auth import bearer_or_basic_auth
from jobard_api.docker import dto as docker_dto
from jobard_api.docker import service as docker_service
from jobard_api.docker.exceptions import (
    UnknownImageIdException,
    AlreadyRegisteredException,
    UnknownMountPointIdException,
    ForbiddenDockerImageException,
)
from jobard_api.users.dto import AccessToken

router = APIRouter(
    prefix='/{0}'.format('docker-mount-points'),
    tags=['{0}'.format('docker-mount-points')]
)


@router.post(
    '/',
    status_code=status.HTTP_201_CREATED,
    description='Create a Docker mount point',
)
async def create_docker_mount_point(
    mount_point_input: docker_dto.DockerMountPointInput,
    token: AccessToken = Depends(bearer_or_basic_auth),
):
    try:
        return await docker_service.create_docker_mount_point(
            mount_point_input=mount_point_input,
            user_id=token.user_id,
        )
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except ForbiddenDockerImageException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))
    except UnknownImageIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))


@router.get(
    '/{image_id}',
    response_model=docker_dto.DockerMountPointList,
    status_code=status.HTTP_200_OK,
    description='Retrieve Docker mount points',
)
async def get_docker_mount_points(
    image_id: int,
    token: AccessToken = Depends(bearer_or_basic_auth),
    offset: Optional[int] = 0,
    limit: Optional[int] = 1000,
):
    try:
        return await docker_service.get_docker_mount_points(
            user_id=token.user_id,
            image_id=image_id,
            offset=offset,
            limit=limit,
        )
    except UnknownImageIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except ForbiddenDockerImageException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))


@router.get(
    '/{mount_point_id}/details',
    response_model=docker_dto.DockerMountPoint,
    status_code=status.HTTP_200_OK,
    description='Retrieve Docker mount point properties',
)
async def get_docker_mount_point(
    mount_point_id: int,
    token: AccessToken = Depends(bearer_or_basic_auth),
):
    try:
        return await docker_service.get_docker_mount_point(mount_point_id=mount_point_id, user_id=token.user_id)
    except (UnknownMountPointIdException, UnknownImageIdException) as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except ForbiddenDockerImageException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))


@router.put(
    '/',
    response_model=docker_dto.DockerMountPoint,
    description='Update a Docker mount point''s properties',
)
async def update_docker_mount_point(
    docker_mount_point: docker_dto.DockerMountPointUpdate,
    token: AccessToken = Depends(bearer_or_basic_auth),
):
    try:
        return await docker_service.update_docker_mount_point(
            mount_point_update=docker_mount_point,
            user_id=token.user_id,
        )
    except (UnknownMountPointIdException, UnknownImageIdException) as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except ForbiddenDockerImageException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))


@router.delete(
    '/{mount_point_id}',
    status_code=status.HTTP_204_NO_CONTENT,
    description='Delete a Docker mount point',
)
async def delete_docker_mount_point(
    mount_point_id: int,
    token: AccessToken = Depends(bearer_or_basic_auth),
):
    try:
        return await docker_service.delete_docker_mount_point(mount_point_id=mount_point_id, user_id=token.user_id)
    except (UnknownMountPointIdException, UnknownImageIdException) as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except ForbiddenDockerImageException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))
