"""Jobard API Users router."""
from typing import Optional

from fastapi import APIRouter, HTTPException, Depends
from fastapi import status

from jobard_api.dependencies.auth import bearer_or_basic_auth_admin
from jobard_api.docker.exceptions import (
    AlreadyRegisteredException,
)
from jobard_api.users import dto as users_dto
from jobard_api.users import service as users_service
from jobard_api.users.dto import AccessToken
from jobard_api.users.exceptions import UnknownUserIdException, AdminUserDeletionForbidden

router = APIRouter(
    prefix='/{0}'.format('users'),
    tags=['{0}'.format('users')]
)


@router.post(
    '/',
    status_code=status.HTTP_201_CREATED,
    description='Create a Jobard user',
)
async def create_user(
    user_input: users_dto.JobardUserInput,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        return await users_service.create_user(user_input=user_input)
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))


@router.get(
    '/{user_id}',
    response_model=users_dto.JobardUser,
    status_code=status.HTTP_200_OK,
    description='Retrieve Jobard user properties',
)
async def get_jobard_user(
    user_id: int,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        user = await users_service.get_user(user_id=user_id)
        return user
    except UnknownUserIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))

@router.put(
    '/',
    response_model=users_dto.JobardUser,
    description='Update a Jobard user properties',
)
async def update_jobard_user(
    user: users_dto.JobardUserUpdate,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        return await users_service.update_user(user_update=user)
    except UnknownUserIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))


@router.delete(
    '/{user_id}',
    status_code=status.HTTP_204_NO_CONTENT,
    description='Delete a Jobard user',
)
async def delete_jobard_user(
    user_id: int,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        return await users_service.delete_user(user_id=user_id)
    except UnknownUserIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except AdminUserDeletionForbidden as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))

@router.get(
    '',
    response_model=users_dto.JobardUserList,
    status_code=status.HTTP_200_OK,
    description='Retrieve Jobard users',
)
async def get_jobard_users(
    offset: Optional[int] = 0,
    limit: Optional[int] = 1000,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    return await users_service.get_users(
        offset=offset,
        limit=limit,
    )

