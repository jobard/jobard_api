"""Jobard API Cluster router."""
from typing import Optional

from fastapi import APIRouter, Depends, HTTPException
from fastapi import status

from jobard_api.dependencies.auth import bearer_or_basic_auth_admin
from jobard_api.docker.exceptions import (
    AlreadyRegisteredException,
)
from jobard_api.monitoring import dto as monitoring_dto
from jobard_api.monitoring import service as monitoring_service
from jobard_api.monitoring.exceptions import UnknownClusterIdException, RemoteConnectionExistForClusterException, \
    ClusterAccessExistForClusterException, NoClusterException
from jobard_api.users.dto import AccessToken

router = APIRouter(
    prefix='/{0}'.format('clusters'),
    tags=['{0}'.format('clusters')]
)


@router.post(
    '/',
    status_code=status.HTTP_201_CREATED,
    description='Create a Cluster',
)
async def create_cluster(
    cluster_input: monitoring_dto.ClusterInput,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        return await monitoring_service.create_cluster(cluster_input=cluster_input)
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))


@router.get(
    '',
    response_model=monitoring_dto.ClusterList,
    status_code=status.HTTP_200_OK,
    description='Retrieve Clusters',
)
async def get_clusters(
    offset: Optional[int] = 0,
    limit: Optional[int] = 1000,
):
    try:
        return await monitoring_service.get_clusters(
            offset=offset,
            limit=limit
        )
    except NoClusterException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))


@router.get(
    '/{cluster_id}',
    response_model=monitoring_dto.Cluster,
    status_code=status.HTTP_200_OK,
    description='Retrieve Cluster properties',
)
async def get_cluster(
    cluster_id: int,
    token: AccessToken = Depends(bearer_or_basic_auth_admin),
):
    try:
        cluster = await monitoring_service.get_cluster(cluster_id=cluster_id)
        return cluster
    except UnknownClusterIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))


@router.put(
    '/',
    response_model=monitoring_dto.Cluster,
    description='Update a Cluster''s properties',
)
async def update_cluster(
    cluster: monitoring_dto.ClusterUpdate,
    token: AccessToken = Depends(bearer_or_basic_auth_admin),
):
    try:
        return await monitoring_service.update_cluster(cluster_update=cluster)
    except UnknownClusterIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))


@router.delete(
    '/{cluster_id}',
    status_code=status.HTTP_204_NO_CONTENT,
    description='Delete a Cluster',
)
async def delete_cluster(
    cluster_id: int,
    token: AccessToken = Depends(bearer_or_basic_auth_admin),
):
    try:
        return await monitoring_service.delete_cluster(cluster_id=cluster_id)
    except UnknownClusterIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except RemoteConnectionExistForClusterException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except ClusterAccessExistForClusterException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))


