"""Jobard API Cluster access router."""
from typing import Optional

from fastapi import APIRouter, HTTPException, Depends
from fastapi import status

from jobard_api.dependencies.auth import bearer_or_basic_auth_admin
from jobard_api.docker.exceptions import (
    AlreadyRegisteredException,
)
from jobard_api.monitoring.exceptions import UnknownClusterIdException
from jobard_api.monitoring.service import get_cluster
from jobard_api.users import dto as cluster_access_dto
from jobard_api.users import service as cluster_access_service
from jobard_api.users.dto import AccessToken
from jobard_api.users.exceptions import UnknownClusterAccessIdException, UnknownUserIdException, \
    NoClusterAccessException
from jobard_api.users.service import get_user

router = APIRouter(
    prefix='/{0}'.format('cluster-accesses'),
    tags=['{0}'.format('cluster-accesses')]
)


@router.post(
    '/',
    status_code=status.HTTP_201_CREATED,
    description='Create a Cluster access',
)
async def create_cluster_access(
    cluster_access_input: cluster_access_dto.ClusterAccessInput,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        # Check cluster and user validity
        await get_user(user_id=cluster_access_input.user_id)
        await get_cluster(cluster_id=cluster_access_input.cluster_id)

        return await cluster_access_service.create_cluster_access(cluster_access_input=cluster_access_input)
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except UnknownClusterIdException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except UnknownUserIdException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))


@router.get(
    '/{cluster_access_id}',
    response_model=cluster_access_dto.ClusterAccess,
    status_code=status.HTTP_200_OK,
    description='Retrieve Cluster access properties',
)
async def get_cluster_access(
    cluster_access_id: int,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        cluster_access = await cluster_access_service.get_cluster_access(cluster_access_id=cluster_access_id)
        return cluster_access
    except UnknownClusterAccessIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))

@router.put(
    '/',
    response_model=cluster_access_dto.ClusterAccess,
    description='Update a Cluster access properties',
)
async def update_cluster_access(
    cluster_access: cluster_access_dto.ClusterAccessUpdate,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        # Check cluster and user validity
        await get_user(user_id=cluster_access.user_id)
        await get_cluster(cluster_id=cluster_access.cluster_id)

        return await cluster_access_service.update_cluster_access(cluster_access_update=cluster_access)
    except UnknownClusterAccessIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except UnknownClusterIdException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except UnknownUserIdException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))


@router.delete(
    '/{cluster_access_id}',
    status_code=status.HTTP_204_NO_CONTENT,
    description='Delete a Cluster access',
)
async def delete_cluster_access(
    cluster_access_id: int,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        return await cluster_access_service.delete_cluster_access(cluster_access_id=cluster_access_id)
    except UnknownClusterAccessIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))

@router.get(
    '',
    response_model=cluster_access_dto.ClusterAccessList,
    status_code=status.HTTP_200_OK,
    description='Retrieve Cluster access list',
)
async def get_cluster_accesses(
    offset: Optional[int] = 0,
    limit: Optional[int] = 1000,
    token: AccessToken = Depends(bearer_or_basic_auth_admin)
):
    try:
        return await cluster_access_service.get_cluster_accesses(
            offset=offset,
            limit=limit,
        )
    except NoClusterAccessException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))

