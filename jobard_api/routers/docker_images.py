"""Jobard API Docker images router."""
from typing import Optional

from fastapi import APIRouter, Depends, HTTPException
from fastapi import status

from jobard_api.dependencies.auth import bearer_or_basic_auth
from jobard_api.docker import dto as docker_dto
from jobard_api.docker import service as docker_service
from jobard_api.docker.exceptions import (
    UnknownImageIdException,
    AlreadyRegisteredException,
    ForbiddenDockerImageException,
)
from jobard_api.users.dto import AccessToken
from jobard_api.users.exceptions import UnknownUserIdException

router = APIRouter(
    prefix='/{0}'.format('docker-images'),
    tags=['{0}'.format('docker-images')]
)


@router.post(
    '/',
    status_code=status.HTTP_201_CREATED,
    description='Create a Docker image',
)
async def create_docker_image(
    docker_image_input: docker_dto.DockerImageInput,
    token: AccessToken = Depends(bearer_or_basic_auth)
):
    try:
        return await docker_service.create_docker_image(image_input=docker_image_input, user_id=token.user_id)
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))


@router.get(
    '',
    response_model=docker_dto.DockerImageList,
    status_code=status.HTTP_200_OK,
    description='Retrieve Docker images',
)
async def get_docker_images(
    token: AccessToken = Depends(bearer_or_basic_auth),
    offset: Optional[int] = 0,
    limit: Optional[int] = 1000,
):
    try:
        return await docker_service.get_docker_images(
            user_id=token.user_id,
            offset=offset,
            limit=limit,
        )
    except UnknownUserIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))


@router.get(
    '/{image_id}',
    response_model=docker_dto.DockerImage,
    status_code=status.HTTP_200_OK,
    description='Retrieve Docker image properties',
)
async def get_docker_image(
    image_id: int,
    token: AccessToken = Depends(bearer_or_basic_auth),
):
    try:
        return await docker_service.get_docker_image(image_id=image_id, user_id=token.user_id)
    except UnknownImageIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except ForbiddenDockerImageException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))


@router.put(
    '/',
    response_model=docker_dto.DockerImage,
    description='Update a Docker image''s properties',
)
async def update_docker_image(
    docker_image: docker_dto.DockerImageUpdate,
    token: AccessToken = Depends(bearer_or_basic_auth),
):
    try:
        return await docker_service.update_docker_image(docker_image_update=docker_image, user_id=token.user_id)
    except UnknownImageIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except AlreadyRegisteredException as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    except ForbiddenDockerImageException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))


@router.delete(
    '/{image_id}',
    status_code=status.HTTP_204_NO_CONTENT,
    description='Delete a Docker image and its potential mount points',
)
async def delete_docker_image(
    image_id: int,
    token: AccessToken = Depends(bearer_or_basic_auth),
):
    try:
        return await docker_service.delete_docker_image(image_id=image_id, user_id=token.user_id)
    except UnknownImageIdException as error:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(error))
    except ForbiddenDockerImageException as error:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(error))
