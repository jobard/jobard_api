"""Jobard API authentication router."""
import typing as t
from typing import Optional

from fastapi import Depends, HTTPException
from fastapi import status
from fastapi.security.http import HTTPAuthorizationCredentials, HTTPBearer, HTTPBasic, HTTPBasicCredentials

from jobard_api.users import service as users_service
from jobard_api.users.dto import AccessToken
from jobard_api.users.exceptions import InvalidTokenException, ExpiredTokenException, NotAdminTokenException


async def basic_auth(
    auth: t.Optional[HTTPBasicCredentials] = Depends(HTTPBasic(auto_error=False)),
) -> Optional[AccessToken]:
    """Validate Basic authentification

      Args:
          auth: HTTP credentials
      Returns:
          The validated token.
    """
    # Check the token
    if auth is None:
        return None
    # Validate the token by requesting it in database
    try:
        token = await users_service.validate_access_token(auth.password)
    except (InvalidTokenException, ExpiredTokenException) as error:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=str(error),
        )
    return token


async def bearer_auth(
    auth: t.Optional[HTTPAuthorizationCredentials] = Depends(HTTPBearer(auto_error=False)),
) -> Optional[AccessToken]:
    """Validate Bearer authentification.

      Args:
          auth: HTTP credentials
      Returns:
          The validated token.
    """
    # Check the token
    if auth is None:
        return auth
    # Validate the token by requesting it in database
    try:
        token = await users_service.validate_access_token(auth.credentials)
    except (InvalidTokenException, ExpiredTokenException) as error:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=str(error),
        )
    return token


async def bearer_or_basic_auth(
    bearer_result=Depends(bearer_auth),
    basic_result=Depends(basic_auth),
) -> AccessToken:
    """Validate authentication. Available authentication are Basic or Bearer.

      Returns:
          The validated token.
    """
    if not (bearer_result or basic_result):
        raise HTTPException(status_code=401, detail="Not authenticated")
    if basic_result:
        return basic_result
    if bearer_result:
        return bearer_result

async def basic_auth_admin(
    auth: t.Optional[HTTPBasicCredentials] = Depends(HTTPBasic(auto_error=False)),
) -> Optional[AccessToken]:
    """Validate Basic authentification for admin token

      Args:
          auth: HTTP credentials
      Returns:
          The validated token.
    """
    # Check the token
    if auth is None:
        return None
    # Validate the token by requesting it in database
    try:
        token = await users_service.validate_access_token(token=auth.password, admin=True)
    except (InvalidTokenException, ExpiredTokenException, NotAdminTokenException) as error:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=str(error),
        )
    return token


async def bearer_auth_admin(
    auth: t.Optional[HTTPAuthorizationCredentials] = Depends(HTTPBearer(auto_error=False)),
) -> Optional[AccessToken]:
    """Validate Bearer authentification dor admin token.

      Args:
          auth: HTTP credentials
      Returns:
          The validated token.
    """
    # Check the token
    if auth is None:
        return auth
    # Validate the token by requesting it in database
    try:
        token = await users_service.validate_access_token(token=auth.credentials,admin=True)
    except (InvalidTokenException, ExpiredTokenException, NotAdminTokenException) as error:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=str(error),
        )
    return token


async def bearer_or_basic_auth_admin(
    bearer_result=Depends(bearer_auth_admin),
    basic_result=Depends(basic_auth_admin),
) -> AccessToken:
    """Validate administration authentication. Available authentication are Basic or Bearer.

      Returns:
          The validated token.
    """
    if not (bearer_result or basic_result):
        raise HTTPException(status_code=401, detail="Not authenticated")
    if basic_result:
        return basic_result
    if bearer_result:
        return bearer_result