"""Configure the application from environment variables."""
from functools import lru_cache
from pathlib import Path
from typing import List

from pydantic import BaseSettings, PostgresDsn, validator

from jobard_api.conf.version import get_version

AES_KEY_FILE = 'aes-key'
ADMIN_TOKEN_FILE = 'admin-token' # noqa: S105

class AppSettings(BaseSettings):
    """Jobard API settings."""

    debug: bool = False
    context_path: str = ''
    version: str = get_version()
    allowed_hosts: List[str] = ['*']

    database_dsn: PostgresDsn = 'postgresql://jobard:jobard@localhost:5432/jobard'

    # Directory for Docker secrets
    secrets_dir: str = '/run/secrets'

    @validator('database_dsn')
    def check_database_name(cls, dsn):  # noqa: N805, D102
        assert dsn.path and len(dsn.path) > 1, 'database name must be provided'  # noqa: S101
        return dsn

    @validator('secrets_dir')
    def check_secrets_dir(cls, secrets_dir):  # noqa: N805, D102
        assert secrets_dir, 'Secrets dir must be provided'  # noqa: S101
        assert Path(secrets_dir).exists(), 'Secrets dir must exists' # noqa: S101
        return secrets_dir

    def get_aes_key(self) -> str:
        """Returns the AES key for passwords and SSH keys database encryption."""
        assert Path(self.secrets_dir,AES_KEY_FILE).exists(), 'AES key file must exists' # noqa: S101
        aes_key_path = Path(self.secrets_dir, AES_KEY_FILE)
        # Read the AES key from the path
        with open(aes_key_path) as f:
            aes_key = f.read().strip()

        assert aes_key, 'Missing AES key'
        assert len(aes_key) > 0, 'AES key is empty' # noqa: S101
        return aes_key

    def get_admin_token(self) -> str:
        """Returns the root user token."""
        assert Path(self.secrets_dir,ADMIN_TOKEN_FILE).exists(), 'admin token file must exists' # noqa: S101
        admin_token_path = Path(self.secrets_dir, ADMIN_TOKEN_FILE)
        # Read the token from the path
        with open(admin_token_path) as f:
            admin_token = f.read().strip()

        assert admin_token, 'Missing admin token'
        assert len(admin_token) > 0, 'Admin token is empty' # noqa: S101
        return admin_token

    class Config(object):  # noqa: D106, WPS431
        validate_assignment = True
        env_prefix = 'jobard_api_'


@lru_cache()
def get_settings():
    """Store settings in cache.

    Returns:
        Jobard Settings instance
    """
    return AppSettings()


settings = get_settings()
