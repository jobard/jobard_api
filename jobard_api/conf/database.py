"""Database driver."""
import databases
import sqlalchemy as sa

from jobard_api.conf.settings import settings

metadata = sa.MetaData()

database = databases.Database(settings.database_dsn)
