"""Jobard database models for users management"""

import sqlalchemy as sa

from jobard_api.conf.database import metadata
from jobard_api.conf.settings import settings
from jobard_api.core.alembic.custom_types import PGPString

users = sa.Table(
    'jobard_user',
    metadata,
    sa.Column('user_id', sa.Integer, primary_key=True, autoincrement=True),
    sa.Column('username', sa.String, unique=True),
    sa.Column('email', sa.String, unique=True),
    sa.Column('remote_username', sa.String),
    sa.Column('password', PGPString(settings.get_aes_key()), nullable=True),
    sa.Column('client_key', PGPString(settings.get_aes_key()), nullable=True),
    sa.Column('unix_uid', sa.Integer, nullable=True),
    sa.Column('enabled', sa.Boolean, default=True),
)

access_tokens = sa.Table(
    'access_token',
    metadata,
    sa.Column('token_id', sa.Integer, primary_key=True, autoincrement=True),
    sa.Column('user_id', sa.Integer, sa.ForeignKey('user.user_id')),
    sa.Column('access_token', sa.String, primary_key=True),
    sa.Column('expiration_date', sa.TIMESTAMP, nullable=True),
)

cluster_access = sa.Table(
    'cluster_access',
    metadata,
    sa.Column('cluster_access_id', sa.Integer, primary_key=True, autoincrement=True),
    sa.Column('cluster_access_name', sa.String, unique=True),
    sa.Column('user_id', sa.Integer, sa.ForeignKey('jobard_user.user_id')),
    sa.Column('cluster_id', sa.Integer, sa.ForeignKey('cluster.cluster_id')),
    sa.Column('log_root', sa.String, nullable=True),
    sa.Column('remote_app_path', sa.String, nullable=True),
    sa.Column('entry_point_wrapper', sa.String, nullable=True),
    sa.Column('entry_point', sa.String),
    sa.Column('enabled', sa.Boolean, default=True),
)
