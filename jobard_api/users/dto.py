"""Jobard users DTO."""
from __future__ import annotations

from typing import Optional, List
from pydantic.types import SecretStr

from jobard_api.core.dto import JobardModel
from jobard_api.jobs.dto import JobardListModel


class JobardUserInput(JobardModel):
    """Jobard user DTO."""

    username: str
    email: str
    remote_username: str
    password: Optional[str]
    client_key: Optional[str]
    unix_uid: Optional[int]


class JobardUser(JobardModel):
    """Jobard user DTO."""

    username: str
    email: str
    remote_username: str
    password: Optional[SecretStr]
    client_key: Optional[SecretStr]
    unix_uid: Optional[int]
    user_id: int
    enabled: bool = True

    def get_password_string(self) -> Optional[str]:
        return self.password.get_secret_value() if self.password else None

    def get_client_key_string(self) -> Optional[str]:
        return self.client_key.get_secret_value() if self.client_key else None


class JobardUserList(JobardListModel[JobardUser]):
    """List of Jobard users DTO."""

class JobardUserUpdate(JobardModel):
    """Jobard user DTO."""

    user_id: int
    username: str
    email: str
    remote_username: str
    password: Optional[str]
    client_key: Optional[str]
    unix_uid: Optional[int]


class ClusterAccessInput(JobardModel):
    """Cluster access DTO."""

    cluster_access_name: str
    user_id: int
    cluster_id: int
    log_root: Optional[str] = None
    remote_app_path: Optional[str] = None
    entry_point_wrapper: Optional[str] = None
    entry_point: str


class ClusterAccess(ClusterAccessInput):
    """Cluster access DTO."""

    cluster_access_id: int
    enabled: bool = True

class ClusterAccessUpdate(JobardModel):
    """Cluster access DTO."""

    cluster_access_id: int
    cluster_access_name: str
    user_id: int
    cluster_id: int
    log_root: Optional[str] = None
    remote_app_path: Optional[str] = None
    entry_point_wrapper: Optional[str] = None
    entry_point: str

class ClusterAccessList(JobardListModel[ClusterAccess]):
    """List of cluster access DTO."""

class AccessTokenInput(JobardModel):
    """Access token DTO."""

    user_id: int
    access_token: str

class AccessToken(AccessTokenInput):
    """Access token DTO."""

    token_id: int

class AccessTokenUpdate(JobardModel):
    """Access token DTO."""

    token_id: int
    user_id: int
    access_token: str

class AccessTokenList(JobardListModel[AccessToken]):
    """List of access tokens DTO."""


class UserClusterAccessInput(JobardModel):
    """Cluster access DTO for a user"""

    cluster_access_name: str
    cluster_id: int
    log_root: Optional[str] = None
    remote_app_path: Optional[str] = None
    entry_point_wrapper: Optional[str] = None
    entry_point: str

class UserAccessesInput(JobardModel):
    """User, cluster accesses and token DTO."""
    user: JobardUserInput
    access_token: str
    cluster_accesses: Optional[List[UserClusterAccessInput]] = None
