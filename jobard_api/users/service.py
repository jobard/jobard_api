"""Jobard users service."""
from datetime import datetime
from typing import List, NoReturn, Dict

from pydantic.types import SecretStr
from sqlalchemy import true, and_, func

from jobard_api.conf.database import database
from jobard_api.conf.settings import settings
from jobard_api.core.types import ClusterType
from jobard_api.docker.models import docker_images
from jobard_api.monitoring.service import get_cluster
from jobard_api.users import dto
from jobard_api.users.dto import ClusterAccess, ClusterAccessList, AccessTokenList, AccessToken, AccessTokenInput, \
    ClusterAccessInput
from jobard_api.users.exceptions import (
    ExpiredTokenException,
    InvalidTokenException,
    DeniedClusterAccessException,
    UnknownUserIdException, AlreadyRegisteredJobardUserException, AlreadyRegisteredJobardUserEmailException,
    NotAdminTokenException, AdminUserDeletionForbidden, AlreadyRegisteredClusterAccessException,
    UnknownClusterAccessIdException, UnknownAccessTokenIdException, AlreadyRegisteredAccessTokenException,
    MandatoryPasswordOrKeyException, MandatoryRemoteUsernameException, NoAccessTokenException, NoClusterAccessException,
    AdminAccessTokenCreationForbidden, AdminAccessTokenUpdateForbidden,
)
from jobard_api.users.models import users, cluster_access, access_tokens

COUNT_FIELD = 'count'

CLUSTER_ACCESS_QUERY = """
    SELECT 
      "cl"."cluster_id" AS cluster_id,
      "cl"."type" AS cluster_type,
      "cl"."password" AS cl_password,
      "cl"."client_key" AS cl_client_key,
      "cl"."remote_username" AS cl_remote_username,
      "j_user"."password" AS j_user_password,
      "j_user"."client_key" AS j_user_client_key,
      "j_user"."remote_username" AS j_user_remote_username
    FROM
      "remote_connection" AS rc
      INNER JOIN "cluster" AS "cl" ON "rc"."cluster_id" = "cl"."cluster_id" 
      INNER JOIN "cluster_access" AS "cla" ON "cla"."cluster_id" = "cl"."cluster_id"
      INNER JOIN "jobard_user" as "j_user" ON "j_user"."user_id" = "cla"."user_id"
    WHERE
      "rc"."name" = '{0}' AND "j_user"."user_id" = {1} 
      AND "rc"."enabled" = TRUE AND "cl"."enabled" = TRUE 
      AND "cla"."enabled" = TRUE AND "j_user"."enabled" = TRUE;
"""

USER_CLUSTER_TYPES_QUERY = """
    SELECT
      "cl"."type" AS cluster_type
    FROM
      "cluster_access" as ca
      INNER JOIN "cluster" as "cl" ON "cl"."cluster_id" = "ca"."cluster_id"
    WHERE
      "ca"."user_id" = {0};
"""

async def create_user(user_input: dto.JobardUserInput) -> int:
    """Create a new Jobard user.

    Args:
        user_input: Jobard user information

    Returns:
        Jobard user ID
    """

    # Check unicity constraints
    if user_input.username:
        await _check_users_name_uk(name=user_input.username)
    if user_input.email:
        await _check_users_email_uk(email=user_input.email)

    query = users.insert().values({'enabled': True, **user_input.dict()})
    return await database.execute(query)

async def get_user(user_id: int) -> dto.JobardUser:
    """Retrieve a Jobard user by id.

    Args:
        user_id: The user ID
    Returns:
        The corresponding Jobard user if enabled

    Raises:
        UnknownUserIdException: If the Jobard user's id is unknown or disabled
    """
    query = users.select().where(and_(users.c.user_id == user_id, users.c.enabled == true()))
    user_response = await database.fetch_one(query)
    if not user_response:
        raise UnknownUserIdException(user_id)
    return dto.JobardUser.parse_obj(user_response)



async def _check_users_name_uk(name: str):
    """Check user name unicity constraint on jobard users.

    Args:
        name: User name

    Raises:
        AlreadyRegisteredJobardUserException: If the user name already exists
    """
    query = users.select().where(
        users.c.username == name
    )
    response = await database.fetch_one(query)

    if response:
        user = dto.JobardUser.parse_obj(response)
        if user:
            raise AlreadyRegisteredJobardUserException(name)

async def _check_users_email_uk(email: str):
    """Check email unicity constraint on jobard users.

    Args:
        email: User email

    Raises:
        AlreadyRegisteredJobardUserEmailException: If the user email already exists
    """
    query = users.select().where(
        users.c.email == email
    )
    response = await database.fetch_one(query)

    if response:
        user = dto.JobardUser.parse_obj(response)
        if user:
            raise AlreadyRegisteredJobardUserEmailException(email)

async def update_user(user_update: dto.JobardUserUpdate) -> dto.JobardUser:
    """Update a Jobard user if enabled

    Args:
        user_update: Jobard user information

    Returns:
        The updated Jobard user
    """

    # Check user_id validity access to user
    db_user = await get_user(user_id=user_update.user_id)
    if user_update.username and user_update.username != db_user.username:
        # Check unicity constraint
        await _check_users_name_uk(name=user_update.username)
        db_user.username = user_update.username
    if user_update.email and user_update.email != db_user.email:
        # Check unicity constraint
        await _check_users_email_uk(email=user_update.email)
        db_user.email = user_update.email
    if user_update.remote_username and user_update.remote_username != db_user.remote_username:
        db_user.remote_username = user_update.remote_username
    if user_update.unix_uid != db_user.unix_uid:
        db_user.unix_uid = user_update.unix_uid

    if user_update.password != db_user.get_password_string():
        db_user.password = SecretStr(user_update.password)  if user_update.password else None
    if user_update.client_key != db_user.get_client_key_string():
        db_user.client_key = SecretStr(user_update.client_key)  if user_update.client_key else None

    update_query = (
        (
            users
            .update()
            .where(users.c.user_id == db_user.user_id)
        )
        .values(
            username= db_user.username,
            email=db_user.email,
            remote_username=db_user.remote_username,
            password=db_user.password.get_secret_value() if db_user.password else None,
            client_key=db_user.client_key.get_secret_value() if db_user.client_key else None,
            unix_uid=db_user.unix_uid
        )
    )
    await database.execute(update_query)
    return db_user

async def _check_cluster_access_name_uk(name: str):
    """Check cluster access name unicity constraint on jobard users.

    Args:
        name: Cluster access name

    Raises:
        AlreadyRegisteredClusterAccessException: If the cluster access name already exists
    """
    query = cluster_access.select().where(
        cluster_access.c.cluster_access_name == name
    )
    response = await database.fetch_one(query)

    if response:
        user = dto.ClusterAccess.parse_obj(response)
        if user:
            raise AlreadyRegisteredClusterAccessException(name)

async def create_cluster_access(cluster_access_input: dto.ClusterAccessInput) -> int:
    """Create a new cluster access.

    Args:
        cluster_access_input: Cluster access information

    Returns:
        Cluster access ID
    """
    # Check unicity constraints
    if cluster_access_input.cluster_access_name:
        await _check_cluster_access_name_uk(name=cluster_access_input.cluster_access_name)

    query = cluster_access.insert().values({'enabled': True, **cluster_access_input.dict()})
    return await database.execute(query)


async def get_cluster_access(cluster_access_id: int) -> dto.ClusterAccess:
    """Retrieve a Cluster access by id.

    Args:
        cluster_access_id: The Cluster access ID
    Returns:
        The corresponding Cluster access if enabled

    Raises:
        UnknownClusterAccessIdException: If the Cluster access's id is unknown or disabled
    """
    query = cluster_access.select().where(
        and_(cluster_access.c.cluster_access_id == cluster_access_id, cluster_access.c.enabled == true())
    )
    response = await database.fetch_one(query)
    if not response:
        raise UnknownClusterAccessIdException(cluster_access_id)
    return dto.ClusterAccess.parse_obj(response)

async def delete_cluster_access(cluster_access_id: int) -> NoReturn:
    """Delete a Cluster access if enabled. Sets enabled boolean to False.

    Args:
        cluster_access_id: The Cluster access ID
    """
    # Check cluster access validity
    await get_cluster_access(cluster_access_id=cluster_access_id)
    disable_query = cluster_access.update().where(
        cluster_access.c.cluster_access_id == cluster_access_id
    ).values(enabled=False)
    await database.execute(disable_query)

async def get_cluster_accesses(
    offset: int,
    limit: int,
) -> dto.ClusterAccessList:
    """List Cluster accesses.

    Args:
        offset: first cluster access to select
        limit: maximum number of cluster accesses returned

    Returns:
        List of enabled cluster accesses
    """
    query = cluster_access.select().where(cluster_access.c.enabled == true())

    count_result = await database.fetch_one(
        query.with_only_columns(func.count(cluster_access.c.cluster_access_id).label(COUNT_FIELD))
    )

    result = await database.fetch_all(query.order_by(cluster_access.c.cluster_access_id).offset(offset).limit(limit))

    if len(result) == 0:
        raise NoClusterAccessException

    return ClusterAccessList(
        count=count_result[0],
        limit=limit,
        offset=offset,
        records=[ClusterAccess.parse_obj(ca) for ca in result],
    )

async def update_cluster_access(cluster_access_update: dto.ClusterAccessUpdate) -> dto.ClusterAccess:
    """Update a Cluster access if enabled

    Args:
        cluster_access_update: Cluster access information

    Returns:
        The updated Cluster access
    """

    # Check cluster_access_id validity access
    db_ca = await get_cluster_access(cluster_access_id=cluster_access_update.cluster_access_id)

    if cluster_access_update.cluster_access_name and cluster_access_update.cluster_access_name != db_ca.cluster_access_name:
        # Check unicity constraint
        await _check_cluster_access_name_uk(name=cluster_access_update.cluster_access_name)
        db_ca.cluster_access_name = cluster_access_update.cluster_access_name
    if cluster_access_update.user_id and cluster_access_update.user_id != db_ca.user_id:
        await get_user(user_id=cluster_access_update.user_id)
        db_ca.user_id = cluster_access_update.user_id
    if cluster_access_update.cluster_id and cluster_access_update.cluster_id != db_ca.cluster_id:
        await get_cluster(cluster_id=cluster_access_update.cluster_id)
        db_ca.cluster_id = cluster_access_update.cluster_id
    if cluster_access_update.entry_point and cluster_access_update.entry_point != db_ca.entry_point:
        db_ca.entry_point = cluster_access_update.entry_point

    if cluster_access_update.log_root != db_ca.log_root:
        db_ca.log_root = cluster_access_update.log_root
    if cluster_access_update.remote_app_path != db_ca.remote_app_path:
        db_ca.remote_app_path = cluster_access_update.remote_app_path
    if cluster_access_update.entry_point_wrapper != db_ca.entry_point_wrapper:
        db_ca.entry_point_wrapper = cluster_access_update.entry_point_wrapper

    update_query = (
        (
            cluster_access
            .update()
            .where(cluster_access.c.cluster_access_id == db_ca.cluster_access_id)
        )
        .values(**db_ca.dict())
    )
    await database.execute(update_query)
    return db_ca

async def _check_access_token_value_uk(access_token: str):
    """Check access token value unicity constraint on access tokens.

    Args:
        access_token: The access token value.

    Raises:
        AlreadyRegisteredAccessTokenException: If the token value already exists
    """
    query = access_tokens.select().where(
        access_tokens.c.access_token == access_token
    )
    response = await database.fetch_one(query)

    if response:
        user = dto.AccessToken.parse_obj(response)
        if user:
            raise AlreadyRegisteredAccessTokenException(access_token)

async def create_access_token(access_token_input: dto.AccessTokenInput) -> int:
    """Create a new access token.

    Args:
        access_token_input: Access token information

    Returns:
        Access token ID
    """

    # Check user_id validity, access token creation is forbidden for admin user
    if access_token_input.user_id == 1:
        raise AdminAccessTokenCreationForbidden

    if access_token_input.access_token:
        # Check unicity constraint
        await _check_access_token_value_uk(access_token=access_token_input.access_token)
    query = access_tokens.insert().values(**access_token_input.dict())
    return await database.execute(query)

async def get_access_token(access_token_id: int) -> dto.AccessToken:
    """Retrieve an access token by id.

    Args:
        access_token_id: The access token ID
    Returns:
        The corresponding access token if enabled

    Raises:
        UnknownAccessTokenIdException: If the access token id is unknown or disabled
    """
    query = access_tokens.select().where(
        and_(access_tokens.c.token_id == access_token_id)
    )
    response = await database.fetch_one(query)
    if not response:
        raise UnknownAccessTokenIdException(access_token_id)
    return dto.AccessToken.parse_obj(response)

async def update_access_token(access_token_update: dto.AccessTokenUpdate) -> dto.AccessToken:
    """Update an access token if enabled

    Args:
        access_token_update: access token information

    Returns:
        The updated access token
    """

    # Check user_id validity, access token update is forbidden for admin user
    if access_token_update.user_id == 1:
        raise AdminAccessTokenUpdateForbidden

    # Check token_id validity access
    db_at = await get_access_token(access_token_id=access_token_update.token_id)

    if access_token_update.access_token and access_token_update.access_token != db_at.access_token:
        # Check unicity constraint
        await _check_access_token_value_uk(access_token=access_token_update.access_token)
        db_at.access_token = access_token_update.access_token

    if access_token_update.user_id and access_token_update.user_id != db_at.user_id:
        db_at.user_id = access_token_update.user_id

    update_query = (
        (
            access_tokens
            .update()
            .where(access_tokens.c.token_id == db_at.token_id)
        )
        .values(**db_at.dict())
    )
    await database.execute(update_query)
    return db_at

async def delete_access_token(access_token_id: int) -> NoReturn:
    """Delete an access token if enabled.

    Args:
        access_token_id: The access token ID
    """
    # Check token_id validity access
    await get_access_token(access_token_id=access_token_id)
    delete_query = access_tokens.delete().where(
        access_tokens.c.token_id == access_token_id
    )
    await database.execute(delete_query)

async def get_access_tokens(
    offset: int,
    limit: int,
) -> dto.AccessTokenList:
    """List access tokens.

    Args:
        offset: first access token to select
        limit: maximum number of access token returned

    Returns:
        List of access tokens
    """
    query = access_tokens.select().where()

    count_result = await database.fetch_one(
        query.with_only_columns(func.count(access_tokens.c.token_id).label(COUNT_FIELD))
    )

    result = await database.fetch_all(query.order_by(access_tokens.c.token_id).offset(offset).limit(limit))

    if len(result) == 0:
        raise NoAccessTokenException()

    return AccessTokenList(
        count=count_result[0],
        limit=limit,
        offset=offset,
        records=[AccessToken.parse_obj(ta) for ta in result],
    )

async def validate_access_token(token: str, admin: bool = False) -> dto.AccessToken:
    """Validate an access token given by the request by checking its existence and its expiration date in database
    if admin parameter is set to False and by comparing with the token stored in admin token file if admin parameter
    is set to True.

    Args:
        token: The token to validate.
        admin: Admin token flag.

    Returns:
        The validated token.

    Raises:
        UnknownJobIdException: if the job id is unknown
    """
    # For not admin users, check the token in database
    if not admin:
        query = access_tokens.select().where(access_tokens.c.access_token == token)
        access_token = await database.fetch_one(query)
        if not access_token:
            raise InvalidTokenException
        now = datetime.today()
        if access_token['expiration_date'] is not None and access_token['expiration_date'] < now:
            raise ExpiredTokenException
    # For admin user, check the token from docker secret
    else:
        if token != settings.get_admin_token():
            raise NotAdminTokenException
        access_token = token
    return access_token


async def get_user_cluster_types(user_id: int) -> List[ClusterType]:
    """Retrieve user's cluster types

    Args:
        user_id: User id

    Returns:
        The list of user's cluster types
    """
    query = USER_CLUSTER_TYPES_QUERY.format(user_id)
    results = await database.fetch_all(query)
    return  [result['cluster_type'] for result in results]


async def validate_cluster_access(remote_connection_name: str, user_id: int) -> ClusterType:
    """Validate access to a cluster with a remote connection and a Jobard user.

    Args:
        remote_connection_name: The name of the remote connection.
        user_id: The Jobard user id.

    Returns the cluster type.

    Raises:
        DeniedClusterAccessException: if the cluster access is denied
    """

    query = CLUSTER_ACCESS_QUERY.format(remote_connection_name, user_id)
    query_result = await database.fetch_all(query)

    if len(query_result) == 0:
        raise DeniedClusterAccessException

    # For SWARM and KUBE clusters, check that the cluster password or the SSH key is not null and
    # the remote_username is not null
    cluster_type = query_result[0]['cluster_type']
    if cluster_type in (ClusterType.SWARM, ClusterType.KUBE):
        if not query_result[0]['cl_password'] and not query_result[0]['cl_client_key']:
            raise MandatoryPasswordOrKeyException('Cluster', query_result[0]['cluster_id'])
        if not query_result[0]['cl_remote_username']:
            raise MandatoryRemoteUsernameException('Cluster', query_result[0]['cluster_id'])
    # For other clusters, check that the user password or the SSH key is not null and
    # the remote_username is not null
    else:
        if not query_result[0]['j_user_password'] and not query_result[0]['j_user_client_key']:
            raise MandatoryPasswordOrKeyException('User', query_result[0]['cluster_id'])
        if not query_result[0]['j_user_remote_username']:
            raise MandatoryRemoteUsernameException('User', query_result[0]['cluster_id'])
    return cluster_type


async def get_users(
    offset: int,
    limit: int,
) -> dto.JobardUserList:
    """List Jobard users

    Args:
        offset: first user to select
        limit: maximum number of users returned

    Returns:
        List of enabled users
    """
    query = users.select().where(users.c.enabled == true())

    count_result = await database.fetch_one(
        query.with_only_columns(func.count(users.c.user_id).label(COUNT_FIELD))
    )

    result = await database.fetch_all(query.order_by(users.c.user_id).offset(offset).limit(limit))

    # Hide password and key of the users
    records = [dto.JobardUser.parse_obj(user) for user in result]
    return dto.JobardUserList(
        count=count_result[0],
        limit=limit,
        offset=offset,
        records=records
    )

async def get_user_by_id(user_id: int) -> dto.JobardUser:
    """Retrieve a user by id

    Args:
        user_id: The user id

    Returns:
        The Jobard user

    Raises:
        UnknownUserIdException: If the user id is unknown
    """
    user_query = users.select().where(users.c.user_id == user_id)
    user_response = await database.fetch_one(user_query)
    if not user_response:
        raise UnknownUserIdException(user_id)
    else:
        user = dto.JobardUser.parse_obj(user_response)
        return user


async def delete_user(user_id: int) -> NoReturn:
    """Delete a Jobard user if enabled. Sets enabled boolean to False.

    Args:
        user_id: The user ID
    """
    # Check user_id validity
    db_user = await get_user(user_id=user_id)
    if db_user.user_id == 1:
        raise AdminUserDeletionForbidden

    # Disable Docker images, cluster accesses, tokens related to this user
    disable_query = docker_images.update().where(
        docker_images.c.user_id == user_id
    ).values(enabled=False)
    await database.execute(disable_query)

    disable_query = cluster_access.update().where(
        cluster_access.c.user_id == user_id
    ).values(enabled=False)
    await database.execute(disable_query)

    disable_query = access_tokens.delete().where(
        access_tokens.c.user_id == user_id)
    await database.execute(disable_query)

    # Disable the user
    disable_query = users.update().where(users.c.user_id == user_id).values(enabled=False)
    await database.execute(disable_query)

async def create_user_accesses(user_access_input: dto.UserAccessesInput) -> int:
    """Create a new Jobard user withe cluster accesses and access token.

    Args:
        user_access_input: Jobard user, cluster accesses and token information

    Returns:
        Jobard user ID
    """

    # First check the user, the token and the cluster accesses
    await _check_users_name_uk(name=user_access_input.user.username)
    await _check_users_email_uk(email=user_access_input.user.email)
    await _check_access_token_value_uk(access_token=user_access_input.access_token)
    if user_access_input.cluster_accesses:
        for cla in user_access_input.cluster_accesses:
            await _check_cluster_access_name_uk(name=cla.cluster_access_name)
            await get_cluster(cluster_id=cla.cluster_id)

    # Then create the user, the token and the cluster accesses
    user_id = await create_user(user_input=user_access_input.user)

    token_dict = {'user_id' : user_id, 'access_token': user_access_input.access_token}
    await create_access_token(access_token_input=AccessTokenInput(**token_dict))

    if user_access_input.cluster_accesses:
        for cla in user_access_input.cluster_accesses:
            cla_dict = {
                'cluster_access_name' : cla.cluster_access_name,
                'user_id': user_id,
                'cluster_id': cla.cluster_id,
                'log_root': cla.log_root,
                'remote_app_path': cla.remote_app_path,
                'entry_point_wrapper': cla.entry_point_wrapper,
                'entry_point': cla.entry_point
            }
            await create_cluster_access(cluster_access_input=ClusterAccessInput(**cla_dict))

    return user_id
