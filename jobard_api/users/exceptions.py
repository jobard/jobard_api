"""Jobard API Users exceptions."""
from typing import Any

from jobard_api.core.exceptions import JobardApiException, UnknownIdException, AlreadyRegisteredException


class InvalidTokenException(JobardApiException):
    """Invalid token exception class."""

    def __init__(self):
        """Initialize exception."""
        super().__init__('Invalid token')

class NotAdminTokenException(JobardApiException):
    """Not admin token exception class."""

    def __init__(self):
        """Initialize exception."""
        super().__init__('Invalid admin token')

class ExpiredTokenException(JobardApiException):
    """Expired token exception class."""

    def __init__(self):
        """Initialize exception."""
        super().__init__('Token has expired. Please request a new token.')


class DeniedClusterAccessException(JobardApiException):
    """Denied cluster exception class."""

    def __init__(self):
        """Initialize exception."""
        super().__init__('The cluster is not allowed.')

class MandatoryPasswordOrKeyException(JobardApiException):
    """Mandatory password or key exception class."""

    def __init__(self, item_name: Any, item_id: Any):
        """Initialize exception."""
        super().__init__(f'{item_name} {item_id} must have a password or a SSH key')

class MandatoryRemoteUsernameException(JobardApiException):
    """Mandatory remote username exception class."""

    def __init__(self, item_name: Any, item_id: Any):
        """Initialize exception."""
        super().__init__(f'{item_name} {item_id} must have a remote username')

class UnknownUserIdException(UnknownIdException):
    """Unknown User ID exception class."""

    def __init__(self, item_id: Any):
        """Initialize exception.

        Args:
            item_id: identifier of item
        """
        super().__init__('User', item_id)

class UnknownClusterAccessIdException(UnknownIdException):
    """Unknown Cluster Access ID exception class."""

    def __init__(self, item_id: Any):
        """Initialize exception.

        Args:
            item_id: identifier of item
        """
        super().__init__('Cluster Access', item_id)

class UnknownAccessTokenIdException(UnknownIdException):
    """Unknown Access token ID exception class."""

    def __init__(self, item_id: Any):
        """Initialize exception.

        Args:
            item_id: identifier of item
        """
        super().__init__('Access token', item_id)

class AlreadyRegisteredJobardUserException(AlreadyRegisteredException):
    """AlreadyRegistered Jobard user exception class."""

    def __init__(self, item_value: Any):
        """Initialize exception.

        Args:
            item_value: value of the item
        """
        super().__init__('Jobard user', item_property='username', item_value=item_value)

class AlreadyRegisteredJobardUserEmailException(AlreadyRegisteredException):
    """AlreadyRegistered Jobard user email exception class."""

    def __init__(self, item_value: Any):
        """Initialize exception.

        Args:
            item_value: value of the item
        """
        super().__init__('Jobard user', item_property='email', item_value=item_value)


class AlreadyRegisteredClusterAccessException(AlreadyRegisteredException):
    """AlreadyRegistered Cluster access exception class."""

    def __init__(self, item_value: Any):
        """Initialize exception.

        Args:
            item_value: value of the item
        """
        super().__init__('Cluster access', item_property='cluster_access_name', item_value=item_value)

class AlreadyRegisteredAccessTokenException(AlreadyRegisteredException):
    """AlreadyRegistered Access token exception class."""

    def __init__(self, item_value: Any):
        """Initialize exception.

        Args:
            item_value: value of the item
        """
        super().__init__('Access token', item_property='access_token', item_value=item_value)


class AdminUserDeletionForbidden(JobardApiException):
    """Denied cluster exception class."""

    def __init__(self):
        """Initialize exception."""
        super().__init__('The admin user cannot be deleted')


class AdminAccessTokenCreationForbidden(JobardApiException):
    """Denied admin access token creation exception class."""

    def __init__(self):
        """Initialize exception."""
        super().__init__('Admin user access token creation forbidden')


class AdminAccessTokenUpdateForbidden(JobardApiException):
    """Denied admin access token update exception class."""

    def __init__(self):
        """Initialize exception."""
        super().__init__('Admin user access token update forbidden')


class NoAccessTokenException(JobardApiException):
    """No access token exception class."""

    def __init__(self):
        """Initialize exception."""
        super().__init__('No access token found')

class NoClusterAccessException(JobardApiException):
    """No cluster access exception class."""

    def __init__(self):
        """Initialize exception."""
        super().__init__('No cluster access found')