"""Jobard database models."""

import sqlalchemy as sa

from jobard_api.conf.database import metadata
from jobard_api.core.types import State

DEFAULT_VALUE_ZERO = '0'
DEFAULT_VALUE_ONE = '1'
DEFAULT_VALUE_NOW_UTC = sa.text("(NOW() AT TIME ZONE 'UTC')")

job_orders = sa.Table(
    'job_order',
    metadata,
    sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
    sa.Column('command', sa.ARRAY(sa.String)),
    sa.Column('arguments', sa.ARRAY(sa.String), nullable=True),
    sa.Column('interpolate_command', sa.Boolean, server_default='f'),
    sa.Column('split', sa.String, nullable=True),
    sa.Column('priority', sa.Integer, server_default=DEFAULT_VALUE_ONE),
    sa.Column('cores', sa.Integer, server_default=DEFAULT_VALUE_ONE),
    sa.Column('memory', sa.String, server_default='512M'),
    sa.Column('walltime', sa.String, server_default='00:05:00'),
    sa.Column('concurrency', sa.Integer, server_default=DEFAULT_VALUE_ZERO),
    sa.Column('max_concurrency', sa.Integer, server_default=DEFAULT_VALUE_ONE),
    sa.Column('job_extra', sa.ARRAY(sa.String), nullable=True),
    sa.Column('env_extra', sa.JSON, nullable=True),
    sa.Column('state', sa.Enum(State), server_default=State.INIT_ASKED),
    sa.Column('progress', sa.Float, server_default=DEFAULT_VALUE_ZERO),
    sa.Column('job_count', sa.Integer, server_default=DEFAULT_VALUE_ZERO),
    sa.Column('created', sa.TIMESTAMP, server_default=DEFAULT_VALUE_NOW_UTC),
    sa.Column('updated', sa.TIMESTAMP, server_default=DEFAULT_VALUE_NOW_UTC),
    sa.Column('submitted', sa.TIMESTAMP, nullable=True),
    sa.Column('finished', sa.TIMESTAMP, nullable=True),
    sa.Column('root_log_path', sa.String, nullable=True),
    sa.Column('name', sa.String, nullable=True),
    sa.Column('error_message', sa.String, nullable=True),
    sa.Column('is_healthcheck', sa.Boolean),
    sa.Column('connection', sa.String),
    sa.Column('user_id', sa.Integer),
    sa.Column('image_id', sa.Integer, nullable=True),
    sa.Column('command_alias', sa.String(32), nullable=True),
    sa.Column('job_log_prefix_arg_idx', sa.Integer, nullable=True),
)

job_arrays = sa.Table(
    'job_array',
    metadata,
    sa.Column('id', sa.Integer, primary_key=True, autoincrement=True, nullable=True),
    sa.Column('job_order', sa.Integer, sa.ForeignKey('job_order.id', ondelete='CASCADE')),
    sa.Column('job_offset', sa.Integer, server_default=DEFAULT_VALUE_ZERO),
    sa.Column('job_count', sa.Integer, server_default=DEFAULT_VALUE_ZERO),
    sa.Column('scheduled_id', sa.String, nullable=True),
    sa.Column('state', sa.Enum(State), server_default=State.NEW),
    sa.Column('progress', sa.Float, server_default=DEFAULT_VALUE_ZERO),
    sa.Column('created', sa.TIMESTAMP, server_default=DEFAULT_VALUE_NOW_UTC),
    sa.Column('updated', sa.TIMESTAMP, server_default=DEFAULT_VALUE_NOW_UTC),
    sa.Column('submitted', sa.TIMESTAMP, nullable=True),
    sa.Column('finished', sa.TIMESTAMP, nullable=True),
    sa.Column('daemon_id', sa.String, nullable=True),
    sa.Column('executor_id', sa.String, nullable=True),
    sa.Column('executor_log_path', sa.String, nullable=True),
    sa.Column('error_message', sa.String, nullable=True),
    sa.Column('try_nb', sa.Integer, server_default=DEFAULT_VALUE_ONE),
)

jobs = sa.Table(
    'job',
    metadata,
    sa.Column('id', sa.Integer, primary_key=True, autoincrement=True, nullable=True),
    sa.Column('job_array', sa.Integer, sa.ForeignKey('job_array.id', ondelete='CASCADE')),
    sa.Column('job_array_index', sa.Integer, server_default=DEFAULT_VALUE_ONE),
    sa.Column('command_with_parameters', sa.ARRAY(sa.String)),
    sa.Column('arguments',  sa.ARRAY(sa.String)),
    sa.Column('scheduled_id', sa.String, nullable=True),
    sa.Column('state', sa.Enum(State), server_default=State.NEW),
    sa.Column('progress', sa.Float),
    sa.Column('created', sa.TIMESTAMP, server_default=DEFAULT_VALUE_NOW_UTC),
    sa.Column(
        'updated',
        sa.TIMESTAMP,
        server_default=DEFAULT_VALUE_NOW_UTC,
        server_onupdate=DEFAULT_VALUE_NOW_UTC,  # noqa: WPS221
    ),
    sa.Column('submitted', sa.TIMESTAMP, nullable=True),
    sa.Column('finished', sa.TIMESTAMP, nullable=True),
    sa.Column('error_message', sa.String, nullable=True),
    sa.Column('log_path_stdout', sa.String, nullable=True),
    sa.Column('log_path_stderr', sa.String, nullable=True),
)
