"""Jobard API exceptions."""
from typing import Any

from jobard_api.core.exceptions import JobardApiException, UnknownIdException, AccessForbiddenException


class ForbiddenJobOrderException(AccessForbiddenException):
    """Job order access forbidden exception class."""

    def __init__(self, item_id: Any):
        """Initialize exception.

        Args:
            item_id: identifier of item
        """
        super().__init__('Job order', item_id)


class UnknownJobOrderIdException(UnknownIdException):
    """Unknown Job Order ID exception class."""

    def __init__(self, item_id: Any):
        """Initialize exception.

        Args:
            item_id: identifier of item
        """
        super().__init__('Job order', item_id)


class UnknownJobIdException(UnknownIdException):
    """Unknown Job ID exception class."""

    def __init__(self, item_id: Any):
        """Initialize exception.

        Args:
            item_id: identifier of item
        """
        super().__init__('Job', item_id)


class NotCancellableException(JobardApiException):
    """Not cancellable exception class."""
