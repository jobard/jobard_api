"""JobOrder DTO."""
from __future__ import annotations

from datetime import datetime
from typing import Dict, Generic, List, Optional

from pydantic import validator
from pydantic.generics import GenericModel

from jobard_api.core.dto import JobardModel, jobard_model_type
from jobard_api.core.types import Arguments, Memory, Split, State, Walltime, CommandAlias, JobOrderName


class JobardListModel(GenericModel, Generic[jobard_model_type]):
    """List of jobs DTO."""

    count: int = 0
    offset: int = 0
    limit: int = 0
    records: Optional[List[jobard_model_type]] = None


class JobOrderSubmit(JobardModel):
    """Job order submitted DTO."""

    name: JobOrderName = None
    command: List[str]
    arguments: Optional[List[Arguments]] = None
    interpolate_command: Optional[bool] = False
    split: Optional[Split] = None
    priority: Optional[int] = 1
    cores: Optional[int] = 1
    memory: Optional[Memory] = '512M'
    walltime: Optional[Walltime] = '00:05:00'
    max_concurrency: Optional[int] = 1
    connection: str = 'localhost'
    job_extra: Optional[List[str]] = None
    env_extra: Optional[Dict[str, str]] = None
    is_healthcheck: Optional[bool] = False
    user_id: Optional[int] = None
    image_id: Optional[int] = None
    command_alias: Optional[CommandAlias] = None
    job_log_prefix_arg_idx: Optional[int] = None

    @validator('command')
    def command_validator(cls, v):
        if v is not None and len(v) < 1:
            raise ValueError('command must have at least 1 element.')
        return v

    @validator('command_alias')
    def command_alias_validator(cls, v):
        if v is not None and (len(v) < 1 or len(v) > 32):
            raise ValueError('command_alias length must be at least 1 character and be lower than 32.')
        return v

    @validator('job_log_prefix_arg_idx')
    def job_log_prefix_arg_idx_validator(cls, v, values, **kwargs):
        if v is not None:
           args = values['arguments'] if 'arguments' in values else []
           max_idx = len(min(args, key=len)) - 1
           if v < 0 or v > max_idx:
               raise ValueError(f'Invalid job log prefix argument index value, must be a value between 0 and {max_idx}')
        return v

class JobOrder(JobOrderSubmit):
    """Job order DTO."""

    id: int
    state: Optional[str] = State.INIT_ASKED
    progress: Optional[float] = 0
    job_count: Optional[int] = 0
    created: Optional[datetime] = datetime.utcnow()
    updated: Optional[datetime] = datetime.utcnow()
    submitted: Optional[datetime] = None
    finished: Optional[datetime] = None
    concurrency: Optional[int] = 0
    root_log_path: Optional[str] = None


class JobArray(JobardModel):
    """JobArray DTO."""

    id: int
    job_order: int
    job_offset: int
    job_count: int
    scheduled_id: Optional[int]
    state: State = State.NEW
    progress: float = 0
    created: Optional[datetime] = datetime.utcnow()
    updated: Optional[datetime] = datetime.utcnow()
    submitted: Optional[datetime] = None
    finished: Optional[datetime] = None
    daemon_id: Optional[str] = None
    executor_id: Optional[str] = None
    executor_log_path: Optional[str] = None
    error_message: Optional[str] = None
    try_nb: int


class Job(JobardModel):
    """Job DTO."""

    id: int
    job_array: int
    job_array_index: int
    command_with_parameters: List[str]
    arguments: Arguments = None
    scheduled_id: Optional[str] = None
    state: Optional[str] = State.NEW
    progress: Optional[float] = 0
    created: Optional[datetime] = datetime.utcnow()
    updated: Optional[datetime] = datetime.utcnow()
    submitted: Optional[datetime] = None
    finished: Optional[datetime] = None
    error_message: Optional[str] = None
    log_path_stdout: Optional[str] = None
    log_path_stderr: Optional[str] = None


class StatProgress(JobardModel):
    """JobOrder progress DTO."""

    percentage: float = 0
    job_states: dict = None


class JobOrderStat(JobardModel):
    """JobOrder statistics DTO."""

    id: int
    name: str
    state: str
    created: Optional[datetime] = None
    updated: Optional[datetime] = None
    submitted: Optional[datetime] = None
    finished: Optional[datetime] = None
    job_count: int = 0
    progress: Optional[StatProgress] = None
    error_message: Optional[str] = None


class JobOrderList(JobardListModel[JobOrder]):
    """List of job orders DTO."""


class JobList(JobardListModel[Job]):
    """List of jobs DTO."""


class JobArgList(JobardListModel[Arguments]):
    """List of job arguments DTO."""


class JobArrayList(JobardListModel[JobArray]):
    """List of job arrays DTO."""
