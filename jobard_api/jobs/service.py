"""Job order service."""
from typing import List, Optional

from sqlalchemy import func, select

from jobard_api.conf.database import database
from jobard_api.jobs import dto
from jobard_api.jobs.exceptions import (
    NotCancellableException,
    UnknownJobIdException,
    UnknownJobOrderIdException,
    ForbiddenJobOrderException,
)
from jobard_api.jobs.models import job_arrays, job_orders, jobs

STATE_FIELD = 'state'
COUNT_FIELD = 'count'
CANCELLABLE_STATES = ('INIT_ASKED', 'INIT_RUNNING', 'NEW', 'ENQUEUED', 'SUBMITTED', 'RUNNING')

JOB_USER_ID_QUERY = """
    SELECT 
      "jo"."user_id" AS "job_user_id",
      "jo"."id" AS "job_order_id"
    FROM 
      "job" AS j
      INNER JOIN "job_array" as "ja" ON "j"."job_array" = "ja"."id"
      INNER JOIN "job_order" as "jo" ON "ja"."job_order" = "jo"."id"
    WHERE
      "j"."id" = {0};
"""


async def create_job_order(job_order_input: dto.JobOrderSubmit) -> int:
    """Create a new job order.

    Args:
        job_order_input: Job order information

    Returns:
        JobOrder ID
    """
    query = job_orders.insert().values(**job_order_input.dict())
    return await database.execute(query)


async def cancel_job_order(job_order_id: int, user_id: int) -> None:
    """Ask for cancellation of a job order.

    Args:
        job_order_id: Job order ID
        user_id: The user ID.

    Raises:
        NotCancellableException: if the job order can not be cancelled
    """
    job_order: dto.JobOrder = await get_job_order(job_order_id=job_order_id, user_id=user_id)

    # check if state is eligible for cancellation
    if job_order.state not in CANCELLABLE_STATES:
        raise NotCancellableException('Job order state {0} does not allow cancellation'.format(job_order.state))

    # ask for cancellation
    job_order_cancellation_query = job_orders.update().where(
        job_orders.c.id == job_order_id,
    ).values(
        state=dto.State.CANCELLATION_ASKED,
    )
    await database.execute(job_order_cancellation_query)


async def get_job(job_id: int, user_id: int) -> dto.Job:
    """Retrieve a job from its ID.

    Args:
        job_id: Job ID
        user_id: User ID

    Returns:
        The corresponding Job

    Raises:
        UnknownJobIdException: If the job id is unknown
        ForbiddenJobOrderException: If the job order does not belong to the authenticated user
    """
    user_query = JOB_USER_ID_QUERY.format(job_id)
    user_query_result = await database.fetch_one(user_query)
    if user_query_result and user_query_result['job_user_id'] != user_id:
        raise ForbiddenJobOrderException(item_id=user_query_result['job_order_id'])
    job_query = jobs.select().where(jobs.c.id == job_id)
    job = await database.fetch_one(job_query)
    if not job:
        raise UnknownJobIdException(job_id)
    return job


async def get_jobs_list(
    job_order_id: int,
    user_id: int,
    offset: int,
    limit: int,
    states: Optional[List[dto.State]] = None,
) -> dto.JobList:
    """List jobs of a job order.

    Args:
        job_order_id: JobOrder ID
        user_id: the user ID
        offset: first job to select
        limit: maximum number of jobs returned,
        states: Job state

    Returns:
        List of Job ID
    """
    await get_job_order(job_order_id=job_order_id, user_id=user_id)

    # build query to retrieve job ID
    jobs_query = select(jobs.c).where(
        job_orders.c.id == job_order_id,
        job_arrays.c.job_order == job_order_id,
        jobs.c.job_array == job_arrays.c.id,
    )
    if states is not None:
        jobs_query = jobs_query.filter(jobs.c.state.in_(states))

    # build and execute count query
    count_result = await database.fetch_one(
        jobs_query.with_only_columns(func.count(jobs.c.id).label(COUNT_FIELD)),
    )
    jobs_result = await database.fetch_all(jobs_query.order_by(jobs.c.id).offset(offset).limit(limit))

    # retrieve jobs ID
    return dto.JobList(
        count=count_result[0],
        limit=limit,
        offset=offset,
        records=[dto.Job.parse_obj(job) for job in jobs_result],
    )


async def get_job_order_stat(job_order_id: int, user_id: int) -> dto.JobOrderStat:
    """Retrieve statistics of a job order.

    Args:
        job_order_id: Job order ID
        user_id: User ID

    Returns:
        Statistics of the job order

    Raises:
        UnknownJobOrderIdException: if the JobOrder ID is unknown
    """
    await get_job_order(job_order_id=job_order_id, user_id=user_id)
    job_order_query = select(
        job_orders.c.id,
        job_orders.c.name,
        job_orders.c.state,
        job_orders.c.error_message,
        job_orders.c.created,
        job_orders.c.updated,
        job_orders.c.submitted,
        job_orders.c.finished,
        job_orders.c.job_count,
        job_orders.c.progress,
    ).where(job_orders.c.id == job_order_id)
    job_order = await database.fetch_one(job_order_query)

    # check if job order exist
    if not job_order:
        raise UnknownJobOrderIdException(job_order_id)

    # retrieve progress by job state
    stat_progress_query = jobs.select().where(
        job_arrays.c.job_order == job_order_id,
        jobs.c.job_array == job_arrays.c.id,
    ).with_only_columns(
        jobs.c.state,
        func.count(jobs.c.state).label('job_state_count'),
    ).group_by(jobs.c.state)
    stat_progress_list = await database.fetch_all(stat_progress_query)

    # transform to JobOrderStat
    return dto.JobOrderStat(
        id=job_order['id'],
        name=job_order['name'],
        state=job_order[STATE_FIELD],
        created=job_order['created'],
        updated=job_order['updated'],
        submitted=job_order['submitted'],
        finished=job_order['finished'],
        job_count=job_order['job_count'],
        error_message=job_order['error_message'],
        progress=dto.StatProgress(
            percentage=job_order['progress'],
            job_states={
                stat_progress[STATE_FIELD]: stat_progress['job_state_count'] for stat_progress in stat_progress_list
            },
        ),
    )


async def get_jobs_arguments(
    job_order_id: int,
    user_id: int,
    offset: int,
    limit: int,
    states: Optional[List[dto.State]] = None,
) -> dto.JobArgList:
    """List jobs args of a job order.

    Args:
        job_order_id: JobOrder ID
        user_id: the user ID
        offset: first job to select
        limit: maximum number of jobs returned,
        states: Job states

    Returns:
        List of Job ID
    """
    await get_job_order(job_order_id=job_order_id, user_id=user_id)

    # build query to retrieve job ID
    jobs_query = select(jobs.c.arguments).where(
        job_orders.c.id == job_order_id,
        job_arrays.c.job_order == job_order_id,
        jobs.c.job_array == job_arrays.c.id,
    )
    if states is not None:
        jobs_query = jobs_query.filter(jobs.c.state.in_(states))

    # build and execute count query
    count_result = await database.fetch_one(
        jobs_query.with_only_columns(func.count(jobs.c.id).label(COUNT_FIELD)),
    )
    jobs_result = await database.fetch_all(jobs_query.offset(offset).limit(limit))

    # retrieve jobs ID
    return dto.JobArgList(
        count=count_result[0],
        limit=limit,
        offset=offset,
        records=[job[0] for job in jobs_result],
    )


async def get_job_order(job_order_id: int, user_id: int) -> dto.JobOrder:
    """Retrieve a job order from its ID.

    Args:
        job_order_id: Job order ID
        user_id: The user ID

    Returns:
        The corresponding job order id

    Raises:
        UnknownJobOrderIdException: If the job order id is unknown
        ForbiddenJobOrderException: If the job order does not belong to the authenticated user
    """
    job_query = job_orders.select().where(job_orders.c.id == job_order_id)
    job_order_response = await database.fetch_one(job_query)
    if job_order_response:
        job_order = dto.JobOrder.parse_obj(job_order_response)
        if job_order.user_id != user_id:
            raise ForbiddenJobOrderException(item_id=job_order_id)
    else:
        raise UnknownJobOrderIdException(job_order_id)
    return job_order_response


async def get_job_arrays(
    job_order_id: int,
    user_id: int,
    offset: int,
    limit: int,
    states: Optional[List[dto.State]] = None,
) -> dto.JobArrayList:
    """Retrieve the job arrays of a job order.

    Args:
        job_order_id: Job order ID
        user_id: The user ID
        offset: first job to select
        limit: maximum number of jobs returned,
        states: Job states

    Returns:
        The corresponding job arrays
    """
    await get_job_order(job_order_id=job_order_id, user_id=user_id)

    # build query to retrieve job ID
    job_arrays_query = job_arrays.select().where(job_arrays.c.job_order == job_order_id)
    if states is not None:
        job_arrays_query = job_arrays_query.filter(jobs.c.state.in_(states))

    # build and execute count query
    count_result = await database.fetch_one(
        job_arrays_query.with_only_columns(func.count(job_arrays.c.id).label(COUNT_FIELD)),
    )

    arrays_result = await database.fetch_all(job_arrays_query.order_by(job_arrays.c.id).offset(offset).limit(limit))

    # retrieve jobs ID
    return dto.JobArrayList(
        count=count_result[0],
        limit=limit,
        offset=offset,
        records=[dto.JobArray.parse_obj(job_array) for job_array in arrays_result],
    )


async def get_job_orders(
    user_id: int,
    offset: int,
    limit: int,
    states: Optional[List[dto.State]] = None,
    name: Optional[str] = None,
) -> dto.JobOrderList:
    """List job orders.

    Args:
        user_id: The user ID
        offset: first job to select
        limit: maximum number of jobs returned,
        states: Job state
        name: job order name

    Returns:
        List of job orders
    """
    # build query to retrieve job ID
    job_orders_query = job_orders.select().where(job_orders.c.user_id == user_id)
    if states is not None:
        job_orders_query = job_orders_query.filter(job_orders.c.state.in_(states))
    if name is not None and isinstance(name, str):
        job_orders_query = job_orders_query.filter(job_orders.c.name.contains(name, autoescape=True))

    # build and execute count query
    count_result = await database.fetch_one(
        job_orders_query.with_only_columns(func.count(job_orders.c.id).label(COUNT_FIELD)),
    )
    job_orders_result = await database.fetch_all(job_orders_query.order_by(job_orders.c.id).offset(offset).limit(limit))

    # retrieve jobs ID
    return dto.JobOrderList(
        count=count_result[0],
        limit=limit,
        offset=offset,
        records=[dto.JobOrder.parse_obj(job_order) for job_order in job_orders_result],
    )
