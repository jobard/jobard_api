"""Jobard API Docker images exceptions."""
from typing import Any

from jobard_api.core.exceptions import (
    UnknownIdException,
    AlreadyRegisteredException,
    AccessForbiddenException,
)


class ForbiddenDockerImageException(AccessForbiddenException):
    """Forbidden Docker image access exception class."""

    def __init__(self, item_id: Any):
        """Initialize exception.

        Args:
            item_id: identifier of item
        """
        super().__init__('Docker image', item_id)


class UnknownImageIdException(UnknownIdException):
    """Unknown Docker image ID exception class."""

    def __init__(self, item_id: Any):
        """Initialize exception.

        Args:
            item_id: identifier of item
        """
        super().__init__('Docker image', item_id)


class UnknownMountPointIdException(UnknownIdException):
    """Unknown Docker mount point ID exception class."""

    def __init__(self, item_id: Any):
        """Initialize exception.

        Args:
            item_id: identifier of item
        """
        super().__init__('Docker mount point', item_id)


class AlreadyRegisteredDockerImageException(AlreadyRegisteredException):
    """AlreadyRegistered Docker image exception class."""

    def __init__(self, item_value: Any):
        """Initialize exception.

        Args:
            item_value: value of the item
        """
        super().__init__('Docker image', item_property='name', item_value=item_value)


class AlreadyRegisteredDockerMountPointException(AlreadyRegisteredException):
    """AlreadyRegistered Docker mount point exception class."""

    def __init__(self, item_value: Any):
        """Initialize exception.

        Args:
            item_value: value of the item
        """
        super().__init__('Docker mount point', item_property='name', item_value=item_value)
