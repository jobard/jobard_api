"""Jobard database models for Docker images management"""

import sqlalchemy as sa
from sqlalchemy import UniqueConstraint

from jobard_api.conf.database import metadata

docker_images = sa.Table(
    'docker_image',
    metadata,
    sa.Column('image_id', sa.Integer, primary_key=True, autoincrement=True),
    sa.Column('name', sa.String),
    sa.Column('user_id', sa.Integer, sa.ForeignKey('jobard_user.user_id')),
    sa.Column('url', sa.String),
    sa.Column('is_test', sa.Boolean),
    sa.Column('enabled', sa.Boolean),
    UniqueConstraint('name', 'user_id'),
)

docker_mount_points = sa.Table(
    'docker_mount_point',
    metadata,
    sa.Column('mount_point_id', sa.Integer, primary_key=True, autoincrement=True),
    sa.Column('image_id', sa.Integer, sa.ForeignKey('docker_image.image_id')),
    sa.Column('name', sa.String),
    sa.Column('source', sa.String),
    sa.Column('destination', sa.String),
    UniqueConstraint('name', 'image_id'),
)
