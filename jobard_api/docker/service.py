"""Docker images service."""
from typing import NoReturn, Union, Dict, Any

from sqlalchemy import func, true, and_

from jobard_api.conf.database import database
from jobard_api.core.types import ClusterType
from jobard_api.docker import dto
from jobard_api.docker.dto import DockerImage, DockerMountPoint
from jobard_api.docker.exceptions import (
    UnknownImageIdException,
    UnknownMountPointIdException,
    AlreadyRegisteredDockerImageException,
    AlreadyRegisteredDockerMountPointException,
    ForbiddenDockerImageException,
)
from jobard_api.docker.models import docker_images, docker_mount_points
from jobard_api.users import service as users_service

COUNT_FIELD = 'count'


async def _check_docker_images_name_user_id_uk(name: str, user_id: int):
    """Check name and user_id unicity constraint on docker_images

    Args:
        name: Image name
        user_id: User id

    Raises:
        AlreadyRegisteredDockerImageException: If the image name already exists for the passed user id
    """
    query = docker_images.select().where(
        docker_images.c.name == name,
        docker_images.c.user_id == user_id
    )
    docker_image_response = await database.fetch_one(query)

    if docker_image_response:
        docker_image = dto.DockerImage.parse_obj(docker_image_response)
        if docker_image:
            raise AlreadyRegisteredDockerImageException(name)


async def _check_mount_point_name_image_id_uk(name: str, image_id: int):
    """Check name and image_id unicity constraint on docker_mount_points

    Args:
        name: Mount point name
        image_id: Docker image id

    Raises:
        AlreadyRegisteredDockerMountPointException: If the image name already exists for the passed user id
    """
    query = docker_mount_points.select().where(
        docker_mount_points.c.name == name,
        docker_mount_points.c.image_id == image_id
    )
    docker_mount_point_response = await database.fetch_one(query)

    if docker_mount_point_response:
        docker_mount_point = dto.DockerMountPoint.parse_obj(docker_mount_point_response)
        if docker_mount_point:
            raise AlreadyRegisteredDockerMountPointException(name)


async def create_docker_image(image_input: dto.DockerImageInput, user_id: int) -> Union[int, Dict[str, Any]]:
    """Create a new Docker image
    Args:
        image_input: Docker image information.
        user_id: The user ID

    Returns:
        Either the Docker image ID or a dictionary containing the Docker image ID and a warning message
    """
    warning_message = None
    cluster_types = await users_service.get_user_cluster_types(user_id=user_id)
    if ClusterType.KUBE not in cluster_types and ClusterType.SWARM not in cluster_types:
        warning_message = 'User insufficient cluster access : Docker images can only be used on SWARM or KUBERNETES'
    # Check unicity constraint
    await _check_docker_images_name_user_id_uk(name=image_input.name, user_id=user_id)
    query = docker_images.insert().values({'user_id': user_id, 'enabled': True, **image_input.dict()})
    query_result = await database.execute(query)
    if warning_message:
        return {'image_id': query_result, 'warning': warning_message}
    return query_result


async def get_docker_image(image_id: int, user_id: int) -> dto.DockerImage:
    """Retrieve a Docker image by id.

    Args:
        image_id: Docker image ID
        user_id: The user ID
    Returns:
        The corresponding Docker image if enabled

    Raises:
        UnknownImageIdException: If the Docker image's id is unknown or disabled
        ForbiddenDockerImageException: If the Docker image does not belong to the user
    """
    query = docker_images.select().where(and_(docker_images.c.image_id == image_id, docker_images.c.enabled == true()))
    docker_image_response = await database.fetch_one(query)
    if docker_image_response:
        docker_image = DockerImage.parse_obj(docker_image_response)
        if docker_image.user_id != user_id:
            raise ForbiddenDockerImageException(item_id=image_id)
    else:
        raise UnknownImageIdException(image_id)
    return docker_image_response


async def get_docker_images(
    user_id: int,
    offset: int,
    limit: int,
) -> dto.DockerImageList:
    """List user's Docker images

    Args:
        offset: first Docker image to select
        limit: maximum number of Docker images returned
        user_id: user id

    Returns:
        List of user's enabled Docker images
    """
    # Check user_id
    await users_service.get_user_by_id(user_id=user_id)

    query = docker_images.select().where(and_(docker_images.c.user_id == user_id, docker_images.c.enabled == true()))

    count_result = await database.fetch_one(
        query.with_only_columns(func.count(docker_images.c.user_id).label(COUNT_FIELD))
    )

    docker_images_result = await database.fetch_all(
        query.order_by(docker_images.c.image_id).offset(offset).limit(limit))

    return dto.DockerImageList(
        count=count_result[0],
        limit=limit,
        offset=offset,
        records=[dto.DockerImage.parse_obj(docker_image) for docker_image in docker_images_result]
    )


async def update_docker_image(docker_image_update: dto.DockerImageUpdate, user_id: int) -> dto.DockerImage:
    """Update a Docker image if enabled

    Args:
        docker_image_update: Docker image information
        user_id: The user ID

    Returns:
        The updated Docker image
    """
    # Check image_id validity and user access to Docker image
    db_docker_image = await get_docker_image(image_id=docker_image_update.image_id, user_id=user_id)
    db_docker_image = dto.DockerImage.parse_obj(db_docker_image)
    if docker_image_update.name and docker_image_update.name != db_docker_image.name:
        # Check unicity constraint
        await _check_docker_images_name_user_id_uk(name=docker_image_update.name, user_id=db_docker_image.user_id)
        db_docker_image.name = docker_image_update.name
    if docker_image_update.url and docker_image_update.url != db_docker_image.url:
        db_docker_image.url = docker_image_update.url
    if docker_image_update.is_test != db_docker_image.is_test:
        db_docker_image.is_test = docker_image_update.is_test

    update_query = (
        (
            docker_images
            .update()
            .where(docker_images.c.image_id == db_docker_image.image_id)
         )
        .values(
            name=db_docker_image.name,
            url=db_docker_image.url,
            is_test=db_docker_image.is_test,
        )
    )
    await database.execute(update_query)
    return db_docker_image


async def delete_docker_image(image_id: int, user_id: int) -> NoReturn:
    """Delete a Docker image if enabled and its potential mount points. Sets enabled boolean to False.

    Args:
        image_id: The Docker image id
        user_id: The user ID
    """
    # Check image_id validity and user access to Docker image
    await get_docker_image(image_id=image_id, user_id=user_id)
    disable_query = docker_images.update().where(docker_images.c.image_id == image_id).values(enabled=False)
    await database.execute(disable_query)


async def create_docker_mount_point(mount_point_input: dto.DockerMountPointInput, user_id: int) -> int:
    """Create a new Docker mount point
    Args:
        mount_point_input: Docker mount point information.
        user_id: The user ID

    Returns:
        The Docker mount point ID
    """
    # Check image_id validity and user access to Docker image
    await get_docker_image(image_id=mount_point_input.image_id, user_id=user_id)
    # Check unicity constraint
    await _check_mount_point_name_image_id_uk(name=mount_point_input.name, image_id=mount_point_input.image_id)
    query = docker_mount_points.insert().values(**mount_point_input.dict())
    return await database.execute(query)


async def get_docker_mount_points(
    image_id: int,
    user_id: int,
    offset: int,
    limit: int,
) -> dto.DockerMountPointList:
    """List image's Docker mount points

    Args:
        offset: first Docker mount point to select
        limit: maximum number of Docker mount points returned
        image_id: Docker image id
        user_id: the user ID

    Returns:
        List of image's Docker mount points
    """
    # Check image_id and user access to Docker image
    await get_docker_image(image_id=image_id, user_id=user_id)

    docker_mount_points_query = docker_mount_points.select().where(docker_mount_points.c.image_id == image_id)

    count_result = await database.fetch_one(
        docker_mount_points_query.with_only_columns(func.count(docker_mount_points.c.image_id).label(COUNT_FIELD))
    )

    docker_mount_points_result = await database.fetch_all(
        docker_mount_points_query.order_by(docker_mount_points.c.mount_point_id).offset(offset).limit(limit))

    return dto.DockerMountPointList(
        count=count_result[0],
        limit=limit,
        offset=offset,
        records=[dto.DockerMountPoint.parse_obj(mount_point) for mount_point in docker_mount_points_result]
    )


async def get_docker_mount_point(mount_point_id: int, user_id: int) -> dto.DockerMountPoint:
    """Retrieve a Docker mount point by id.

    Args:
        mount_point_id: Docker mount point ID
        user_id: The user ID

    Returns:
        The corresponding Docker mount point

    Raises:
        UnknownMountPointIdException: If the Docker mount point's id is unknown
    """
    query = docker_mount_points.select().where(docker_mount_points.c.mount_point_id == mount_point_id)
    docker_mount_point_response = await database.fetch_one(query)
    if docker_mount_point_response:
        docker_mount_point = DockerMountPoint.parse_obj(docker_mount_point_response)
        await get_docker_image(image_id=docker_mount_point.image_id, user_id=user_id)
    else:
        raise UnknownMountPointIdException(mount_point_id)
    return docker_mount_point

async def update_docker_mount_point(
    mount_point_update: dto.DockerMountPointUpdate,
    user_id: int,
) -> dto.DockerMountPoint:
    """Update a Docker mount point

    Args:
        mount_point_update: Docker mount point information
        user_id: The user ID

    Returns:
        The updated Docker mount point
    """
    db_docker_mount_point = \
        await get_docker_mount_point(mount_point_id=mount_point_update.mount_point_id, user_id=user_id)
    docker_mount_point_dto = dto.DockerMountPoint.parse_obj(db_docker_mount_point)
    await get_docker_image(image_id=docker_mount_point_dto.image_id, user_id=user_id)
    if docker_mount_point_dto.name and mount_point_update.name != docker_mount_point_dto.name:
        # Check unicity constraint
        await _check_mount_point_name_image_id_uk(
            name=mount_point_update.name,
            image_id=docker_mount_point_dto.image_id,
        )
        docker_mount_point_dto.name = mount_point_update.name
    if mount_point_update.source and mount_point_update.source != docker_mount_point_dto.source:
        docker_mount_point_dto.source = mount_point_update.source
    if mount_point_update.destination and mount_point_update.destination != docker_mount_point_dto.destination:
        docker_mount_point_dto.destination = mount_point_update.destination

    update_query = (
        (
            docker_mount_points
            .update()
            .where(docker_mount_points.c.mount_point_id == docker_mount_point_dto.mount_point_id)
         )
        .values(
            name=docker_mount_point_dto.name,
            source=docker_mount_point_dto.source,
            destination=docker_mount_point_dto.destination,
        )
    )
    await database.execute(update_query)
    return docker_mount_point_dto


async def delete_docker_mount_point(mount_point_id: int, user_id: int) -> NoReturn:
    """Delete a Docker mount point

    Args:
        mount_point_id: The Docker mount point id
        user_id: The user ID
    """
    response = await get_docker_mount_point(mount_point_id=mount_point_id, user_id=user_id)
    if response:
        mount_point = DockerMountPoint.parse_obj(response)
        await get_docker_image(image_id=mount_point.image_id, user_id=user_id)
    delete_query = docker_mount_points.delete().where(docker_mount_points.c.mount_point_id == mount_point_id)
    await database.execute(delete_query)
