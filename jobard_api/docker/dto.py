"""Jobard docker images and mount points DTO."""
from __future__ import annotations

from jobard_api.core.dto import JobardModel
from jobard_api.jobs.dto import JobardListModel


class DockerImageUpdate(JobardModel):
    """Docker image update DTO."""

    image_id: int
    name: str = None
    url: str = None
    is_test: bool


class DockerImageInput(JobardModel):
    """Docker image input DTO."""

    name: str
    url: str
    is_test: bool = False


class DockerImage(DockerImageInput):
    """Jobard Docker image DTO."""

    image_id: int
    user_id: int
    enabled: bool = True


class DockerImageList(JobardListModel[DockerImage]):
    """List of Docker images DTO."""


class DockerMountPointUpdate(JobardModel):
    """Docker mount point update DTO."""

    mount_point_id: int
    name: str = None
    source: str = None
    destination: str = None


class DockerMountPointInput(JobardModel):
    """Docker mount point input DTO."""

    image_id: int
    name: str
    source: str
    destination: str


class DockerMountPoint(DockerMountPointInput):
    """Docker mount point DTO."""

    mount_point_id: int


class DockerMountPointList(JobardListModel[DockerMountPoint]):
    """List of Docker mount points DTO."""
