"""Monitoring DTO."""
from __future__ import annotations

from typing import List, Optional

from fastapi_healthcheck.enum import HealthCheckStatusEnum
from pydantic import validator
from pydantic.datetime_parse import timedelta
from pydantic.types import SecretStr

from jobard_api.core.dto import JobardModel
from jobard_api.core.types import ClusterType
from jobard_api.jobs.dto import JobardListModel


class MonitoringResultEntity(JobardModel):
    """Monitoring entity output information."""

    alias: Optional[str] = None
    status: Optional[HealthCheckStatusEnum] = None
    time_taken: Optional[timedelta] = None
    tags: Optional[List[str]] = None


class MonitoringResultDatabaseEntity(MonitoringResultEntity):
    """Monitoring database output information."""

    def __init__(self, **kwargs):
        """Initialize object."""
        super().__init__(**kwargs)
        self.tags = ['database']
        self.alias = 'Postgres Database'


class MonitoringResultConnectionEntity(MonitoringResultEntity):
    """Monitoring host connection output information."""

    host_connection: str
    preemption_time: Optional[timedelta] = None

    def __init__(self, **kwargs):
        """Initialize object."""
        super().__init__(**kwargs)
        self.tags = ['daemon']
        self.alias = 'Host connection {0}'.format(self.host_connection)


class MonitoringResultDaemonEntity(MonitoringResultEntity):
    """Monitoring daemon output information."""

    daemon_id: str
    host_connections: List[MonitoringResultConnectionEntity]

    def __init__(self, **kwargs):
        """Initialize object."""
        super().__init__(**kwargs)
        self.tags = ['daemon']
        self.alias = 'Daemon #{0}'.format(self.daemon_id)


class MonitoringResult(JobardModel):
    """The global monitoring output information."""

    status: Optional[HealthCheckStatusEnum] = None
    total_time_taken: Optional[timedelta] = None
    database_entity: MonitoringResultDatabaseEntity = None
    daemon_entities: List[MonitoringResultDaemonEntity] = None


class ClusterInput(JobardModel):
    """Cluster DTO."""

    cluster_name: str
    type: Optional[ClusterType] = ClusterType.LOCAL
    healthcheck_interval: timedelta
    remote_username: Optional[str]
    password: Optional[str]
    client_key: Optional[str]

    @validator('healthcheck_interval')
    def check_healthcheck_interval(cls, healthcheck_interval):  # noqa: N805, D102
        assert healthcheck_interval > timedelta(minutes=5), 'healthcheck interval must be greater than 5 minutes.'
        return healthcheck_interval

class Cluster(JobardModel):
    """Cluster DTO."""

    cluster_id: int
    cluster_name: str
    type: Optional[ClusterType] = ClusterType.LOCAL
    healthcheck_interval: timedelta
    remote_username: Optional[str]
    password: Optional[SecretStr]
    client_key: Optional[SecretStr]
    enabled: bool = True

    def get_password_string(self) -> Optional[str]:
        return self.password.get_secret_value() if self.password else None

    def get_client_key_string(self) -> Optional[str]:
        return self.client_key.get_secret_value() if self.client_key else None

class ClusterUpdate(ClusterInput):
    """Cluster DTO."""

    cluster_id: int

class ClusterList(JobardListModel[Cluster]):
    """List of clusters DTO."""


class RemoteConnectionInput(JobardModel):
    """Remote connection DTO."""

    name: str
    cluster_id: int
    daemon_id: Optional[str] = '0'

class RemoteConnection(RemoteConnectionInput):
    """Remote connection DTO."""

    enabled: bool = True

class RemoteConnectionUpdate(JobardModel):
    """Remote connection DTO."""

    name: str
    cluster_id: int
    daemon_id: Optional[str] = '0'

class ConnectionList(JobardListModel[RemoteConnection]):
    """List of host connections DTO."""

class RemoteConnectionClusterInput(JobardModel):
    """Remote connection + cluster DTO."""

    name: str
    daemon_id: Optional[str] = '0'
    cluster: ClusterInput
