"""Monitoring service."""
import time
from functools import wraps
from typing import NoReturn

from fastapi_healthcheck.enum import HealthCheckStatusEnum
from pydantic.datetime_parse import timedelta
from pydantic.types import SecretStr
from sqlalchemy import func, true, and_

from jobard_api.conf.database import database
from jobard_api.core.types import MonitoringStatus
from jobard_api.monitoring.dto import MonitoringResultDatabaseEntity, MonitoringResultDaemonEntity, \
    MonitoringResultConnectionEntity, MonitoringResult, ConnectionList, RemoteConnection, Cluster, ClusterList, \
    ClusterInput, RemoteConnectionInput, ClusterUpdate, RemoteConnectionUpdate, RemoteConnectionClusterInput
from jobard_api.monitoring.exceptions import NoConnectionException, NoClusterException, \
    AlreadyRegisteredClusterException, AlreadyRegisteredRemoteConnectionException, UnknownRemoteConnectionNameException, \
    UnknownClusterIdException, ClusterAccessExistForClusterException, RemoteConnectionExistForClusterException
from jobard_api.monitoring.models import clusters, remote_connections
from jobard_api.users.models import cluster_access

COUNT_FIELD = 'count'

MONITORING_QUERY = """
    SELECT 
      conn.name AS connection_name,
      conn.daemon_id AS daemon_id,
      COALESCE(subquery.jo_effective_time, INTERVAL '0 DAY') AS execution_time,
      COALESCE(subquery.jo_total_time - subquery.jo_effective_time, INTERVAL '0 DAY') AS preemption_time,
      -- A connection config is considered healthy if the test job order is in success and it finished in the interval 
      CASE 
        WHEN subquery.jo_state = 'FAILED' THEN 'DOWN' 
        WHEN subquery.jo_state = 'SUCCEEDED' AND subquery.jo_finished >= NOW() - cl.healthcheck_interval THEN 'UP' 
        ELSE 'DOWN' 
      END AS status 
    FROM 
      (
        -- Select the job order total time and effective time 
        SELECT 
          "jo"."connection" AS jo_connection_config,
          "ja"."daemon_id" AS jo_daemon_id,
          "jo"."state" AS jo_state,
          "jo"."finished" AS jo_finished,
          ("jo"."finished" - "jo"."submitted") AS jo_total_time,
          -- Select the effective processing delay of the job_order
          -- A test job_order has only one job_array which has only one job
          COALESCE("j"."finished" - "j"."submitted", INTERVAL '0 DAY') AS jo_effective_time 
        FROM 
          "job_order" AS "jo" 
          INNER JOIN "job_array" AS "ja" ON "ja"."job_order" = "jo"."id" 
          INNER JOIN "job" AS "j" ON "j"."job_array" = "ja"."id"
        WHERE 
          "jo"."id" IN(
            -- Select the last test job_order for each connection 
            WITH jo_last_finished AS (
              SELECT 
                "jo"."connection", 
                MAX("jo"."finished") AS max_finished 
              FROM 
                "job_order" AS "jo" 
              WHERE 
                (
                  "jo"."state" = 'SUCCEEDED' OR "jo"."state" = 'FAILED'
                ) 
                AND "jo"."is_healthcheck" = TRUE 
              GROUP BY 
                "jo"."connection"
            ) 
            SELECT 
              jo.id 
            FROM 
              job_order AS jo 
              INNER JOIN jo_last_finished ON jo.connection = jo_last_finished.connection 
              AND jo.finished = jo_last_finished.max_finished
          ) 
      ) subquery 
      RIGHT OUTER JOIN "remote_connection" AS "conn" ON "conn"."name" = "subquery"."jo_connection_config" 
      AND "conn"."daemon_id" = "subquery"."jo_daemon_id"
      INNER JOIN "cluster" AS "cl" ON "cl"."cluster_id" = "conn"."cluster_id"
    """


def timeit(function):
    """Decorator returning processing time of a function."""
    @wraps(function)
    def timeit_wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = function(*args, **kwargs)
        end_time = time.perf_counter()
        return result, timedelta(end_time - start_time)
    return timeit_wrapper


@timeit
def get_database_status(
) -> bool:
    """Retrieve the database status.
    Returns:
        True if database is connected, False otherwise.
    """
    return database.is_connected


async def get_monitoring(
) -> MonitoringResult:
    """Retrieve the monitoring database information and the monitoring daemon information
    from the test joborders/arrays/jobs in database.

    Returns:
        The monitoring information.
    """
    # Build the global monitoring information, first check database status.
    db_status, db_time_taken = get_database_status()
    db_entity = MonitoringResultDatabaseEntity(status=HealthCheckStatusEnum.HEALTHY if db_status
                                               else HealthCheckStatusEnum.UNHEALTHY,
                                               time_taken=db_time_taken)

    if db_entity.status == HealthCheckStatusEnum.UNHEALTHY:
        return MonitoringResult(status=HealthCheckStatusEnum.UNHEALTHY,
                                total_time_taken=db_time_taken,
                                database_entity=db_entity,
                                daemon_entities=[])

    try:
        # Retrieve daemons and connections monitoring information in database
        query_result = await database.fetch_all(MONITORING_QUERY)
        daemon_ids = set([result['daemon_id'] for result in query_result])

        # For each daemon,
        daemon_entities = list()
        for daemon_id in daemon_ids:
            # Retrieve the host connection status
            connections_status = [result for result in query_result if result['daemon_id'] == daemon_id]

            # Host connections health
            host_connection_results = list()
            host_connection_results.extend([MonitoringResultConnectionEntity(
                host_connection=connection_status['connection_name'],
                status=HealthCheckStatusEnum.HEALTHY
                if connection_status['status'] == MonitoringStatus.UP else HealthCheckStatusEnum.UNHEALTHY,
                time_taken=connection_status['execution_time'],
                preemption_time=connection_status['preemption_time']
            ) for connection_status in connections_status])

            # Global daemon health
            daemon_status = HealthCheckStatusEnum.HEALTHY \
                if all([connection_result.status == HealthCheckStatusEnum.HEALTHY for connection_result
                        in host_connection_results]) else HealthCheckStatusEnum.UNHEALTHY
            daemon_time_taken = sum([connection_result.time_taken for connection_result in host_connection_results],
                                    timedelta())

            daemon_entity = MonitoringResultDaemonEntity(daemon_id=daemon_id, host_connections=host_connection_results,
                                                         status=daemon_status, time_taken=daemon_time_taken)
            daemon_entities.append(daemon_entity)

        # Compute global status
        global_status = HealthCheckStatusEnum.HEALTHY \
            if (
                all([entity.status == HealthCheckStatusEnum.HEALTHY for entity in daemon_entities])
                and
                db_entity.status == HealthCheckStatusEnum.HEALTHY
                ) else HealthCheckStatusEnum.UNHEALTHY
        total_time_taken = sum([entity.time_taken for entity in daemon_entities], timedelta()) + db_entity.time_taken

        return MonitoringResult(status=global_status,
                                total_time_taken=total_time_taken,
                                database_entity=db_entity,
                                daemon_entities=daemon_entities
                                )
    except Exception:
        return MonitoringResult(status=HealthCheckStatusEnum.UNHEALTHY)


async def _check_cluster_name_uk(name: str):
    """Check name unicity constraint on cluster

    Args:
        name: Cluster name

    Raises:
        AlreadyRegisteredClusterException: If the cluster name already exists
    """
    query = clusters.select().where(
        clusters.c.cluster_name == name
    )
    cluster_response = await database.fetch_one(query)

    if cluster_response:
        cluster = Cluster.parse_obj(cluster_response)
        if cluster:
            raise AlreadyRegisteredClusterException(name)


async def get_clusters(
        offset: int,
        limit: int,
) -> ClusterList:
    """Retrieve the clusters stored in database .
    Args:
         offset: first Cluster to select
         limit: maximum number of Clusters returned

    Returns:
        The clusters.
    """

    # Retrieve the clusters in database
    query = clusters.select().where(clusters.c.enabled == true())

    count_result = await database.fetch_one(
        query.with_only_columns(func.count(clusters.c.cluster_id).label(COUNT_FIELD))
    )

    clusters_result = await database.fetch_all(query.order_by(clusters.c.cluster_id).offset(offset).limit(limit))

    if len(clusters_result) == 0:
        raise NoClusterException()

    # Hide password and key of the clusters
    records = [Cluster.parse_obj(result) for result in clusters_result]

    return ClusterList(
        count=count_result[0],
        limit=limit,
        offset=offset,
        records=records,
    )


async def create_cluster(cluster_input: ClusterInput) -> int:
    """Create a new cluster.
    Args:
        cluster_input: Cluster information

    Returns:
        The Cluster ID.
    """
    # Check unicity constraint
    await _check_cluster_name_uk(name=cluster_input.cluster_name)

    query = clusters.insert().values({'enabled': True, **cluster_input.dict()})
    return await database.execute(query)


async def get_cluster(cluster_id: int) -> Cluster:
    """Retrieve a Cluster by id.

    Args:
        cluster_id: Cluster ID
    Returns:
        The corresponding Cluster if enabled

    Raises:
        UnknownClusterIdException: If the Cluster's ID is unknown or disabled
    """
    query = clusters.select().where(and_(clusters.c.cluster_id == cluster_id, clusters.c.enabled == true()))
    cluster_response = await database.fetch_one(query)
    if not cluster_response:
        raise UnknownClusterIdException(cluster_id)
    db_cluster = Cluster.parse_obj(cluster_response)
    return db_cluster


async def update_cluster(cluster_update: ClusterUpdate) -> Cluster:
    """Update a Cluster if enabled

    Args:
        cluster_update: Cluster information

    Returns:
        The updated Cluster
    """
    # Check cluster validity
    db_cluster = await get_cluster(cluster_id=cluster_update.cluster_id)
    db_cluster = Cluster.parse_obj(db_cluster)
    if cluster_update.cluster_name and cluster_update.cluster_name != db_cluster.cluster_name:
        # Check unicity constraint
        await _check_cluster_name_uk(name=cluster_update.cluster_name)
        db_cluster.cluster_name = cluster_update.cluster_name
    if cluster_update.type and cluster_update.type != db_cluster.type:
        db_cluster.type = cluster_update.type
    if cluster_update.healthcheck_interval and cluster_update.healthcheck_interval != db_cluster.healthcheck_interval:
        db_cluster.healthcheck_interval = cluster_update.healthcheck_interval
    if cluster_update.remote_username != db_cluster.remote_username:
        db_cluster.remote_username = cluster_update.remote_username

    if cluster_update.password != db_cluster.get_password_string():
        db_cluster.password = SecretStr(cluster_update.password) if cluster_update.password else None
    if cluster_update.client_key != db_cluster.get_client_key_string():
        db_cluster.client_key = SecretStr(cluster_update.client_key) if cluster_update.client_key else None

    update_query = (
        (
            clusters
            .update()
            .where(clusters.c.cluster_id == db_cluster.cluster_id)
         )
        .values(
            cluster_name=db_cluster.cluster_name,
            type=db_cluster.type,
            healthcheck_interval=db_cluster.healthcheck_interval,
            remote_username=db_cluster.remote_username,
            password=db_cluster.password.get_secret_value() if db_cluster.password else None,
            client_key=db_cluster.client_key.get_secret_value() if db_cluster.client_key else None
        )
    )
    await database.execute(update_query)
    return db_cluster

async def _check_cluster_access_and_connection(cluster_id: int):
    """Check if there are connections or cluster accesses pointing to the cluster

    Args:
        cluster_id: cluster ID
    Raises:
        ClusterAccessExistForClusterException: If there are cluster access pointing to the cluster.
    """
    # Check cluster accesses
    query = cluster_access.select().join(clusters).where(
        and_(cluster_access.c.cluster_id == cluster_id, cluster_access.c.enabled == true()))
    query_result = await database.fetch_all(query)

    if len(query_result) > 0:
        raise ClusterAccessExistForClusterException(cluster_id=cluster_id)

    # Check remote connections
    query = remote_connections.select().join(clusters).where(
        and_(remote_connections.c.cluster_id == cluster_id, remote_connections.c.enabled == true()))
    query_result = await database.fetch_all(query)

    if len(query_result) > 0:
        raise RemoteConnectionExistForClusterException(cluster_id=cluster_id)

async def delete_cluster(cluster_id: int) -> NoReturn:
    """Delete a Cluster if enabled. Sets enabled boolean to False.

    Args:
        cluster_id: The Cluster ID
    """
    # Check cluster_name validity
    await get_cluster(cluster_id=cluster_id)

    # Check there are no cluster access or remote connection refering to the cluster
    await _check_cluster_access_and_connection(cluster_id=cluster_id)

    disable_query = clusters.update().where(clusters.c.cluster_id == cluster_id).values(enabled=False)
    await database.execute(disable_query)


async def _check_connection_name_daemon_id_uk(name: str, daemon_id: str):
    """Check name unicity constraint on connection

    Args:
        name: Connection name
        daemon_id: daemon Id

    Raises:
        AlreadyRegisteredRemoteConnectionException: If the connection name, daemon ID already exist
    """
    query = remote_connections.select().where(and_(remote_connections.c.name == name,
                                                   remote_connections.c.daemon_id == daemon_id))
    connection_response = await database.fetch_one(query)

    if connection_response:
        connection = RemoteConnection.parse_obj(connection_response)
        if connection:
            raise AlreadyRegisteredRemoteConnectionException(name+','+daemon_id)


async def create_connection(connection_input: RemoteConnectionInput) -> int:
    """Create a new remote connection.
    Args:
        connection_input: Remote connection information

    Returns:
        The remote connection ID.
    """

    # Check unicity constraint
    await _check_connection_name_daemon_id_uk(name=connection_input.name, daemon_id=connection_input.daemon_id)

    query = remote_connections.insert().values({'enabled': True, **connection_input.dict()})
    return await database.execute(query)

async def update_connection(connection_update: RemoteConnectionUpdate) -> RemoteConnection:
    """Update a Connection if enabled

    Args:
        connection_update: Connection information

    Returns:
        The updated Connection
    """
    # Check connection validity
    db_connection = await get_connection(name=connection_update.name, daemon_id=connection_update.daemon_id)
    if connection_update.cluster_id and connection_update.cluster_id != db_connection.cluster_id:
        db_connection.cluster_id = connection_update.cluster_id

    update_query = (
        (
            remote_connections
            .update()
            .where(
                remote_connections.c.name == db_connection.name,
                remote_connections.c.daemon_id == db_connection.daemon_id)
         )
        .values(cluster_id=db_connection.cluster_id)
    )
    await database.execute(update_query)
    return db_connection


async def get_connections(
        offset: int,
        limit: int,
) -> ConnectionList:
    """Retrieve the remote connections stored in database .
    Args:
        offset: first connection to select
        limit: maximum number of connections returned
    Returns:
        The remote connections.
    """

    # Retrieve connections in database
    query = remote_connections.select().where(remote_connections.c.enabled == true())

    count_result = await database.fetch_one(
        query.with_only_columns(func.count().label(COUNT_FIELD))
    )

    connections_result = await database.fetch_all(
        query.order_by(remote_connections.c.daemon_id, remote_connections.c.name).offset(offset).limit(limit))

    if len(connections_result) == 0:
        raise NoConnectionException()
    return ConnectionList(
        count=count_result[0],
        limit=limit,
        offset=offset,
        records=[RemoteConnection.parse_obj(connection) for connection in connections_result],
    )

async def get_connection(name: str, daemon_id: str) -> RemoteConnection:
    """Retrieve a remote connection by its name and the daemon id.

    Args:
        name: Connection name
        daemon_id : Daemon ID.
    Returns:
        The corresponding connection if enabled

    Raises:
        UnknownConnectionNameException: If the Connection is unknown or disabled
    """
    query = remote_connections.select().where(and_(remote_connections.c.name == name,
                                                   remote_connections.c.daemon_id == daemon_id,
                                                   remote_connections.c.enabled == true()))
    connection_response = await database.fetch_one(query)
    if not connection_response:
        raise UnknownRemoteConnectionNameException(f'name : {name} daemon id : {daemon_id}')
    db_connection = RemoteConnection.parse_obj(connection_response)
    return db_connection

async def delete_connection(name: str, daemon_id: str) -> NoReturn:
    """Delete a Remote connection if enabled. Sets enabled boolean to False.

    Args:
        name: The connection name
        daemon_id : Daemon ID.
    """
    # Check connection name validity
    await get_connection(name=name, daemon_id=daemon_id)
    disable_query = remote_connections.update().where(
        and_(remote_connections.c.name == name, remote_connections.c.daemon_id == daemon_id)).values(enabled=False)
    await database.execute(disable_query)

async def create_connection_cluster(connection_cluster_input: RemoteConnectionClusterInput) -> int:
    """Create a new remote connection and the associated cluster.
    Args:
        connection_cluster_input: Remote connection and cluster information

    Returns:
        The remote connection ID.
    """

    # Check unicity constraint
    await _check_connection_name_daemon_id_uk(name=connection_cluster_input.name,
                                              daemon_id=connection_cluster_input.daemon_id)

    # First create the cluster
    cluster_id = await create_cluster(cluster_input=connection_cluster_input.cluster)

    # Then create the connection
    connection_dict = connection_cluster_input.dict()
    del connection_dict['cluster']
    connection_dict['cluster_id'] = cluster_id
    query = remote_connections.insert().values({'enabled': True, **connection_dict})
    return await database.execute(query)
