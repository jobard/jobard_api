"""Jobard database models for monitoring."""

import sqlalchemy as sa

from jobard_api.conf.database import metadata
from jobard_api.core.alembic.custom_types import PGPString
from jobard_api.core.types import ClusterType
from jobard_api.conf.settings import settings

remote_connections = sa.Table(
    'remote_connection',
    metadata,
    sa.Column('daemon_id', sa.String, primary_key=True),
    sa.Column('name', sa.String, primary_key=True),
    sa.Column('cluster_id', sa.Integer, sa.ForeignKey('cluster.cluster_id')),
    sa.Column('enabled', sa.Boolean, default=True),
)

clusters = sa.Table(
    'cluster',
    metadata,
    sa.Column('cluster_id', sa.Integer, primary_key=True, autoincrement=True),
    sa.Column('cluster_name', sa.String, unique=True),
    sa.Column('type', sa.Enum(ClusterType), server_default=ClusterType.LOCAL),
    sa.Column('healthcheck_interval', sa.Interval),
    sa.Column('remote_username', sa.String, nullable=True),
    sa.Column('password', PGPString(settings.get_aes_key()), nullable=True),
    sa.Column('client_key', PGPString(settings.get_aes_key()), nullable=True),
    sa.Column('enabled', sa.Boolean, default=True),
)
