from typing import Any
from jobard_api.core.exceptions import JobardApiException, UnknownNameException, AlreadyRegisteredException, \
    UnknownIdException


class NoConnectionException(JobardApiException):
    """No host connection exception class."""

    def __init__(self):
        """Initialize exception."""
        super().__init__('No host connection found')


class NoClusterException(JobardApiException):
    """No cluster exception class."""

    def __init__(self):
        """Initialize exception."""
        super().__init__('No clusters found')

class UnknownClusterIdException(UnknownIdException):
    """Unknown cluster ID exception class."""

    def __init__(self, item_id: Any):
        """Initialize exception.

        Args:
            item_id: identifier of item
        """
        super().__init__('Cluster', item_id)

class AlreadyRegisteredClusterException(AlreadyRegisteredException):
    """AlreadyRegistered Cluster exception class."""

    def __init__(self, item_value: Any):
        """Initialize exception.

        Args:
            item_value: value of the item
        """
        super().__init__('Cluster', item_property='name', item_value=item_value)

class ClusterAccessExistForClusterException(JobardApiException):
    """ClusterAccessExistForClusterException Cluster exception class."""

    def __init__(self, cluster_id: int):
        """Initialize exception.

        Args:
            cluster_id: The cluster Id.
        """
        super().__init__(f'There are at least one cluster access which still exists for the cluster {cluster_id}')

class RemoteConnectionExistForClusterException(JobardApiException):
    """RemoteConnectionExistForClusterException Cluster exception class."""

    def __init__(self, cluster_id: int):
        """Initialize exception.

        Args:
            cluster_id: The cluster Id.
        """
        super().__init__(f'There are at least one remote connection which still exists for the cluster {cluster_id}')

class UnknownRemoteConnectionNameException(UnknownNameException):
    """Unknown connection name exception class."""

    def __init__(self, item_name: Any):
        """Initialize exception.

        Args:
            item_name: identifier of item
        """
        super().__init__('Remote connection', item_name)


class AlreadyRegisteredRemoteConnectionException(AlreadyRegisteredException):
    """AlreadyRegistered connection exception class."""

    def __init__(self, item_value: Any):
        """Initialize exception.

        Args:
            item_value: value of the item
        """
        super().__init__('Remote connection', item_property='name,daemon_id', item_value=item_value)


