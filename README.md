# jobard-api

Jobard API to submit and follow the execution of jobs.

Documentation : <https://jobard.gitlab-pages.ifremer.fr/documentation>

## Development

### Install dependencies

- installer poetry

```bash
pip install poetry poetry-dynamic-versioning
```

- installer les dépendances et le projet

```bash
poetry install -vv
```

### Pre-commit

- register pre-commit

```bash
pre-commit install
```

- Run the hooks manually

```bash
pre-commit run --all-files
```

### Check code quality

```bash
flake8 .
```

### Run unit tests

```bash
pytest --tb=line
```

## Launch application

### Database DSN

The default DSN is: `postgresql://jobard:jobard@localhost:5433/jobard`

You can configure the dabase DSN with an environnement variable :

```bash
export jobard_api_database_dsn='postgresql://jobard_rw:jobard_pwd@localhost:5433/jobard'
```

### Database migration

You can create or upgrade databse schema with alembic :

```bash
docker run -p 5432:5432 --name jobard_db -e POSTGRES_PASSWORD=jobard -e POSTGRES_USER=jobard postgres:14
alembic upgrade head
```

### Run the applicatiion in a development mode

Run the script `tests/run.py`

```bash
usage: run.py [-h] [--start-database] [--no-start-database] [--database-port DATABASE_PORT]

optional arguments:
  -h, --help            show this help message and exit
  --start-database
  --no-start-database
  --database-port DATABASE_PORT
```

This will :
- Start a postgresql in a container with a database/user/password `jobard`
- Start the Jobard API application

When the script is stopped, the container is removed.

### Test application

- Goto OpenAPI page (<http://localhost:8000/docs>) and try endpoints

  - <http://localhost:8000/docs#/default/submit_job_order_job_orders__post>

  ```json
    {
       "command":["my_command"],
       "arguments":[
          ["myFile1", "myOption1"],
          ["myFile2", "myOption2"],
          ["myFileN", "myOptionN"]
       ],
       "split":"/10",
       "cores":2,
       "memory":"2G",
       "walltime":"00:05:00",
       "connection":"datarmor",
       "job_extra": [
          "-q sequentiel"
       ]
    }
  ```
